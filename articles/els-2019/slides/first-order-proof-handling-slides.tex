\documentclass{beamer}
\usepackage{fontspec}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\setmainfont{DejaVu Serif}
\usepackage{polyglossia}
\setmainlanguage{english}
\usepackage{indentfirst}
\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing, angles, quotes, matrix, positioning,
  patterns}
\usepackage{csquotes}

\setbeamercovered{transparent=50}
\mode<presentation>{
\usetheme{Madrid}
}

\title{Handling first-order proofs}
\author[M.~Raskin, C.~Welzel]{\textbf{Michael Raskin}, raskin@mccme.ru \\ Christoph Welzel}
\institute[TUM]{Dept. of CS, TU Munich}
\date{April 1, 2019}

\begin{document}


\begin{frame}
\maketitle

\vfill
        {\hfil
                \raisebox{-2em}{\includegraphics[height=4em]{LOGO_ERC.jpg}}
        \hfil
        \includegraphics[height=2em]{TUM_Logo_blau_rgb_p-e1539771008892.png}
        \hfil
        \includegraphics[height=2em]{PaVeS_logo.png}
        \hfil
        \\ \fontsize{1.2ex}{0.5ex}\selectfont The project has received funding from the European Research Council (ERC)
        under the European Union’s Horizon 2020 research and innovation programme under grant agreement No 787367}
\end{frame}

\begin{frame}
        \frametitle{Overview}

\begin{itemize}
        \item Direction: algorithm and program verification
        \\ \qquad Local goal: formal proofs for distributed algorithms
        \item Approach: first-order logic and help from automated systems
        \item Example: mutual exclusion
        \item Our tooling and workflow
\end{itemize}
\end{frame}

\begin{frame}
        \frametitle{General context}
        
        Some programs have bugs

        ~

        More complex tasks; hardware rewards more complex approaches

        Bugs harder to detect, reproduce, eliminate

        ~

        Testing? Strict in-language restrictions (types, lifetimes, \ldots)? Static analysis? Proofs?

        ~

        All the approaches are useful — let developers choose

        ~ \pause

        We want to expand the options for \textbf{proofs}
\end{frame}

\begin{frame}
                \frametitle{Proofs: what kind of proofs?}

                We currently work on verifying distributed algorithm design via formal proofs

                ~

                What kind of proofs and tools can we use?

\begin{itemize}
        \item First-order logic proofs (with support from Automated Theorem Provers)
                \\ \qquad More compatible tools, more automation available,
                simpler logic, cannot use theory-specific knowledge
        \item Proofs in specific first-order theories (via SMT)
                \\ \qquad Reliance on complicated tools for a more complex problem
        \item Higher-order logic proofs (probably with interactive provers)
                \\ \qquad More expressive logics — different for different tools
\end{itemize}

                ~

We use first-order logic and ATP systems
\end{frame}

\begin{frame}[fragile]
                \frametitle{First-order logic and higher-order logic: illustration}

                Second-order logic puts function variables into language

\begin{verbatim}
        (setf f (lambda (x) …))
        (funcall f arg)
\end{verbatim}

                ~ 

                First-order logic: function variables not supported directly;

                \qquad … so interface-passing style is needed

\begin{verbatim}
        (defmethod call-f ((f (eql :something)) x) …)
        (defmethod call-f ((f (eql :something-else)) x) …)
        (setf f :something)
        (call-f f arg)
\end{verbatim}

~ \pause

        Our current experiments don't use that (yet)

\end{frame}



\begin{frame}
                \frametitle{First-order logic — our view}

                Closest to what is used in mathematics and theoretical CS textbooks for proofs

                \qquad … if everything goes well, reusing published proofs might become less work
                after some time

                ~

                A lot of compatible tools — even with the same format

                (Thousands of Problems for Theorem Provers — TPTP)

                \qquad … hope of cross-verification

                ~

                Most advanced proof search tools

                ~ 

                Hard to do fully formal proofs — but can ask automated provers to finish proof details
\end{frame}

\begin{frame}
                \frametitle{Example: mutual exclusion — Dijkstra's algorithm}

                Our current experiments: around Dijkstra's mutex

                (single CPU core, multi-threading)

                ~

                Algorithm idea: 
              
                (Some code to resolve conflicts faster — who gets priority)

                Process declares intent to enter critical section

                Verifies no other process has declared same intent 

                Executes critical section

                Cleans up declarations

                ~ \pause

                Logical encoding:

                Some theory of discrete time, and discrete list of agents

                Variables — functions from time to values

                Local variables — time and agent to values

                State of the program (instruction pointer) — local variable

                Single-threaded execution: active agent is a global variable
\end{frame}

\begin{frame}
        \frametitle{Example: Dijkstra's mutex (cont.)}

                Process declares intent to enter critical section 

                Verifies no other process has declared same intent

                ~

                Some theory of discrete time, and discrete list of agents

                Variables — functions from time to values

                Local variables — time and agent to values

                State of the program (instruction pointer) — local variable

                Single-threaded execution: active agent is a global variable

                ~

                Want to prove:

                Safety — two different agents cannot enter critical section simultaneously

                ~

                Actually prove:

                Define invariant; if it holds, it holds at the next moment

                Invariant as defined implies safety

\end{frame}

\begin{frame}
                \frametitle{Proving induction step}

                Want to prove:

                Safety — two different agents cannot enter critical section simultaneously

                ~

                Actually prove:

                Define invariant; if it holds now, it holds at the next moment

                Invariant as defined implies safety

                ~

                The base case tends to be obvious

                ~ \pause

                Why prove only inductive step? 

                Simpler… and already hard enough for now

                ~ 

                Easy to also prove the base case — 
                still exploring the options for proving the (harder) step

\end{frame}

\begin{frame}
                \frametitle{Why prove only inductive step?}

                Easy encoding of induction needs infinitely many axioms

                Rich theories harder for proof search

                ~

                Clear strategies for encoding first-order theories with induction
                
                \qquad (for example, in an interface-passing style)

                This is not our current priority (yet)
\end{frame}


\begin{frame}
                \frametitle{Working with a proof}

        Defining axioms (model + algorithm)

~

Defining the safety condition

~

Defining invariant

~

Claiming invariant is inductive

Claiming invariant implies safety

~

Adding/generating lemmas

~

Verifying the proof

\end{frame}

\begin{frame}
                \frametitle{Axioms}

                Must have manual part

                \qquad (if we care what we prove…)

                ~

                Basic theory + system behaviour

                ~

                Basic theory: «order is transitive»

                Execution model: «we can iterate over threads in order»

                Behaviour: «this is how this variable is updated by that assignment»

                ~

                Behaviour specification partially generated

                \qquad has to be tuned if (when…) model changes
\end{frame}

\begin{frame}[fragile]
        \frametitle{Axioms: (relatively) generic}

        «order is transitive»

\begin{verbatim}
fof(leq_transitive, axiom,
  ![X,Y,Z]: ((leq(X,Y)&leq(Y,Z))=>leq(X,Z))).
\end{verbatim}

$\forall X,Y,Z: X\leq Y \wedge Y\leq Z \Rightarrow X \leq Z$

~

«we can iterate over threads in order»

\begin{verbatim}
fof(next_agent_exhaustive, axiom,
  ![X,Y]: ((is_agent_or_initial_or_failure(X)
            & is_agent_or_initial_or_failure(Y)
            & leq(X,Y) & leq(Y,next_agent(X)))
          =>(X=Y | next_agent(X)=Y))).
\end{verbatim}

$ \approx \forall X,Y: X\leq Y \wedge Y\leq X+1 \Rightarrow X+1=Y \vee X=Y$

\end{frame}

\begin{frame}[fragile]
                \frametitle{Axioms: algorithm}
«this is how this variable is updated by that assignment»

\begin{verbatim}
fof(counter_structure_at_next_moment_3, axiom,
  ![T,A]: (((~(active_agent(T) != A))
            & (~((~is_moment(next_moment(T)))
                  | (~is_agent(A))))
            & (active_state(T,A) = step))
          => counter(next_moment(T),A) =
                          next_agent(counter(T,A)))).
\end{verbatim}

\begin{eqnarray*}
        \approx \forall moment\,T, agent\,A:\text{\hspace{12em}}
        \\
          active\_agent(T)=A
          \wedge active\_state(T,A)=step
          \\
          \Rightarrow
          counter(T+1,A)=counter(T,A)+1
\end{eqnarray*}

\end{frame}

\begin{frame}[fragile]
                \frametitle{Safety condition}

                Must be hand-picked — this is what we care about!

                ~

                Usually simple

                «Two different agents cannot be in critical section at once»

\begin{verbatim}
fof(define_safety_for, checked_definition,
  ![T,A1,A2]: (safe_for(T,A1,A2)<=>(
        (active_state(T,A1)=criticalSection
         & active_state(T,A2)=criticalSection)
        => A1=A2))).
fof(define_safety, checked_definition,
  ![T]: (safe(T)<=>(![A1,A2]: safe_for(T,A1,A2)))).
\end{verbatim}
        \begin{eqnarray*}
                \forall T,A_1,A_2: (safe\_for(T,A_1,A_2) \Leftrightarrow 
                \text{\hspace{10em}} \\
  ((state(T,A_1)=state(T,A_2)=criticalSection)\Rightarrow A_1=A_2))
        \end{eqnarray*}
\end{frame}

\begin{frame}[fragile]
                \frametitle{Invariant}

                Some technical additions to safety…

\begin{verbatim}
fof(define_inside, checked_definition,
  ![T,A]: (inside(T,A) <=> (
    is_moment(T) & is_agent(A) &
    (active_state(T,A) = startCheck
     | active_state(T,A) = selfCheck
     | …)))).
fof(define_inside_correct_for, checked_definition,
  ![T, A]: (
    inside_correct_for(T, A) <=>
      (inside(T,A) => outside(T,A) = false))).
\end{verbatim}

        (per-thread variable $outside$ is consistent with state)
\end{frame}

\begin{frame}[fragile]
                \frametitle{Invariant}
… and some key proof ideas

\begin{verbatim}
fof(define_passed, checked_definition,
  ![T,A,B]: (passed(T,A,B)<=>(instant_agent_pair(T,A,B)
    & counter_scope(T, A) & (leq(B,counter(T,A)))
    & (B=counter(T,A) => active_state(T,A)=step)))).

fof(define_passed_exclusive_for, checked_definition,
  ![T, A, B]: (passed_exclusive_for(T, A, B) <=>
     ~(passed(T,A,B) & passed(T,B,A)))).
\end{verbatim}

$instant\_agent\_pair$: typing/non-aliasing conditions

$counter\_scope$: program part where $counter$ is used

~

If two agents check each other, one sees the other having started check
\end{frame}

\begin{frame}[fragile]
                \frametitle{Invariants}

                Currently added manually

                Exploring ways to generate some of them

~

                Some classes of algorithms known to be verifiable automatically

                (under some assumptions)
\end{frame}

\begin{frame}
                \frametitle{Lemmas}

                If we have infinite computing power, lemmas are not needed

~

        … or if we had perfect proof search algorithms

~

                Currently added manually or semi-manually
\end{frame}

\begin{frame}[fragile]
                \frametitle{Lemmas: manual addition}

\begin{verbatim}
fof(invariant_safety_local, checked_lemma,
  ![T,A,B]: ((passed_exclusive_for(T,A,B)
             & passed_in_critical(T))
        => safe_for(T,A,B))).
\end{verbatim}

        (unfolding quantifier, specifying the dependencies)

        ~

Yes, this makes the proof much faster
\end{frame}

\begin{frame}[fragile]
                \frametitle{Lemmas: guided generation}


\begin{verbatim}
fof(invariant_preserved, checked_lemma,
  ![T]: (invariant(T) => invariant(next_moment(T)))).
tpi(ed_invariant_preserved, expand_specific_definitions,
  invariant_preserved(invariant)).
…
tpi(sc_all, split_all_conjunctions, '-').
\end{verbatim}

Expand specific definition before proving

\qquad … which makes the expansion eligible for conjunction-splitting

\begin{verbatim}
fof(sc_ed_invariant_preserved_expanded_…_…,
  ![T]:((inside_correct(T) & passed_exclusive(T)
        & passed_in_critical(T))
     =>passed_in_critical(next_moment(T)))).
\end{verbatim}
\end{frame}

\begin{frame}
                \frametitle{Lemmas: guided generation}

                Another example — too big to show — case analysis

                ~

                Prove «A or B», 
                
                then «if A then C» and «if B then C»,

                then «C»
\end{frame}



\begin{frame}
                \frametitle{Lemmas: automatic generation}

                Would be nice

                ~

                Some facts easy to generate

                ~

                May be useful even if insufficient

                ~

                (Future direction)

\end{frame}



\begin{frame}
                \frametitle{Verifying the proof}

Running the proof script — as a script

        (automated, of course)

~

Read entire input, apply instructions for lemma generation

~

        For each lemma, make ATP prove it from axioms and previous lemmas

\qquad … each request is completely self-sufficient and independent

        Use that lemma as an axiom in the future
\end{frame}

\begin{frame}[fragile]
                \frametitle{Lemma verification loop}

  \begin{tikzpicture}
    \node [draw, rectangle, minimum width = 4.5cm] (axioms) {Axioms};
    \node [draw, rectangle, minimum width = 4.5cm, below=0.5 of axioms]
      (checkeddefinitions) {\enquote{Checked definitions}};
    \node [draw, rectangle, minimum width = 4.5cm, below=0.5 of
      checkeddefinitions] (checkedlemma1) {\enquote{Checked lemma}};
    \node [draw, rectangle, minimum width = 4.5cm, below=0.5 of checkedlemma1]
      (checkedlemma2) {\enquote{Checked lemma}};
    \node [draw, rectangle, minimum width = 4.5cm, below=0.5 of checkedlemma2]
      (checkedlemma3) {\enquote{Checked lemma}};

    \draw [ decoration = { brace, amplitude = 5pt, raise = 2pt }, decorate,
    line width = 1pt ] (axioms.north east) --
    node [right, xshift = 6pt] (lemma1) {} (checkeddefinitions.south east);
    \draw [ decoration = { brace, amplitude = 5pt, raise = 2pt, mirror },
    decorate, line width = 1pt ] (axioms.north west) --
    node [left, xshift = -6pt] (lemma2) {} (checkedlemma1.south west);
    \draw [ decoration = { brace, amplitude = 5pt, raise = 20pt }, decorate,
    line width = 1pt ] (axioms.north east) --
    node [right, xshift = 24pt] (lemma3) {} (checkedlemma2.south east);

    \node (checkedlemma1anchor) at (checkedlemma1.east-|lemma1) {};
    \node (checkedlemma2anchor) at (checkedlemma2.west-|lemma2) {};
    \node (checkedlemma3anchor) at (checkedlemma3.west-|lemma3) {};

    \draw [->, bend left = 55] (lemma1.center) to node[left, draw, circle,
    inner sep = 1pt] {\tiny 1} (checkedlemma1anchor.center);
    \draw [->, bend right = 65] (lemma2.center) to node[right, draw, circle,
    inner sep = 1pt] {\tiny 2} (checkedlemma2anchor.center);
    \draw [->, bend left = 65] (lemma3.center) to node[right, draw, circle,
    inner sep = 1pt] {\tiny 3} (checkedlemma3anchor.center);

    \node [below = 0.2 of checkedlemma3] (dots) {\vdots};
  \end{tikzpicture}

        Arrows are ATP invocations
\end{frame}


\begin{frame}
        \frametitle{Further guidance}

        User can specify which axioms (and previous lemmas) some lemma needs

        ~

        Guided lemma generation generates such hints

        ~

        Can make proof search much faster
\end{frame}


\begin{frame}
                \frametitle{Once the proof is verified…}

        If ATP prints proofs (not all do…), we can:

        Export dependency graph of steps taken when proving a single lemma

        … and a dependency graph between axioms/lemmas in the entire proof
\end{frame}

\begin{frame}
                \frametitle{Everything is complicated}

\begin{itemize}
        \item Different provers have different power
                \\ \qquad and different functionality around proof search
        \item Global rankings do not reflect fitness for specific purpose
        \item Proof search is a dark art based on heuristics
                \\ \qquad useless lemmas can be useful for guiding search…
\end{itemize}
\end{frame}

\begin{frame}
                \frametitle{}
        { \Huge

Thanks for your attention!

~

\hfill Questions?
        

~

        }

        ~

https://gitlab.common-lisp.net/mraskin/gen-fof-proof
\end{frame}



\end{document}

