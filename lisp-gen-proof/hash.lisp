(in-package :gen-fof-proof)

(defun string-to-bytes (s)
  (map '(vector (unsigned-byte 8)) 'char-code s))

(defun bytes-to-base16 (b)
  (string-downcase
    (format nil "~{~2,'0x~}"
            (map 'list 'identity b))))

(defun apply-hash-algo (algo plaintext)
  (bytes-to-base16
    (ironclad:digest-sequence
      (ironclad:make-digest
        (intern (string-upcase (string algo))
                :keyword))
      (string-to-bytes
        (format nil "~a" plaintext)))))

(defun sha512 (s)
  (apply-hash-algo :sha512 s))
