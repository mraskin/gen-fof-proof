#! /bin/sh

time="${1:-15}"
args="$2"
command="$3"

export PS1="<TPI> $PS1"

shift; shift; shift

target_dir_prefix="/tmp/tpi-export-${USER}/$(date +%Y%m%d-%H%M%S)/"
mkdir -p "$target_dir_prefix"
target_dir="$(mktemp -d -p "$target_dir_prefix")"
echo "Target directory: [[ $target_dir ]]"
TPI_PROVER="'$(dirname "$0")/export-quasiprover.sh' '$target_dir'" TPI_PROVER_SIMPLE_OPTIONS=' simple ' "${command:-SHELL}" "$@"
