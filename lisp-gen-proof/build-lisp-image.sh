#! /bin/sh

cd "$(dirname "$0")"

./nix-load-lisp.sh --eval 

NO_RLWRAP=1 ./nix-load-lisp.sh --eval '
(progn
  (sb-ext:save-lisp-and-die "./gen-fof-proof.bin" :executable t))
'
