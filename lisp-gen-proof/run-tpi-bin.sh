#! /bin/sh

file="$1"
options="$2"
shift
shift

rlwrap ./gen-fof-proof.bin --eval "(progn $*)" --eval '(gen-fof-proof:run-tpi-file "'"$file"'" '"$options"')' --eval '(quit)'

