(in-package :gen-fof-proof)

(defun tptp-fof-normal-form (f &key
                               ; bound variables are scoped,
                               ; free variable naming decisions from one
                               ; branch matter globally
                               (free-variables
                                 (make-hash-table :test 'equal))
                               (bound-variables nil)
                               (variable-count
                                 (length (all-subtrees
                                           f 'tptp-syntax::variable)))
                               (number-length
                                 (length (format nil "~d" variable-count)))
                               (clean nil))
  (labels
    ((recurse (x &rest args)
              (apply
                'tptp-fof-normal-form
                x
                (append
                  args
                  (list
                    :free-variables free-variables
                    :bound-variables bound-variables
                    :number-length number-length
                    :clean clean))))
     (take-args (x)
                (and (listp x)
                     (if clean
                       (second x)
                       (and
                         (listp (second x)) (second (second x))))))
     (numbered-name (prefix n)
                    (format nil (format nil "~~a~~~d,'0d" number-length)
                            prefix n))
     (default-recurse () (list (first f)(recurse (second f))))
     )
    (if (stringp f) f
      (let* ((op (first f))
             (args (take-args f)))
        (if (or (listp op) (stringp op))
          (loop for ff in f
                collect (recurse ff))
          (case op
            ((tptp-syntax::Sp) nil)
            ((tptp-syntax::variable)
             (let* ((name (tptp-true-text (tptp-no-space f)))
                    (bound-name (cdr (assoc name bound-variables :test 'equal)))
                    (free-name (gethash name free-variables))
                    (free-count (hash-table-count free-variables))
                    (known-name (or bound-name free-name))
                    (new-name (numbered-name "F" (1+ free-count)))
                    (result-name (or known-name new-name))
                    (ast (esrap:parse op result-name)))
               (unless known-name (setf (gethash name free-variables) new-name))
               ast))
            ((tptp-syntax::fof_quantified_formula)
             (let* ((quantifier (tptp-true-text
                                  (tptp-no-space (nth 0 args))))
                    (varlist (nth (if clean 2 4) args))
                    (content (nth (if clean 5 10) args))
                    (variables
                      (loop for v in
                            (all-subtrees varlist 'tptp-syntax::variable)
                            collect (tptp-true-text (tptp-no-space v))))
                    (preexisting (length bound-variables))
                    (new-bindings
                      (loop for v in variables
                            for k upfrom (1+ preexisting)
                            for n :=
                            (numbered-name "B" k)
                            collect (cons v n)))
                    (renamed-content
                      (recurse content
                               :bound-variables
                               (append new-bindings bound-variables)))
                    (new-formula-text
                      (format
                        nil
                        "~a[~{~a~#[~:;,~]~}]:~a"
                        quantifier
                        (mapcar 'cdr new-bindings)
                        (extract-parse-tree-text
                          renamed-content nil)))
                    (new-formula (esrap:parse op new-formula-text)))
               new-formula))
            ((tptp-syntax::fof_unitary_formula)
             (cond
               ((equal (first args) "(" #+nil")" )
                (let* ((content (nth (if clean 1 2) args))
                       (deep-content (take-args content))
                       (inner-op (first deep-content)))
                  (case inner-op
                    ((tptp-syntax::fof_unitary_formula
                       tptp-syntax::fof_unary_formula)
                     (recurse deep-content))
                    (t (default-recurse)))))
               (t (default-recurse))))
            ((tptp-syntax::fof_unary_formula)
             (cond
               ((and (listp (first args))
                     (eq (first (first args)) 'tptp-syntax::unary_connective))
                (let* ((content (nth (if clean 1 2) args))
                       (deep-content (take-args content))
                       (inner-op (first deep-content))
                       (inner-args (take-args deep-content)))
                  (cond
                    ((and
                       (eq inner-op 'tptp-syntax::fof_unitary_formula)
                       (equal (first inner-args) "(" #+nil ")"))
                     (let* ((deeper-content
                              (take-args (nth (if clean 1 2) inner-args)))
                            (deeper-op (first deeper-content)))
                       (case deeper-op
                         ((tptp-syntax::fof_unitary_formula
                            tptp-syntax::fof_unary_formula)
                          (esrap:parse
                            op
                            (format nil "~a~a"
                                    (extract-parse-tree-text
                                      (tptp-no-space (first args))
                                      nil)
                                    (extract-parse-tree-text
                                      (recurse deeper-content)
                                      nil))))
                         (t (default-recurse)))))
                    (t (default-recurse)))))
               (t (default-recurse))))
            ((tptp-syntax::fof_and_formula
               tptp-syntax::fof_or_formula)
             (let* ((binop (fof-assoc-binop f))
                    (arguments
                      (loop with stack := (list f)
                            with res := nil
                            while stack
                            for current := (pop stack)
                            for current-assoc :=
                            (fof-extract-binary-assoc current)
                            for recurse :=
                            (and
                              current-assoc
                              (equal 
                                binop
                                (fof-assoc-binop current-assoc)))
                            do
                            (if recurse
                              (setf stack (append (fof-assoc-components
                                                    current-assoc)
                                                  stack))
                              (push (recurse current) res))
                            finally (return res)))
                    (arguments (loop for a in arguments
                                     collect
                                     (extract-parse-tree-text a nil)))
                    (arguments (sort arguments 'string<))
                    (joined
                      (cons (first arguments)
                            (loop for a in (rest arguments)
                                  collect binop
                                  collect a))))
               (esrap:parse op (format nil "~{~a~}" joined))))
            (t (default-recurse))))))))

(defun tptp-fof-simplify (f)
  (extract-parse-tree-text
    (tptp-fof-normal-form
      (esrap:parse
        'tptp-syntax::fof_formula
        f))
    nil))
