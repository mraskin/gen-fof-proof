#! /bin/sh

escape_for_lisp() {
  sed -e 's/\\/&&/g; s/"/\\&/g'
}

filter="$1"
file="$2"

shift; shift

if test -e "$filter"; then
        filter="(alexandria:read-file-into-string \"$(echo "$filter" | escape_for_lisp)\")"
else
        filter="\"$(echo "$filter" | escape_for_lisp)\""
fi

if test -e "$file"; then
        target="\"$(echo "$file" | escape_for_lisp).used\""
else
        target=
fi

if test -z "$file"; then
        file="\"$(escape_for_lisp)\""
elif test -e "$file"; then
        file="(alexandria:read-file-into-string \"$(echo "$file" | escape_for_lisp)\")"
else
        file="\"$(echo "$file" | escape_for_lisp)\""
fi

if test -n "$target"; then
        ./nix-load-lisp.sh --eval "(progn $*)" --eval "(alexandria:write-string-into-file (gen-fof-proof::filter-tpi-lemmas $file $filter) $target :if-exists :supersede)" --eval '(quit)'
else
        ./nix-load-lisp.sh --eval "(progn $*)" --eval "(princ (gen-fof-proof::filter-tpi-lemmas $file $filter))" --eval '(quit)' | grep -A999999 -E '^(fof|cnf|tpi)[(]'
fi
