Whitespace <- " " / "\t" / "\n" / "\r" / "\uc2a0"
Sp <- Whitespace +

Comma <- Sp? "," Sp?
OParen <- Sp? "(" Sp?
CParen <- Sp? ")" Sp?

AnnotatedFormula <- FOFAnnotated
FOFAnnotated <- "fof" OParen Name Comma FormulaRole Comma FOFFormula Sp? Annotations? CParen "." Sp?

Annotations <- Comma Source OptionalInfo

FormulaRole <- LowerWord

FOFFormula <- FOFLogicFormula
FOFLogicFormula <- FOFBinaryFormula / FOFUnitaryFormula
FOFBinaryFormula <- FOFBinaryNonAssoc / FOFBinaryAssoc

FOFBinaryNonAssoc <- FOFUnitaryFormula BinaryConnective FOFUnitaryFormula
FOFBinaryAssoc <- FOFOrFormula / FOFAndFormula
FOFOrFormula <- FOFUnitaryFormula  ( Sp? "|" Sp?  FOFUnitaryFormula) +
FOFAndFormula <- FOFUnitaryFormula ( Sp? "&" Sp?  FOFUnitaryFormula) +

FOFUnitaryFormula <- FOFQuantifiedFormula / FOFUnaryFormula / AtomicFormula / OParen FOFLogicFormula CParen

FOFQuantifiedFormula <- FOLQuantifier "[" Sp? FOFVariableList Sp? "]" Sp? ":" FOFUnitaryFormula
FOFVariableList <- Variable (Comma Variable)*
FOFUnaryFormula <- UnaryConnective FOFUnitaryFormula / FOLInfixUnary

FOLQuantifier <- ("!" / "?") Sp?
BinaryConnective <- Sp? ("<=>" / "=>" / "<=" / "<~>" / "~|" / "~&") Sp?
UnaryConnective <- "~" Sp?

FolInfixUnary <- Term InfixInequality Term

InfixInequality <- "!="

Term <- FunctionTerm / Variable 
FunctionTerm <- PlainTerm / DefinedTerm / SystemTerm
PlainTerm <- Functor OParen Arguments CParen / Constant
Functor <- AtomicWord
Constant <- Functor
Functor <- AtomicWord

SystemTerm <- SystemFunctor OParen Arguments CParen / SystemConstant
SystemConstant <- SystemFunctor
SystemFunctor <- AtomicSystemWord
AtomicSystemWord <- DollarDollarWord

Variable <- UpperWord
Arguments <- Term (Comma Term) *

Name <- AtomicWord / Integer
AtomicWord <- LowerWord / SingleQuoted
AtomicDefinedWord <- DollarWord
Number <- Integer
Integer <- SignedInteger / UnsignedInteger
SignedInteger <- Sign UnsignedInteger
UnsignedInteger <- Decimal
Decimal <- ZeroNumeric / PositiveDecimal
PositiveDecimal <- NonZeroNumeric Numeric*

SingleQuoted <- SingleQuote SQChar+ SingleQuote
SQChar <- "\\" "'" / "\\" "\\" / (! [\\'] .)
DistinctObject <- DoubleQuote DOChar+ DoubleQuote
DOChar <- '\\' '"' / '\\' '\\' / (! [\\"] .)

SingleQuote <- "'"
DoubleQuote <- '"'

DefinedTerm <- DefinedAtom / DefinedAtomicTerm
DefinedAtom <- Number / DistinctObject
DefinedAtomicTerm <- DefinedPlainTerm
DefinedPlainTerm <- DefinedConstant / DefinedFunctor OParen Arguments CParen
DefinedConstant <- DefinedFunctor
DefinedFunctor <- AtomicDefinedWord

DollarWord <- "$" LowerWord
DollarDollarWord <- "$$" LowerWord
UpperWord <- UpperAlpha AlphaNumeric *
LowerWord <- LowerAlpha AlphaNumeric *

ZeroNumeric <- "0"
NonZeroNumeric <- [1-9]
Numeric <- [0-9]
Sign <- [-+]
UpperAlpha <- [A-Z]
LowerAlpha <- [a-z]
Underscore <- "_"
AlphaNumeric <- UpperAlpha / LowerAlpha / Numeric / Underscore
