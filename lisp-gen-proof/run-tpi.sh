#! /bin/sh

file="$1"
options="$2"
shift
shift

./nix-load-lisp.sh --eval "(progn $*)" --eval '(gen-fof-proof:run-tpi-file "'"$file"'" '"$options"')' --eval '(quit)'
