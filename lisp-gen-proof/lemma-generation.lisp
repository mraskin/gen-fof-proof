(in-package :gen-fof-proof)

(defmethod formula-to-code ((formula tptp-leaf) parameters)
  formula)
(defmethod formula-to-code ((formula tptp-object-variable) parameters)
  (or (cdr (assoc (name formula) parameters :test 'equal))
      formula))
(defmethod formula-to-code ((formula tptp-internal-node) parameters)
  `(make-instance ,(class-of formula)
                  :name ,(name formula)
                  :args (list ,@(loop for a in (args formula)
                                      collect
                                      (formula-to-code a parameters)))))
(defmethod formula-to-code ((formula tptp-content-node) parameters)
  `(make-instance ,(class-of formula)
                  :name ,(name formula)
                  :args (list ,@(loop for a in (args formula)
                                      collect
                                      (formula-to-code a parameters)))
                  :content ,(formula-to-code (content formula) parameters)))
(defmethod formula-to-code ((formula tptp-fof-quantified) parameters)
  `(make-instance ,(class-of formula)
                  :name ,(name formula)
                  :args (list ,@(args formula))
                  :content ,(formula-to-code
                              (content formula)
                              (remove-if
                                (lambda (x)
                                  (find (car x)
                                        (mapcar 'name (args formula))
                                        :test 'equal))
                                parameters))))

(defmethod definition-to-instantiator ((definition tptp-fof-quantified))
  (assert (equal (name definition) "!"))
  (definition-to-instantiator (content definition)))
(defmethod definition-to-instantiator ((definition tptp-propositional-infix))
  (assert (equal (name definition) "<=>"))
  (let* ((decl (first (args definition)))
         (content (second (args definition)))
         (args (etypecase decl
                 (tptp-predicate-call (args decl))
                 (tptp-propositional-constant nil)))
         (call-type (if args 'tptp-predicate-call
                      'tptp-propositional-constant))
         (parameters (loop for v in args
                           for vn := (name v)
                           for s := (gensym)
                           collect (cons vn s)))
         (placeholders (mapcar 'cdr parameters))
         (argname (gensym))
         (code (formula-to-code content parameters))
         (code
           `(lambda (,argname)
              (assert (typep ,argname ',call-type))
              ,(when args
                 `(assert (= (length (args ,argname)) ,(length placeholders))))
              (let* (,@(loop for p in placeholders
                             for k upfrom 0
                             collect `(,p (nth ,k (args ,argname)))))
                ,@placeholders
                ,code))))
    (values (eval code)
            (name decl))))
(defmethod definition-to-instantiator ((definition tptp-predicate-infix))
  (assert (equal (name definition) "="))
  (let* ((decl (first (args definition)))
         (content (second (args definition)))
         (args (etypecase decl
                 (tptp-function-call (args decl))
                 (tptp-object-constant nil)))
         (call-type (if args 'tptp-function-call 'tptp-object-constant))
         (parameters (loop for v in args
                           for vn := (name v)
                           for s := (gensym)
                           collect (cons vn s)))
         (placeholders (mapcar 'cdr parameters))
         (argname (gensym))
         (code (formula-to-code content parameters))
         (code
           `(lambda (,argname)
              (assert (typep ,argname ',call-type))
              ,(when args
                 `(assert (= (length (args ,argname)) ,(length placeholders))))
              (let* (,@(loop for p in placeholders
                             for k upfrom 0
                             collect `(,p (nth ,k (args ,argname)))))
                ,@placeholders
                ,code))))
    (values (eval code)
            (name decl))))
(defmethod definition-to-instantiator ((definition string))
  (definition-to-instantiator (esrap:parse 'tptp-syntax::fof_formula
                                           definition)))
(defmethod definition-to-instantiator ((definition list))
  (definition-to-instantiator (tptp-parse-tree-to-object definition)))

(defun definition-list-to-hash-table (definitions)
  (let* ((ht (make-hash-table :test 'equal)))
    (loop for d in definitions
          do (multiple-value-bind (f n)
               (definition-to-instantiator d)
               (setf (gethash n ht) f)))
    ht))

(defmethod apply-definition-once (formula (definitions list) extra)
  (apply-definition-once formula (definition-list-to-hash-table definitions) extra))
(defmethod apply-definition-once ((formula tptp-leaf) (definitions hash-table) extra)
  extra definitions
  (values formula nil))
(defmethod apply-definition-once ((formula tptp-internal-node) (definitions hash-table) extra)
  (let* ((found nil)
         (new-args
           (loop for a in (args formula)
                 collect
                 (if found a
                   (multiple-value-bind (res success)
                     (apply-definition-once a definitions extra)
                     (setf found success)
                     res)))))
    (values (make-instance (class-of formula)
                           :name (name formula)
                           :args new-args)
            found)))
(defmethod apply-definition-once ((formula tptp-content-node) (definitions hash-table) extra)
  (let* ((found nil)
         (new-args
           (loop for a in (cons
                            (content formula)
                            (args formula))
                 collect
                 (if found a
                   (multiple-value-bind (res success)
                     (apply-definition-once a definitions extra)
                     (setf found success)
                     res)))))
    (values (make-instance (class-of formula)
                           :name (name formula)
                           :content (first new-args)
                           :args (rest new-args))
            found)))
(defmethod apply-definition-once ((formula tptp-object-constant) (definitions hash-table) extra)
  (let* ((name (name formula))
         (handler (gethash name definitions)))
    (if handler (values (funcall handler formula) name)
      (call-next-method))))
(defmethod apply-definition-once ((formula tptp-propositional-constant) (definitions hash-table) extra)
  (let* ((name (name formula))
         (handler (gethash name definitions)))
    (if handler (values (funcall handler formula) name)
      (call-next-method))))
(defmethod apply-definition-once ((formula tptp-predicate-call) (definitions hash-table) extra)
  (let* ((name (name formula))
         (handler (gethash name definitions)))
    (if handler (values (funcall handler formula) name)
      (call-next-method))))
(defmethod apply-definition-once ((formula tptp-function-call) (definitions hash-table) extra)
  (let* ((name (name formula))
         (handler (gethash name definitions)))
    (if handler (values (funcall handler formula) name)
      (call-next-method))))
(defmethod apply-definition-once ((formula string) definitions extra)
  (apply-definition-once (tptp-parse-tree-to-object
                           (esrap:parse 'tptp-syntax::fof_formula formula))
                         definitions extra))

(defclass definition-application-skip-assumptions () ())
(defmethod remove-definition-application-skip-assumptions (e) e)
(defmethod remove-definition-application-skip-assumptions ((e definition-application-skip-assumptions)) nil)

(defmethod apply-definition-once ((formula tptp-propositional-infix)
                                  (definitions hash-table)
                                  (extra definition-application-skip-assumptions))
  (cond ((equal (name formula) "=>")
         (multiple-value-bind (expansion success)
           (apply-definition-once (second (args formula))
                                  definitions
                                  (remove-definition-application-skip-assumptions extra))
           (if success
             (values
               (make-instance (class-of formula)
                              :name (name formula)
                              :args (list (first (args formula)) expansion))
               success))))
        (t (call-next-method))))

(def-tpi-handler
  "index_definitions" (data context)
  (let*
    ((formulae (gethash :formulae context))
     (definition-index
       (or (gethash :definition-index context)
           (make-hash-table :test 'equal)))
     (group-prefix (format nil "~a" data))
     (indexed-definitions
       (loop for f in (reverse formulae)
             for fo := (third f)
             for fn := (name fo)
             for gn := (concatenate 'string group-prefix fn)
             for defp := (equal (role fo) "checked_definition")
             when defp
             do (multiple-value-bind (func n)
                  (definition-to-instantiator (content fo))
                  (setf (gethash n definition-index) (list fn func gn))
                  (push gn (first f)))
             when defp collect fn)))
    (format *trace-output* "Indexed definitions:~{ ~a~}~%"
            indexed-definitions)
    (setf (gethash :definition-index context) definition-index)))

(defvar *expand-lemma-counter* 0)

(defun split-formulae (predicate context
                                 &key add-groups)
  (loop with pre := nil
        for tail on (gethash :formulae context)
        for f := (first tail)
        for fo := (third f)
        for fn := (name fo)
        for fg := (first f)
        for matchp :=
        (funcall predicate
                 :allow-other-keys t
                 :entry f
                 :statement fo
                 :name fn
                 :groups fg
                 :role (role fo))
        unless matchp do (push f pre)
        when matchp return
        (list
          (reverse pre)
          (list
            (append add-groups fg)
            fo f)
          (rest tail))))

(def-tpi-handler
  "expand_definitions" (data context &key expansion-extra max-count
                             allowed-definitions)
  (let* ((target-formula data)
         (target-name
           (etypecase target-formula
             (tptp-propositional-constant (name target-formula))
             (tptp-predicate-infix (name (second (args target-formula))))))
         (first-name
           (cond ((typep target-formula 'tptp-predicate-infix)
                  (name (first (args target-formula))))))
         (definition-index (gethash :definition-index context))
         (formulae-split
           (split-formulae
             (lambda (&key groups name role)
               (and (equal name target-name)
                    (cl-ppcre:scan "^checked_lemma" role)
                    (not (find 
                           "checked_lemmas_with_expanded_definitions"
                           groups :test 'equal))))
             context
             :add-groups (list "checked_lemmas_with_expanded_definitions")))
         (target (second formulae-split))
         (target-statement (second target))
         (target-groups (first target))
         (target-role (and target (role target-statement)))
         (definitions (make-hash-table :test 'equal))
         (simplification-steps
           (when target
             (maphash (lambda (k v)
                        (when
                          (or (null allowed-definitions)
                              (and (listp allowed-definitions)
                                   (find k allowed-definitions :test 'equal))
                              (and (hash-table-p allowed-definitions)
                                   (gethash k allowed-definitions)))
                          (setf (gethash k definitions) (second v))))
                      definition-index)
             (loop with origin := (second target)
                   for current := (content origin) then (first expansion)
                   for expansion := (multiple-value-list
                                      (apply-definition-once
                                        current definitions
                                        expansion-extra))
                   for k upfrom 1
                   while (<= k (or max-count k))
                   while (second expansion)
                   collect expansion)))
         (names (loop for s in (reverse simplification-steps)
                      for base-name :=
                      (format nil "ed_~a_expanded_definition_~a"
                              target-name
                              (sha512 (first s)))
                      for first := t then nil
                      for expanded-name :=
                      (if (and first first-name)
                        first-name
                        (format nil "~a_expansion" base-name))
                      for sufficient-name :=
                      (format nil "~a_sufficiency" base-name)
                      collect (list expanded-name sufficient-name)))
         (max-expansion-statement
           (make-instance 'tptp-statement
                          :kind "fof"
                          :name (first (first names))
                          :role target-role
                          :content
                          (first (first (last simplification-steps)))))
         (max-expansion-text
           (format nil "~a" max-expansion-statement))
         (max-expansion-entry
           (when
             simplification-steps
             (list
               (list "tpi" (first (first names))
                     (format nil "~a_pair" (first (first names)))
                     "checked_lemmas_with_expanded_definitions")
               max-expansion-text
               (statement-parse-to-object max-expansion-text))))
         (inactive-ht (gethash :inactive-formulae context))
         (simplification-entries
           (when simplification-steps
             (loop
               for previous in (reverse simplification-steps)
               for current in 
               (rest (reverse (cons (list (content target-statement) nil)
                                    simplification-steps)))
               for def-name := (second previous)
               for cur-name in (append (rest names) (list (list target-name)))
               for prev-name in names
               for last := (null (second current))
               for groups :=
               (if last target-groups
                 (list "tpi"
                       (first cur-name)
                       (format nil "~a_pair" (first cur-name))
                       "checked_lemmas_with_expanded_definitions"))
               for trans-groups :=
               (list "tpi"
                     (second prev-name)
                     (format nil "~a_pair" (first prev-name))
                     "checked_lemmas_with_expanded_definitions")
               for def-entry := (gethash def-name definition-index)
               for def-group := (third def-entry)
               for trans-claim :=
               (make-instance
                 'tptp-propositional-infix
                 :name "=>"
                 :args (list (first previous) (first current)))
               for trans-statement :=
               (make-instance
                 'tptp-statement
                 :name (second prev-name) :kind "fof"
                 :role (format nil "checked_lemma_~a" def-group)
                 :content trans-claim)
               for trans-text := (format nil "~a" trans-statement)
               for trans-entry :=
               (list 
                 (cons
                   "checked_lemmas_with_split_conjunctions"
                   trans-groups)
                 trans-text trans-statement)
               for cur-statement :=
               (make-instance
                 'tptp-statement
                 :name (first cur-name) :kind "fof"
                 :role (format nil "checked_lemma_~a_pair" (first prev-name))
                 :content (first current))
               for cur-text :=
               (format nil "~a" cur-statement)
               for cur-entry :=
               (list 
                 (append
                   (list
                     "$expand_definitions"
                     "$trivial" "$textual"
                     "checked_lemmas_with_split_conjunctions")
                   groups)
                 cur-text cur-statement)
               for k upfrom 1
               collect trans-entry
               collect cur-entry)))
         (new-formulae
           (append
             (first formulae-split)
             (reverse simplification-entries)
             (list max-expansion-entry)
             (third formulae-split))))
    (cond ((not target)
           (format *trace-output* 
                   "Formula name not found: ~s~%"
                   target-name)
           nil)
          ((not simplification-steps)
           (format *trace-output* 
                   "Nothing to expand (using ~a): ~s~%"
                   definitions
                   target-name)
           nil)
          (t
            (setf (gethash :formulae context) new-formulae)
            (mapcar
              (lambda (n) (setf (gethash n inactive-ht) t))
              (reduce 'append names))
            (first (first names))))))

(def-tpi-handler
  "expand_definitions_in_conclusion" (data context)
  (run-tpi-handler "expand_definitions" data context
                   :expansion-extra
                   (make-instance 'definition-application-skip-assumptions)))
(def-tpi-handler
  "expand_definitions_once" (data context)
  (run-tpi-handler "expand_definitions" data context
                   :max-count 1
                   :expansion-extra
                   (make-instance 'definition-application-skip-assumptions)))
(def-tpi-handler
  "expand_definitions_in_conclusion_once" (data context)
  (run-tpi-handler "expand_definitions" data context
                   :max-count 1
                   :expansion-extra
                   (make-instance 'definition-application-skip-assumptions)))

(def-tpi-handler
  "expand_specific_definitions" (data context &key
                                      expansion-extra)
  (let*
    ((target-formula data)
     (first-part (when (typep target-formula 'tptp-predicate-infix)
                   (assert (equal "=" (name target-formula)))
                   (assert (typep (first (args target-formula))
                                  'tptp-object-constant))
                   (first (args target-formula))))
     (target-part (if (typep target-formula 'tptp-predicate-infix)
                    (second (args target-formula))
                    target-formula))
     (target-name (progn
                    (assert (or (typep target-part 'tptp-function-call)
                                (typep target-part 'tptp-predicate-call)))
                    (name target-part)))
     (allowed-definitions
       (progn
         (assert (not (loop for a in (args target-part)
                            unless (typep a 'tptp-object-constant)
                            return t))
                 (target-part)
                 "definition names to expand should be constants; ~s"
                 (args target-part))
         (mapcar 'name (args target-part))))
     (simplified-target-part
       (make-instance 'tptp-propositional-constant
                      :name target-name))
     (simplified-target (if first-part
                          (make-instance 'tptp-predicate-infix
                                         :name "="
                                         :args (list
                                                 first-part
                                                 simplified-target-part))
                          simplified-target-part)))
    (run-tpi-handler "expand_definitions" simplified-target context
                     :allowed-definitions allowed-definitions
                     :expansion-extra expansion-extra)))
(def-tpi-handler
  "expand_specific_definitions_in_conclusion" (data context)
  (run-tpi-handler "expand_specific_definitions" data context
                   :expansion-extra
                   (make-instance 'definition-application-skip-assumptions)))

(defun split-conjunction (target-statement target-groups)
  (let* ((target-name (name target-statement))
         (target-role (role target-statement))
         (target-formula (content target-statement))
         (target-split 
           (fof-split-conjunction-claim target-formula))
         (success (> (length target-split) 1))
         (split-group 
           (when success
             (format nil "sg_~a_split_conjunction_~a"
                     target-name
                     (sha512 target-formula))))
         (service-groups 
           (remove-if 
             (lambda (x) (not (cl-ppcre:scan "^[$]" x)))
             target-groups))
         (split-lemmas
           (when success
             (loop for subf in target-split
                   for name := (format nil "sc_~a_split_conjunction_~a"
                                       target-name (sha512 subf))
                   for subst := (make-instance 'tptp-statement
                                               :kind "fof"
                                               :name name
                                               :role target-role
                                               :content subf)
                   for text := (format nil "~a" subst)
                   for entry :=
                   (list (append (list "tpi" "$split_conjunction_leaf" split-group)
                                 service-groups)
                           text subst)
                   for k upfrom 1
                   collect entry)))
         (new-target-statement
           (make-instance 'tptp-statement
                          :kind "fof"
                          :name target-name
                          :role (format nil "checked_lemma_~a" split-group)
                          :content target-formula))
         (new-target-text (format nil "~a" new-target-statement))
         (new-formulae
           (when success
             (append
               (list (list (append
                             target-groups
                             (list "$trivial" "$split_conjunction" "$textual"))
                           new-target-text
                           new-target-statement))
               (reverse split-lemmas)))))
    (when new-formulae (values new-formulae split-group))))

(def-tpi-handler
  "split_conjunction" (data context)
  (let* ((target-name (name data))
         (formulae-split
           (split-formulae
             (lambda (&key name groups role)
               (and (equal name target-name)
                    (cl-ppcre:scan "^checked_lemma" role)
                    (not (find 
                           "checked_lemmas_with_split_conjunctions"
                           groups :test 'equal))))
             context
             :add-groups (list "checked_lemmas_with_split_conjunctions")))
         (target (second formulae-split))
         (target-statement (second target))
         (target-groups (first target))
         (split-result
           (when target
             (multiple-value-list
               (split-conjunction target-statement target-groups))))
         (new-formulae (first split-result))
         (split-group (second split-result)))
    (cond
      (new-formulae
        (setf (gethash :formulae context)
              (append 
                (first formulae-split)
                new-formulae
                (third formulae-split)))
        (values split-group (1- (length new-formulae))))
      (t
        (format *trace-output* "Formula name not found: ~s~%"
                target-name) 
        nil))))

(def-tpi-handler
  "split_all_conjunctions" (data context)
  data
  (format *trace-output* "Splitting conjunctions in ~a formulae~%"
          (length (gethash :formulae context)))
  (setf (gethash :formulae context)
        (reduce
          'append
          (loop for f in (gethash :formulae context)
                collect
                (or 
                  (and
                    (not
                      (find "checked_lemmas_with_split_conjunctions"
                            (first f) :test 'equal))
                    (cl-ppcre:scan "^checked_lemma" (role (third f)))
                    (split-conjunction (third f) (first f)))
                  (list f)))))
  (format *trace-output* "Conjunctions split~%"))

