(asdf:load-system :gen-fof-proof)
(use-package :gen-fof-proof)
(esrap-peg:peg-compile (esrap-peg:parse-peg-file "tptp-staged-bnf.peg"))
(load-tptp-syntax-bnf "SyntaxBNF")
(load-tptp-syntax-bnf "SyntaxBNF.definition")
