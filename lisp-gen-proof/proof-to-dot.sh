#! /bin/sh

file="$1"
shift

if test -n "$file"; then
./nix-load-lisp.sh --eval "(progn $*)" --eval '(alexandria:write-string-into-file (gen-fof-proof:tptp-proof-to-dot (alexandria:read-file-into-string "'"$file"'")) "'"$file"'.dot" :if-exists :supersede)' --eval '(quit)'
else
        ./nix-load-lisp.sh --eval "(progn $*)" --eval '(format t "~a~%" (gen-fof-proof:tptp-proof-to-dot "'"$(cat)"'"))' --eval '(quit)' | grep -A 999999999 '^digraph ' --line-buffered
fi
