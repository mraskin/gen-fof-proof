(let* ((filename "data/mutex-proofs/dijkstra-mutex-typed.gen.ax"))
  (alexandria:write-string-into-file
    (tptp-concat
      (make-agent-stub
        (list
          "initialState"
          "forbidSteal" "checkTurn" "failureToEnter" "grabTurn" "cannotGrab"
          "declareEntry" "startCheck"
          "selfCheck" "collisionCheck" "step" "stepCheck"
          "criticalSection"
          "yieldTurn" "declareExit" "permitSteal"
          )
        (list "true" "false"))
      (define-list-predicate "boolean" (list "true" "false"))
      (define-function-type "counter"
                            "agent_or_initial_or_failure" "failure"
                            (list "T" "A") (list "moment" "agent") :total t)
      (define-function-type "turn"
                            "agent_or_initial" "failure"
                            (list "T") (list "moment") :total t)
      (define-function-type "stealable"
                            "boolean" "failure"
                            (list "T" "A") (list "moment" "agent_or_initial")
                            :total t)
      (define-function-type "outside"
                            "boolean" "failure"
                            (list "T" "A") (list "moment" "agent") :total t)
      (define-state-transitions
        "active_state_step"
        `(
          ("initialState" t "forbidSteal" nil)
          ("forbidSteal" t "checkTurn" nil)
          ("checkTurn" "turn(T)=A" "declareEntry" "failureToEnter")
          ("failureToEnter" "stealable(T, turn(T))=true" "grabTurn"
           "cannotGrab")
          ("grabTurn" t "cannotGrab" nil)
          ("cannotGrab" t "checkTurn" nil)
          ("declareEntry" t "startCheck" nil)
          ("startCheck" t "selfCheck" nil)
          ("selfCheck" "counter(T,A) != A" "collisionCheck" "step")
          ("collisionCheck" "outside(T,counter(T,A))=true" "step"
           "checkTurn")
          ("step" t "stepCheck" nil)
          ("stepCheck" "counter(T, A) = failure" "criticalSection"
           "selfCheck")
          ("criticalSection" t "yieldTurn" nil)
          ("yieldTurn" t "declareExit" nil)
          ("declareExit" t "permitSteal" nil)
          ("permitSteal" t "initialState" nil)
          ))
      (define-function-structure
        "counter" "step" "failure"
        (list "next_moment(T)" "A") (list "moment" "agent")
        "counter(T,A)"
        `(
          (("active_agent(T) != A"))
          (("active_state(T,A) = step") "next_agent(counter(T,A))")
          (("active_state(T,A) = startCheck") "next_agent(initial)")
          ))
      (define-function-structure
        "turn" "step" "failure"
        (list "next_moment(T)") (list "moment")
        "turn(T)"
        `(
          (("active_state(T, active_agent(T)) = grabTurn") "active_agent(T)")
          (("active_state(T, active_agent(T)) = yieldTurn") "initial")
          ))
      (define-function-structure
        "stealable" "step" "failure"
        (list "next_moment(T)" "A") (list "moment" "agent_or_initial")
        "stealable(T,A)"
        `(
          (("active_agent(T) != A"))
          (("active_state(T,A) = forbidSteal") "false")
          (("active_state(T,A) = permitSteal") "true")
          ))
      (define-function-structure
        "outside" "step" "failure"
        (list "next_moment(T)" "A") (list "moment" "agent")
        "outside(T,A)"
        `(
          (("active_agent(T) != A"))
          (("active_state(T,A) = failureToEnter") "true")
          (("active_state(T,A) = declareEntry") "false")
          (("active_state(T,A) = declareExit") "true")
          ))
      (tptp-fof-statement
        "initial_stealable"
        "![T]:(is_moment(T) => stealable(T, initial) = true)")
      )
    filename :if-exists :supersede))
