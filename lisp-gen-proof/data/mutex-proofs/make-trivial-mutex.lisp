; The trivial mutex is the following:
; Two states, in and out
; Global variable occupied
; Fully atomic «check-and-occupy»
; out, occupied=0 -> in, occupied=1
; out, occupied=1 -> out, occupied=1
; in, occupied=1 -> out,occupied=0

(let* ((filename "data/mutex-proofs/trivial-mutex.gen.ax"))
  (with-open-file (f filename :direction :output :if-exists :supersede)
    (format
      f "~{include('~a').~%~}"
      (list "order-model.ax" "time-model.ax" "agent-model.ax" "state-model.ax"))
    (format
      f "~{~a~%~}"
      (list
        (define-state-list `("in" "out"))
        "fof(zeroagent_state,axiom,![T]:(activestate(T,minimal)=out))."
        (define-value-transition
          "occupied" ()
          `(("out" () ("occupied(T)=minimal") "maximal")
            ("in" () () "minimal")))
        (define-state-transitions
          `(("in" t "out" nil)
            ("out" "occupied(T)=minimal" "in" "out")))
        ))))
