(defun tptp-multiop (op expressions)
  (format nil (concatenate 'string "~{(~a)~#[~:; " op " ~]~}") expressions))

(defun tptp-one-of (object values)
  (tptp-multiop "|" (mapcar (lambda (v)
                              (format nil "~a = ~a" object v))
                            values)))

(defun define-list-predicate (name elements)
  (format nil "fof(~a, axiom, ![X]: (~a(X)<=>(~a)))."
          name name (tptp-one-of "X" elements)))

(defun tptp-quoted (s) (format nil "~s" s))

(defun define-distinctness (name objects)
  (format nil "~{fof(~{~a_distinct_~a, axiom, ~a!=~a~}).~}"
          (loop with n := 0
                for rest on objects
                for head := (car rest)
                for tail := (cdr rest)
                append
                (loop for second in tail
                      do (incf n)
                      collect (list name n head second)))))

(defun define-state-list (states)
  (format nil "~{~a~%~}"
          (list
            (define-list-predicate "isstate" states)
            (define-distinctness
              "state" (append (list "minimal" "maximal") states))
            "fof(possiblestates, axiom, ![T,A]: (A!=maximal => isstate(activestate(T,A))))."
            )))

(defun define-state-transition (state condition ok-step fail-step)
  (if (find condition (list t nil))
    (format 
      nil 
      "fof(step_~a, axiom, 
      ![T,A]: ((A=activeagent(T)&~a=activestate(T,A))=>(activestate(nextmoment(T),A)=~a)))."
            state state (if condition ok-step fail-step))
    (format 
      nil 
      "fof(step_~a, axiom,
      ![T,A]: ((A=activeagent(T)&~a=activestate(T,A))=>
      (((~a)&activestate(nextmoment(T),A)=~a)|((~~(~a))&activestate(nextmoment(T),A)=~a))))."
    state state
    condition ok-step
    condition fail-step)))

(defun define-state-transitions (l)
  (format nil "~{~a~%~}"
          (loop for tr in l collect (apply 'define-state-transition tr))))

(defun assignment-condition (arguments assignment)
  (let* ((state (first assignment))
         (values (second assignment))
         (preconditions (third assignment))
         (guards (mapcar (lambda (k v) (format nil "~a = ~a" k v))
                         arguments values)))
    (format nil "activestate(T,activeagent(T))=~a ~{& ~a~}"
            state (append guards preconditions))))

(defun some-assignment-condition (arguments assignments)
  (tptp-multiop "|"
                (loop for a in assignments
                      collect (assignment-condition arguments a))))

(defun define-value-transition (function arguments assignments)
  (format
    nil
    "fof(~a_step, axiom,
    ![T~{,~a~}]: 
    ((~~(~a) & ~a(T~{,~a~})=~a(nextmoment(T)~{,~a~}))~{~%| ((~a) & ~a(nextmoment(T)~{,~a~})=~a) ~}))."
    function arguments
    (some-assignment-condition arguments assignments) function arguments function arguments
    (loop for a in assignments
          collect (assignment-condition arguments a)
          collect function
          collect arguments
          collect (fourth a))))
