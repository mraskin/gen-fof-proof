fof(leq_reflexive, axiom, ![X]: leq(X,X)).

fof(leq_antisymmetric, axiom, ![X,Y]: ((leq(X,Y)&leq(Y,X))=>X=Y)).

fof(leq_transitive, axiom, ![X,Y,Z]: ((leq(X,Y)&leq(Y,Z))=>leq(X,Z))).

fof(leq_linear, axiom, ![X,Y]: (leq(X,Y)|leq(Y,X))).


fof(type_list, axiom, (![X]:~(is_initial(X)&is_moment(X))) & (![X]:~(is_initial(X)&is_agent(X))) & (![X]:~(is_initial(X)&is_state(X))) & (![X]:~(is_initial(X)&is_failure(X))) & (![X]:~(is_initial(X)&is_agent_set(X))) & (![X]:~(is_moment(X)&is_agent(X))) & (![X]:~(is_moment(X)&is_state(X))) & (![X]:~(is_moment(X)&is_failure(X))) & (![X]:~(is_moment(X)&is_agent_set(X))) & (![X]:~(is_agent(X)&is_state(X))) & (![X]:~(is_agent(X)&is_failure(X))) & (![X]:~(is_agent(X)&is_agent_set(X))) & (![X]:~(is_state(X)&is_failure(X))) & (![X]:~(is_state(X)&is_agent_set(X))) & (![X]:~(is_failure(X)&is_agent_set(X)))).

fof(failure_definition, axiom, ![X]: (is_failure(X)<=>((X = failure)))).
fof(initial_definition, axiom, ![X]: (is_initial(X)<=>((X = initial)))).
fof(freeze_is_moment, axiom, is_moment(freeze)).

fof(type_order_initial_leq_moment, axiom, ![X,Y]: ((is_initial(X)&is_moment(Y))=>leq(X,Y))).

fof(type_order_initial_leq_agent, axiom, ![X,Y]: ((is_initial(X)&is_agent(Y))=>leq(X,Y))).

fof(type_order_initial_leq_state, axiom, ![X,Y]: ((is_initial(X)&is_state(Y))=>leq(X,Y))).

fof(type_order_initial_leq_failure, axiom, ![X,Y]: ((is_initial(X)&is_failure(Y))=>leq(X,Y))).

fof(type_order_moment_leq_agent, axiom, ![X,Y]: ((is_moment(X)&is_agent(Y))=>leq(X,Y))).

fof(type_order_moment_leq_state, axiom, ![X,Y]: ((is_moment(X)&is_state(Y))=>leq(X,Y))).

fof(type_order_moment_leq_failure, axiom, ![X,Y]: ((is_moment(X)&is_failure(Y))=>leq(X,Y))).

fof(type_order_agent_leq_state, axiom, ![X,Y]: ((is_agent(X)&is_state(Y))=>leq(X,Y))).

fof(type_order_agent_leq_failure, axiom, ![X,Y]: ((is_agent(X)&is_failure(Y))=>leq(X,Y))).

fof(type_order_state_leq_failure, axiom, ![X,Y]: ((is_state(X)&is_failure(Y))=>leq(X,Y))).


fof(next_moment_type, axiom, ![X]: (((is_moment(X)) & is_moment(next_moment(X)))|next_moment(X)=failure)).


fof(next_moment_monotonous, axiom, ![X,Y]: ((moment(X)&moment(Y)) => ((leq(X,Y)=>leq(next_moment(X),next_moment(Y)))))).

fof(next_moment_increasing, axiom, ![X]: (moment(X)=>leq(X,next_moment(X)))).

fof(next_moment_exhaustive, axiom, ![X,Y]: ((is_moment(X)&is_moment(Y)&leq(X,Y)&leq(Y,next_moment(X)))=>(X=Y | next_moment(X)=Y))).


fof(freeze_is_frozen, axiom, next_moment(freeze)=freeze).

fof(freeze_is_last_moment, axiom, ![T]: (is_moment(T)=>leq(T,freeze))).

fof(next_moment_is_total, axiom, ![T]: (is_moment(T)=>is_moment(next_moment(T)))).

fof(agent_or_initial_definition, axiom, ![X]: (is_agent_or_initial(X)<=>((is_agent(X)) | (is_initial(X))))).

fof(agent_or_initial_or_failure_definition, axiom, ![X]: (is_agent_or_initial_or_failure(X)<=>((is_agent(X)) | (is_initial(X)) | (is_failure(X))))).

fof(set_definition, axiom, ![X]: (is_set(X)<=>((is_agent_set(X))))).

fof(next_agent_type, axiom, ![X]: (((is_agent_or_initial_or_failure(X)) & is_agent_or_initial_or_failure(next_agent(X)))|next_agent(X)=failure)).


fof(next_agent_monotonous, axiom, ![X,Y]: ((agent_or_initial_or_failure(X)&agent_or_initial_or_failure(Y)) => ((leq(X,Y)=>leq(next_agent(X),next_agent(Y)))))).

fof(next_agent_increasing, axiom, ![X]: (agent_or_initial_or_failure(X)=>leq(X,next_agent(X)))).

fof(next_agent_exhaustive, axiom, ![X,Y]: ((is_agent_or_initial_or_failure(X)&is_agent_or_initial_or_failure(Y)&leq(X,Y)&leq(Y,next_agent(X)))=>(X=Y | next_agent(X)=Y))).


fof(initial_agent, axiom, is_agent(next_agent(initial))).

fof(active_agent_type, axiom, ![T]: (((is_moment(T)) & is_agent_or_initial(active_agent(T)))|active_agent(T)=failure)).

fof(active_agent_totality, axiom, ![T]: (((is_moment(T))) => is_agent_or_initial(active_agent(T)))).


fof(active_state_type, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A)) & is_state(active_state(T,A)))|active_state(T,A)=failure)).

fof(active_state_totality, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A))) => is_state(active_state(T,A)))).


fof(agent_set_typing, axiom, ![S, E]: ((is_agent_set(S) & is_member_of(S, E)) => is_agent(E))).

fof(agent_set_complete_object, definition, ![E]: ((is_agent(E)) <=> is_member_of(complete_agent_set, E))).

fof(agent_set_type, axiom, ![S, E]: ((is_agent(E) & is_member_of(S, E)) => is_agent_set(S))).

fof(agent_set_choice_function_type, axiom, ![S]: ((is_agent_set(S)) => (is_agent_or_failure(choice_agent_set(S))))).

fof(agent_set_choice_function_member_of_set, axiom, ![S]: (((is_agent_set(S)) & (choice_agent_set(S) != failure)) => (is_member_of(S, choice_agent_set(S))))).

fof(agent_set_choice_function_mistyped, axiom, ![S]: ((~is_agent_set(S)) => (choice_agent_set(S) = failure))).

fof(agent_set_choice_function_welltyped_failure, definition, ![S]: ((is_agent_set(S)) => ((choice_agent_set(S) = failure) <=> (![E]: is_member_of(S,E))))).

fof(agent_set_timed_choice_function_type, axiom, ![S, T]: ((is_agent_set(S) & is_moment(T)) => (is_agent_or_failure(timed_choice_agent_set(T, S))))).

fof(agent_set_timed_choice_function_member_of_set, axiom, ![S, T]: (((is_agent_set(S) & is_moment(T)) & (timed_choice_agent_set(T, S) != failure)) => (is_member_of(S, timed_choice_agent_set(T, S))))).

fof(agent_set_timed_choice_function_mistyped, axiom, ![S, T]: ((~is_agent_set(S) | ~is_moment(T)) => (timed_choice_agent_set(T, S) = failure))).

fof(agent_set_timed_choice_function_welltyped_failure, axiom, ![S, T]: ((is_agent_set(S) & is_moment(T)) => ((timed_choice_agent_set(T, S) = failure) <=> (![E]: s_member_of(S,E))))).


fof(remove_function_for_sets, definition, ![S1, E1, S2]:
                          (remove_member(S1, E1) = S2 <=>
                          (![E2]: ((E2 = E1 & ~is_member_of(S2, E1)) |
                                   (E2 != E1 & (is_member_of(S2, E2) <=> is_member_of(S1, E2))))))).

fof(empty_predicate, definition, ![S]: (is_empty(S) <=> (is_set(S) & ![E]:
                                                   (~is_member_of(S, E))))).

fof(state_definition, axiom, ![X]: (is_state(X)<=>((X = initialState) | (X = forbidSteal) | (X = checkTurn) | (X = failureToEnter) | (X = grabTurn) | (X = cannotGrab) | (X = declareEntry) | (X = startCheck) | (X = selfCheck) | (X = collisionCheck) | (X = step) | (X = stepCheck) | (X = criticalSection) | (X = yieldTurn) | (X = declareExit) | (X = permitSteal)))).
fof(object_list_distinct_1, axiom, initial!=failure).
fof(object_list_distinct_2, axiom, initial!=freeze).
fof(object_list_distinct_3, axiom, initial!=initialState).
fof(object_list_distinct_4, axiom, initial!=forbidSteal).
fof(object_list_distinct_5, axiom, initial!=checkTurn).
fof(object_list_distinct_6, axiom, initial!=failureToEnter).
fof(object_list_distinct_7, axiom, initial!=grabTurn).
fof(object_list_distinct_8, axiom, initial!=cannotGrab).
fof(object_list_distinct_9, axiom, initial!=declareEntry).
fof(object_list_distinct_10, axiom, initial!=startCheck).
fof(object_list_distinct_11, axiom, initial!=selfCheck).
fof(object_list_distinct_12, axiom, initial!=collisionCheck).
fof(object_list_distinct_13, axiom, initial!=step).
fof(object_list_distinct_14, axiom, initial!=stepCheck).
fof(object_list_distinct_15, axiom, initial!=criticalSection).
fof(object_list_distinct_16, axiom, initial!=yieldTurn).
fof(object_list_distinct_17, axiom, initial!=declareExit).
fof(object_list_distinct_18, axiom, initial!=permitSteal).
fof(object_list_distinct_19, axiom, initial!=true).
fof(object_list_distinct_20, axiom, initial!=false).
fof(object_list_distinct_21, axiom, initial!=complete_agent_set).
fof(object_list_distinct_22, axiom, failure!=freeze).
fof(object_list_distinct_23, axiom, failure!=initialState).
fof(object_list_distinct_24, axiom, failure!=forbidSteal).
fof(object_list_distinct_25, axiom, failure!=checkTurn).
fof(object_list_distinct_26, axiom, failure!=failureToEnter).
fof(object_list_distinct_27, axiom, failure!=grabTurn).
fof(object_list_distinct_28, axiom, failure!=cannotGrab).
fof(object_list_distinct_29, axiom, failure!=declareEntry).
fof(object_list_distinct_30, axiom, failure!=startCheck).
fof(object_list_distinct_31, axiom, failure!=selfCheck).
fof(object_list_distinct_32, axiom, failure!=collisionCheck).
fof(object_list_distinct_33, axiom, failure!=step).
fof(object_list_distinct_34, axiom, failure!=stepCheck).
fof(object_list_distinct_35, axiom, failure!=criticalSection).
fof(object_list_distinct_36, axiom, failure!=yieldTurn).
fof(object_list_distinct_37, axiom, failure!=declareExit).
fof(object_list_distinct_38, axiom, failure!=permitSteal).
fof(object_list_distinct_39, axiom, failure!=true).
fof(object_list_distinct_40, axiom, failure!=false).
fof(object_list_distinct_41, axiom, failure!=complete_agent_set).
fof(object_list_distinct_42, axiom, freeze!=initialState).
fof(object_list_distinct_43, axiom, freeze!=forbidSteal).
fof(object_list_distinct_44, axiom, freeze!=checkTurn).
fof(object_list_distinct_45, axiom, freeze!=failureToEnter).
fof(object_list_distinct_46, axiom, freeze!=grabTurn).
fof(object_list_distinct_47, axiom, freeze!=cannotGrab).
fof(object_list_distinct_48, axiom, freeze!=declareEntry).
fof(object_list_distinct_49, axiom, freeze!=startCheck).
fof(object_list_distinct_50, axiom, freeze!=selfCheck).
fof(object_list_distinct_51, axiom, freeze!=collisionCheck).
fof(object_list_distinct_52, axiom, freeze!=step).
fof(object_list_distinct_53, axiom, freeze!=stepCheck).
fof(object_list_distinct_54, axiom, freeze!=criticalSection).
fof(object_list_distinct_55, axiom, freeze!=yieldTurn).
fof(object_list_distinct_56, axiom, freeze!=declareExit).
fof(object_list_distinct_57, axiom, freeze!=permitSteal).
fof(object_list_distinct_58, axiom, freeze!=true).
fof(object_list_distinct_59, axiom, freeze!=false).
fof(object_list_distinct_60, axiom, freeze!=complete_agent_set).
fof(object_list_distinct_61, axiom, initialState!=forbidSteal).
fof(object_list_distinct_62, axiom, initialState!=checkTurn).
fof(object_list_distinct_63, axiom, initialState!=failureToEnter).
fof(object_list_distinct_64, axiom, initialState!=grabTurn).
fof(object_list_distinct_65, axiom, initialState!=cannotGrab).
fof(object_list_distinct_66, axiom, initialState!=declareEntry).
fof(object_list_distinct_67, axiom, initialState!=startCheck).
fof(object_list_distinct_68, axiom, initialState!=selfCheck).
fof(object_list_distinct_69, axiom, initialState!=collisionCheck).
fof(object_list_distinct_70, axiom, initialState!=step).
fof(object_list_distinct_71, axiom, initialState!=stepCheck).
fof(object_list_distinct_72, axiom, initialState!=criticalSection).
fof(object_list_distinct_73, axiom, initialState!=yieldTurn).
fof(object_list_distinct_74, axiom, initialState!=declareExit).
fof(object_list_distinct_75, axiom, initialState!=permitSteal).
fof(object_list_distinct_76, axiom, initialState!=true).
fof(object_list_distinct_77, axiom, initialState!=false).
fof(object_list_distinct_78, axiom, initialState!=complete_agent_set).
fof(object_list_distinct_79, axiom, forbidSteal!=checkTurn).
fof(object_list_distinct_80, axiom, forbidSteal!=failureToEnter).
fof(object_list_distinct_81, axiom, forbidSteal!=grabTurn).
fof(object_list_distinct_82, axiom, forbidSteal!=cannotGrab).
fof(object_list_distinct_83, axiom, forbidSteal!=declareEntry).
fof(object_list_distinct_84, axiom, forbidSteal!=startCheck).
fof(object_list_distinct_85, axiom, forbidSteal!=selfCheck).
fof(object_list_distinct_86, axiom, forbidSteal!=collisionCheck).
fof(object_list_distinct_87, axiom, forbidSteal!=step).
fof(object_list_distinct_88, axiom, forbidSteal!=stepCheck).
fof(object_list_distinct_89, axiom, forbidSteal!=criticalSection).
fof(object_list_distinct_90, axiom, forbidSteal!=yieldTurn).
fof(object_list_distinct_91, axiom, forbidSteal!=declareExit).
fof(object_list_distinct_92, axiom, forbidSteal!=permitSteal).
fof(object_list_distinct_93, axiom, forbidSteal!=true).
fof(object_list_distinct_94, axiom, forbidSteal!=false).
fof(object_list_distinct_95, axiom, forbidSteal!=complete_agent_set).
fof(object_list_distinct_96, axiom, checkTurn!=failureToEnter).
fof(object_list_distinct_97, axiom, checkTurn!=grabTurn).
fof(object_list_distinct_98, axiom, checkTurn!=cannotGrab).
fof(object_list_distinct_99, axiom, checkTurn!=declareEntry).
fof(object_list_distinct_100, axiom, checkTurn!=startCheck).
fof(object_list_distinct_101, axiom, checkTurn!=selfCheck).
fof(object_list_distinct_102, axiom, checkTurn!=collisionCheck).
fof(object_list_distinct_103, axiom, checkTurn!=step).
fof(object_list_distinct_104, axiom, checkTurn!=stepCheck).
fof(object_list_distinct_105, axiom, checkTurn!=criticalSection).
fof(object_list_distinct_106, axiom, checkTurn!=yieldTurn).
fof(object_list_distinct_107, axiom, checkTurn!=declareExit).
fof(object_list_distinct_108, axiom, checkTurn!=permitSteal).
fof(object_list_distinct_109, axiom, checkTurn!=true).
fof(object_list_distinct_110, axiom, checkTurn!=false).
fof(object_list_distinct_111, axiom, checkTurn!=complete_agent_set).
fof(object_list_distinct_112, axiom, failureToEnter!=grabTurn).
fof(object_list_distinct_113, axiom, failureToEnter!=cannotGrab).
fof(object_list_distinct_114, axiom, failureToEnter!=declareEntry).
fof(object_list_distinct_115, axiom, failureToEnter!=startCheck).
fof(object_list_distinct_116, axiom, failureToEnter!=selfCheck).
fof(object_list_distinct_117, axiom, failureToEnter!=collisionCheck).
fof(object_list_distinct_118, axiom, failureToEnter!=step).
fof(object_list_distinct_119, axiom, failureToEnter!=stepCheck).
fof(object_list_distinct_120, axiom, failureToEnter!=criticalSection).
fof(object_list_distinct_121, axiom, failureToEnter!=yieldTurn).
fof(object_list_distinct_122, axiom, failureToEnter!=declareExit).
fof(object_list_distinct_123, axiom, failureToEnter!=permitSteal).
fof(object_list_distinct_124, axiom, failureToEnter!=true).
fof(object_list_distinct_125, axiom, failureToEnter!=false).
fof(object_list_distinct_126, axiom, failureToEnter!=complete_agent_set).
fof(object_list_distinct_127, axiom, grabTurn!=cannotGrab).
fof(object_list_distinct_128, axiom, grabTurn!=declareEntry).
fof(object_list_distinct_129, axiom, grabTurn!=startCheck).
fof(object_list_distinct_130, axiom, grabTurn!=selfCheck).
fof(object_list_distinct_131, axiom, grabTurn!=collisionCheck).
fof(object_list_distinct_132, axiom, grabTurn!=step).
fof(object_list_distinct_133, axiom, grabTurn!=stepCheck).
fof(object_list_distinct_134, axiom, grabTurn!=criticalSection).
fof(object_list_distinct_135, axiom, grabTurn!=yieldTurn).
fof(object_list_distinct_136, axiom, grabTurn!=declareExit).
fof(object_list_distinct_137, axiom, grabTurn!=permitSteal).
fof(object_list_distinct_138, axiom, grabTurn!=true).
fof(object_list_distinct_139, axiom, grabTurn!=false).
fof(object_list_distinct_140, axiom, grabTurn!=complete_agent_set).
fof(object_list_distinct_141, axiom, cannotGrab!=declareEntry).
fof(object_list_distinct_142, axiom, cannotGrab!=startCheck).
fof(object_list_distinct_143, axiom, cannotGrab!=selfCheck).
fof(object_list_distinct_144, axiom, cannotGrab!=collisionCheck).
fof(object_list_distinct_145, axiom, cannotGrab!=step).
fof(object_list_distinct_146, axiom, cannotGrab!=stepCheck).
fof(object_list_distinct_147, axiom, cannotGrab!=criticalSection).
fof(object_list_distinct_148, axiom, cannotGrab!=yieldTurn).
fof(object_list_distinct_149, axiom, cannotGrab!=declareExit).
fof(object_list_distinct_150, axiom, cannotGrab!=permitSteal).
fof(object_list_distinct_151, axiom, cannotGrab!=true).
fof(object_list_distinct_152, axiom, cannotGrab!=false).
fof(object_list_distinct_153, axiom, cannotGrab!=complete_agent_set).
fof(object_list_distinct_154, axiom, declareEntry!=startCheck).
fof(object_list_distinct_155, axiom, declareEntry!=selfCheck).
fof(object_list_distinct_156, axiom, declareEntry!=collisionCheck).
fof(object_list_distinct_157, axiom, declareEntry!=step).
fof(object_list_distinct_158, axiom, declareEntry!=stepCheck).
fof(object_list_distinct_159, axiom, declareEntry!=criticalSection).
fof(object_list_distinct_160, axiom, declareEntry!=yieldTurn).
fof(object_list_distinct_161, axiom, declareEntry!=declareExit).
fof(object_list_distinct_162, axiom, declareEntry!=permitSteal).
fof(object_list_distinct_163, axiom, declareEntry!=true).
fof(object_list_distinct_164, axiom, declareEntry!=false).
fof(object_list_distinct_165, axiom, declareEntry!=complete_agent_set).
fof(object_list_distinct_166, axiom, startCheck!=selfCheck).
fof(object_list_distinct_167, axiom, startCheck!=collisionCheck).
fof(object_list_distinct_168, axiom, startCheck!=step).
fof(object_list_distinct_169, axiom, startCheck!=stepCheck).
fof(object_list_distinct_170, axiom, startCheck!=criticalSection).
fof(object_list_distinct_171, axiom, startCheck!=yieldTurn).
fof(object_list_distinct_172, axiom, startCheck!=declareExit).
fof(object_list_distinct_173, axiom, startCheck!=permitSteal).
fof(object_list_distinct_174, axiom, startCheck!=true).
fof(object_list_distinct_175, axiom, startCheck!=false).
fof(object_list_distinct_176, axiom, startCheck!=complete_agent_set).
fof(object_list_distinct_177, axiom, selfCheck!=collisionCheck).
fof(object_list_distinct_178, axiom, selfCheck!=step).
fof(object_list_distinct_179, axiom, selfCheck!=stepCheck).
fof(object_list_distinct_180, axiom, selfCheck!=criticalSection).
fof(object_list_distinct_181, axiom, selfCheck!=yieldTurn).
fof(object_list_distinct_182, axiom, selfCheck!=declareExit).
fof(object_list_distinct_183, axiom, selfCheck!=permitSteal).
fof(object_list_distinct_184, axiom, selfCheck!=true).
fof(object_list_distinct_185, axiom, selfCheck!=false).
fof(object_list_distinct_186, axiom, selfCheck!=complete_agent_set).
fof(object_list_distinct_187, axiom, collisionCheck!=step).
fof(object_list_distinct_188, axiom, collisionCheck!=stepCheck).
fof(object_list_distinct_189, axiom, collisionCheck!=criticalSection).
fof(object_list_distinct_190, axiom, collisionCheck!=yieldTurn).
fof(object_list_distinct_191, axiom, collisionCheck!=declareExit).
fof(object_list_distinct_192, axiom, collisionCheck!=permitSteal).
fof(object_list_distinct_193, axiom, collisionCheck!=true).
fof(object_list_distinct_194, axiom, collisionCheck!=false).
fof(object_list_distinct_195, axiom, collisionCheck!=complete_agent_set).
fof(object_list_distinct_196, axiom, step!=stepCheck).
fof(object_list_distinct_197, axiom, step!=criticalSection).
fof(object_list_distinct_198, axiom, step!=yieldTurn).
fof(object_list_distinct_199, axiom, step!=declareExit).
fof(object_list_distinct_200, axiom, step!=permitSteal).
fof(object_list_distinct_201, axiom, step!=true).
fof(object_list_distinct_202, axiom, step!=false).
fof(object_list_distinct_203, axiom, step!=complete_agent_set).
fof(object_list_distinct_204, axiom, stepCheck!=criticalSection).
fof(object_list_distinct_205, axiom, stepCheck!=yieldTurn).
fof(object_list_distinct_206, axiom, stepCheck!=declareExit).
fof(object_list_distinct_207, axiom, stepCheck!=permitSteal).
fof(object_list_distinct_208, axiom, stepCheck!=true).
fof(object_list_distinct_209, axiom, stepCheck!=false).
fof(object_list_distinct_210, axiom, stepCheck!=complete_agent_set).
fof(object_list_distinct_211, axiom, criticalSection!=yieldTurn).
fof(object_list_distinct_212, axiom, criticalSection!=declareExit).
fof(object_list_distinct_213, axiom, criticalSection!=permitSteal).
fof(object_list_distinct_214, axiom, criticalSection!=true).
fof(object_list_distinct_215, axiom, criticalSection!=false).
fof(object_list_distinct_216, axiom, criticalSection!=complete_agent_set).
fof(object_list_distinct_217, axiom, yieldTurn!=declareExit).
fof(object_list_distinct_218, axiom, yieldTurn!=permitSteal).
fof(object_list_distinct_219, axiom, yieldTurn!=true).
fof(object_list_distinct_220, axiom, yieldTurn!=false).
fof(object_list_distinct_221, axiom, yieldTurn!=complete_agent_set).
fof(object_list_distinct_222, axiom, declareExit!=permitSteal).
fof(object_list_distinct_223, axiom, declareExit!=true).
fof(object_list_distinct_224, axiom, declareExit!=false).
fof(object_list_distinct_225, axiom, declareExit!=complete_agent_set).
fof(object_list_distinct_226, axiom, permitSteal!=true).
fof(object_list_distinct_227, axiom, permitSteal!=false).
fof(object_list_distinct_228, axiom, permitSteal!=complete_agent_set).
fof(object_list_distinct_229, axiom, true!=false).
fof(object_list_distinct_230, axiom, true!=complete_agent_set).
fof(object_list_distinct_231, axiom, false!=complete_agent_set).


fof(boolean_definition, axiom, ![X]: (is_boolean(X)<=>((X = true) | (X = false)))).
fof(other_agents_type, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A)) & is_agent_set(other_agents(T,A)))|other_agents(T,A)=failure)).


fof(other_agents_structure_at_next_moment_1, axiom, ![T,A]: (((((~is_moment(next_moment(T))) | (~is_agent(A))))) => other_agents(next_moment(T),A)=failure)).

fof(other_agents_structure_at_next_moment_2, axiom, ![T,A]: (((~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_agent(T)!=A))) => other_agents(next_moment(T),A)=other_agents(T,A))).

fof(other_agents_structure_at_next_moment_3, axiom, ![T,A]: (((~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A) = step))) => other_agents(next_moment(T),A)=remove_member(other_agents(T, A), choice_agent_set(other_agents(T, A))))).

fof(other_agents_structure_at_next_moment_4, axiom, ![T,A]: (((~((active_state(T,A) = step))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A) = startCheck))) => other_agents(next_moment(T),A)=complete_agent_set)).

fof(other_agents_structure_at_next_moment_5, axiom, ![T,A]: (((~((active_state(T,A) = startCheck))) & (~((active_state(T,A) = step))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A)))))) => other_agents(next_moment(T),A)=other_agents(T,A))).



fof(turn_type, axiom, ![T]: (((is_moment(T)) & is_agent_or_initial(turn(T)))|turn(T)=failure)).

fof(turn_totality, axiom, ![T]: (((is_moment(T))) => is_agent_or_initial(turn(T)))).


fof(turn_structure_at_next_moment_1, axiom, ![T]: (((((~is_moment(next_moment(T)))))) => turn(next_moment(T))=failure)).

fof(turn_structure_at_next_moment_2, axiom, ![T]: (((~(((~is_moment(next_moment(T)))))) & ((active_state(T, active_agent(T)) = grabTurn))) => turn(next_moment(T))=active_agent(T))).

fof(turn_structure_at_next_moment_3, axiom, ![T]: (((~((active_state(T, active_agent(T)) = grabTurn))) & (~(((~is_moment(next_moment(T)))))) & ((active_state(T, active_agent(T)) = yieldTurn))) => turn(next_moment(T))=initial)).

fof(turn_structure_at_next_moment_4, axiom, ![T]: (((~((active_state(T, active_agent(T)) = yieldTurn))) & (~((active_state(T, active_agent(T)) = grabTurn))) & (~(((~is_moment(next_moment(T))))))) => turn(next_moment(T))=turn(T))).



fof(stealable_type, axiom, ![T,A]: (((is_moment(T)) & (is_agent_or_initial(A)) & is_boolean(stealable(T,A)))|stealable(T,A)=failure)).

fof(stealable_totality, axiom, ![T,A]: (((is_moment(T)) & (is_agent_or_initial(A))) => is_boolean(stealable(T,A)))).


fof(stealable_structure_at_next_moment_1, axiom, ![T,A]: (((((~is_moment(next_moment(T))) | (~is_agent_or_initial(A))))) => stealable(next_moment(T),A)=failure)).

fof(stealable_structure_at_next_moment_2, axiom, ![T,A]: (((~(((~is_moment(next_moment(T))) | (~is_agent_or_initial(A))))) & ((active_agent(T)!=A))) => stealable(next_moment(T),A)=stealable(T,A))).

fof(stealable_structure_at_next_moment_3, axiom, ![T,A]: (((~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent_or_initial(A))))) & ((active_state(T,A) = forbidSteal))) => stealable(next_moment(T),A)=false)).

fof(stealable_structure_at_next_moment_4, axiom, ![T,A]: (((~((active_state(T,A) = forbidSteal))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent_or_initial(A))))) & ((active_state(T,A) = permitSteal))) => stealable(next_moment(T),A)=true)).

fof(stealable_structure_at_next_moment_5, axiom, ![T,A]: (((~((active_state(T,A) = permitSteal))) & (~((active_state(T,A) = forbidSteal))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent_or_initial(A)))))) => stealable(next_moment(T),A)=stealable(T,A))).



fof(outside_type, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A)) & is_boolean(outside(T,A)))|outside(T,A)=failure)).

fof(outside_totality, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A))) => is_boolean(outside(T,A)))).


fof(outside_structure_at_next_moment_1, axiom, ![T,A]: (((((~is_moment(next_moment(T))) | (~is_agent(A))))) => outside(next_moment(T),A)=failure)).

fof(outside_structure_at_next_moment_2, axiom, ![T,A]: (((~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_agent(T)!=A))) => outside(next_moment(T),A)=outside(T,A))).

fof(outside_structure_at_next_moment_3, axiom, ![T,A]: (((~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A) = failureToEnter))) => outside(next_moment(T),A)=true)).

fof(outside_structure_at_next_moment_4, axiom, ![T,A]: (((~((active_state(T,A) = failureToEnter))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A) = declareEntry))) => outside(next_moment(T),A)=false)).

fof(outside_structure_at_next_moment_5, axiom, ![T,A]: (((~((active_state(T,A) = declareEntry))) & (~((active_state(T,A) = failureToEnter))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A) = declareExit))) => outside(next_moment(T),A)=true)).

fof(outside_structure_at_next_moment_6, axiom, ![T,A]: (((~((active_state(T,A) = declareExit))) & (~((active_state(T,A) = declareEntry))) & (~((active_state(T,A) = failureToEnter))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A)))))) => outside(next_moment(T),A)=outside(T,A))).



fof(active_state_step_0, axiom, ![T,A]: ((is_moment(T) & is_agent(A) & active_agent(T)!=A)
      => active_state(next_moment(T),A) = active_state(T,A))).

fof(active_state_step_1, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=initialState) => active_state(next_moment(T),A)=forbidSteal)).

fof(active_state_step_2, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=forbidSteal) => active_state(next_moment(T),A)=checkTurn)).

fof(active_state_step_3_ok, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=checkTurn & turn(T)=A)
                            => active_state(next_moment(T),A)=declareEntry)).

fof(active_state_step_3_fail, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=checkTurn & ~ turn(T)=A)
                            =>
                            active_state(next_moment(T),A)=failureToEnter)).

fof(active_state_step_4_ok, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=failureToEnter & stealable(T, turn(T))=true)
                            => active_state(next_moment(T),A)=grabTurn)).

fof(active_state_step_4_fail, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=failureToEnter & ~ stealable(T, turn(T))=true)
                            =>
                            active_state(next_moment(T),A)=cannotGrab)).

fof(active_state_step_5, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=grabTurn) => active_state(next_moment(T),A)=cannotGrab)).

fof(active_state_step_6, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=cannotGrab) => active_state(next_moment(T),A)=checkTurn)).

fof(active_state_step_7, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=declareEntry) => active_state(next_moment(T),A)=startCheck)).

fof(active_state_step_8, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=startCheck) => active_state(next_moment(T),A)=selfCheck)).

fof(active_state_step_9_ok, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=selfCheck & choice_agent_set(other_agents(T, A)) != A)
                            => active_state(next_moment(T),A)=collisionCheck)).

fof(active_state_step_9_fail, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=selfCheck & ~ choice_agent_set(other_agents(T, A)) != A)
                            =>
                            active_state(next_moment(T),A)=step)).

fof(active_state_step_10_ok, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=collisionCheck & outside(T,choice_agent_set(other_agents(T, A)))=true)
                            => active_state(next_moment(T),A)=step)).

fof(active_state_step_10_fail, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=collisionCheck & ~ outside(T,choice_agent_set(other_agents(T, A)))=true)
                            =>
                            active_state(next_moment(T),A)=checkTurn)).

fof(active_state_step_11, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=step) => active_state(next_moment(T),A)=stepCheck)).

fof(active_state_step_12_ok, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=stepCheck & is_empty(other_agents(T, A)))
                            => active_state(next_moment(T),A)=criticalSection)).

fof(active_state_step_12_fail, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=stepCheck & ~ is_empty(other_agents(T, A)))
                            =>
                            active_state(next_moment(T),A)=selfCheck)).

fof(active_state_step_13, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=criticalSection) => active_state(next_moment(T),A)=yieldTurn)).

fof(active_state_step_14, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=yieldTurn) => active_state(next_moment(T),A)=declareExit)).

fof(active_state_step_15, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=declareExit) => active_state(next_moment(T),A)=permitSteal)).

fof(active_state_step_16, axiom, ![T,A]: ((is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=permitSteal) => active_state(next_moment(T),A)=initialState)).


fof(initial_stealable, axiom, ![T]:(is_moment(T) => stealable(T, initial) = true)).

