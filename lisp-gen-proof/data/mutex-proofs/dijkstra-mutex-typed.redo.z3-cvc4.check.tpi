include('dijkstra-mutex-typed-2.gen.ax').
include('prover-setup-1.tpi').

fof(define_safety_for, checked_definition,
  ![T,A1,A2]: (safe_for(T,A1,A2)<=>(
    (active_state(T,A1)=criticalSection & active_state(T,A2)=criticalSection)
       => A1=A2))).

fof(define_safety, checked_definition,
  ![T]: (safe(T)<=>(![A1,A2]: safe_for(T,A1,A2)))).

fof(define_counter_scope, checked_definition,
  ![T,A]: (counter_scope(T,A) <=> (
    is_moment(T) & is_agent(A) &
    (
    active_state(T,A) = selfCheck |
    active_state(T,A) = stepCheck |
    active_state(T,A) = collisionCheck |
    active_state(T,A) = step |
    active_state(T,A) = criticalSection
    )
  )
  )
).

fof(define_instant_agent_pair, checked_definition,
  ![T,A,B]: (
    instant_agent_pair(T, A, B) <=> (is_moment(T) & is_agent(A) & is_agent(B)
    & A!=B)
  )
).

fof(define_passed, checked_definition,
  ![T,A,B]: (passed(T,A,B)<=>(
    instant_agent_pair(T,A,B) & counter_scope(T, A) &
    (leq(B,counter(T,A))) & (B=counter(T,A) => active_state(T,A)=step)
  ))).

fof(define_passed_exclusive_for, checked_definition,
  ![T, A, B]: (passed_exclusive_for(T, A, B) <=>
     ~(passed(T,A,B) & passed(T,B,A)))).

fof(define_passed_exclusive, checked_definition,
  ![T]: (passed_exclusive(T) <=> (![A, B]: (passed_exclusive_for(T, A, B))))).

fof(define_passed_in_critical_for, checked_definition,
  ![T, A]: (passed_in_critical_for(T, A) <=>
    ![B]: (
      (
        active_state(T, A) = criticalSection
        & instant_agent_pair(T, A, B)
      ) => (
        passed(T, A, B)
      )
    )
  )
).

fof(define_passed_in_critical, checked_definition,
  ![T]: (passed_in_critical(T) <=> ![A]: (passed_in_critical_for(T, A)))).

fof(define_inside, checked_definition,
  ![T,A]: (inside(T,A) <=> (
    is_moment(T) & is_agent(A) &
    (
    active_state(T,A) = startCheck |
    active_state(T,A) = selfCheck |
    active_state(T,A) = stepCheck |
    active_state(T,A) = collisionCheck |
    active_state(T,A) = step |
    active_state(T,A) = criticalSection
    )
  )
  )
).

fof(define_inside_correct_for, checked_definition,
  ![T, A]: (
    inside_correct_for(T, A) <=> (inside(T,A) => outside(T,A) = false)
  )
).

fof(define_inside_correct, checked_definition,
  ![T]: (
    inside_correct(T) <=> (![A]: (inside_correct_for(T,A)))
  )
).

%%% The invariant to prove by induction
fof(define_invariant, checked_definition,
  ![T]: (invariant(T)<=>(
     inside_correct(T)
     & passed_exclusive(T)
     & passed_in_critical(T)
  ))).

fof(invariant_safety_local, checked_lemma,
  ![T,A,B]: ((passed_exclusive(T)
               & passed_in_critical(T)
    )=> safe_for(T,A,B))).

tpi(run_checks_for_defs, index_definitions, 'dg_').
tpi(run_checks_for_defs, check_definitions, '-').

fof(invariant_preserved, checked_lemma,
  ![T]: (invariant(T) => invariant(next_moment(T)))).

tpi(ed_invariant_preserved, expand_specific_definitions,
  invariant_preserved(invariant)).

fof(invariant_safety, checked_lemma,
  ![T]: (invariant(T) => safe(T))).

tpi(sc_all, split_all_conjunctions, '-').
tpi(dump, dump, '$getenv(=TPI_DUMP_LOG)').

tpi(run_checks_gen_defs, check_definitions, '-').
tpi(run_checks, check_lemmas_strict, '=PROOF' = '$getenv(TPI_PROVER) $getenv(TPI_PROVER_FILE_PREFIX)%s ').

tpi(report_success, write, 'checks passed!').
tpi(exit, exit, '0').

tpi(dump, dump, stdout).
tpi(trust_me, trust_me, trust_me).
tpi(dump, dump, stdout).
