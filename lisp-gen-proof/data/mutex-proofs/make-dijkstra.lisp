(let* ((filename "data/mutex-proofs/dijkstra-mutex.gen.ax"))
  (with-open-file (f filename :direction :output :if-exists :supersede)
    (format
      f "~{include('~a').~%~}"
      (list "order-model.ax" "time-model.ax" "agent-model.ax" "state-model.ax"))
    (format
      f "~{~a~%~}"
      (list
        (define-state-list
          (list
            "initialState" "forbidSteal"
            "checkTurn" "failureToEnter" "grabTurn" "cannotGrab"
            "declareEntry" "startCheck" "selfCheck" "collisionCheck"
            "step" "stepCheck"
            "criticalSection"
            "yieldTurn" "declareExit" "permitSteal"))
        "fof(zeroagent_state,axiom,
              ![T]: (
                     activestate(T,minimal)=initialState
                     & stealable(T,minimal)=maximal & outside(T,minimal)=maximal
                     & counter(T,minimal)=minimal))."
        (define-value-transition
          "turn" ()
          `(("grabTurn" () () "activeagent(T)")
            ("yieldTurn" () () "minimal")))
        (define-value-transition
          "stealable" `("A")
          `(("forbidSteal" ("activeagent(T)") () "minimal")
            ("permitSteal" ("activeagent(T)") () "maximal")))
        (define-value-transition
          "outside" `("A")
          `(("failureToEnter" ("activeagent(T)") () "maximal")
            ("declareEntry" ("activeagent(T)") () "minimal")
            ("declareExit" ("activeagent(T)") () "maximal")))
        (define-value-transition
          "counter" `("A")
          `(("startCheck" ("activeagent(T)") () "nextagent(minimal)")
            ("step" ("activeagent(T)") () "nextagent(counter(T,A))")))
        (define-state-transitions
          `(("maximal" t "maximal" nil)
            ("initialState" t "forbidSteal" nil)
            ("forbidSteal" t "checkTurn" nil)
            ("checkTurn" "turn(T)=activeagent(T)" "declareEntry" "failureToEnter")
            ("failureToEnter" "stealable(T,activeagent(T))=maximal" "grabTurn" "cannotGrab")
            ("grabTurn" t "cannotGrab" nil)
            ("declareEntry" t "startCheck" nil)
            ("startCheck" t "selfCheck" nil)
            ("selfCheck" "counter(T,activeagent(T))=activeagent(T)" "step" "collisionCheck")
            ("step" t "stepCheck" nil)
            ("stepCheck" "counter(T,activeagent(T))=maximal" "criticalSection" "selfCheck")
            ("collisionCheck" "outside(T,counter(T,activeagent(T)))=maximal" "step" "checkTurn")
            ("criticalSection" t "yieldTurn" nil)
            ("yieldTurn" t "declareExit" nil)
            ("permitSteal" t "initialState" nil)))
        ))))
