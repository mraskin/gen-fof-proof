(let* ((filename "data/mutex-proofs/optimistic-mutex.gen.ax"))
  (alexandria:write-string-into-file
    (tptp-concat
      (make-agent-stub
        (list
          "initialState"
          "step" "stepCheck" "selfCheck" "collisionCheck"
          "criticalSection")
        (list))
      (define-function-type
        "counter" "agent_or_initial_or_failure" "failure"
        (list "T" "A") (list "moment" "agent") :total t)
      (define-function-structure
        "active_state" "step" "failure"
        (list "next_moment(T)" "A") (list "moment" "agent")
        "active_state(T,A)"
        `(
          (("active_agent(T)!=A"))
          ,@(make-transition-assignments
              "active_state(T,A)"
              `(
                ("initialState" t "step" nil)
                ("step" t "stepCheck" nil)
                ("selfCheck" "counter(T,A)!=A" "collisionCheck" "step")
                ("stepCheck" "counter(T,A)!=failure" "selfCheck" "criticalSection")
                ("collisionCheck" "counter(T,counter(T,A))=initial" "step" "initialState")
                ("criticalSection" t "initialState" nil)
                ))
          ))
      (define-function-structure
        "counter" "step" "failure"
        (list "next_moment(T)" "A") (list "moment" "agent")
        "counter(T,A)"
        `(
          (("active_agent(T)!=A"))
          (("active_state(T,A)=step") "next_agent(counter(T,A))")
          (("active_state(T,A)=initialState") "initial")
          ))
      )
    filename :if-exists :supersede))

(flet ((object (name number) (format nil "~a~6,'0d" name number)))
  (let* ((dt 15)
         (na 3)
         (agent-list (loop for k from 1 to na
                           collect (object "a" k)))
         (filename "data/mutex-proofs/optimistic-mutex.gen.example-1.ax"))
    (alexandria:write-string-into-file
      (tptp-concat
        (define-sequence
          "agents" "next_agent"
          `("initial"
            ,@ agent-list
            "failure"))
        (define-sequence
          "time" "next_moment"
          (append
            (loop for k from 0 to dt collect (object "t" k))
            (list "freeze" "freeze")))
        (loop for k from 0 to dt collect
              (tptp-fof-statement (format nil "~a_is_moment"
                                          (object "t" k))
                                  (format nil "is_moment(~a)"
                                          (object "t" k))))

        (define-values "initial_counters" "counter" `(,(object "t" 0))
                       agent-list (mapcar (constantly "initial") agent-list))
        (define-values "initial_states" "active_state" `("t000000")
                       agent-list
                       (mapcar (constantly "initialState") agent-list))
        (define-sticky-values
          "sample_run"
          `(
            ,@ (loop for k from 0 to dt collect
                     `(nil ,(object "t" k)))
            (nil "freeze")
            )
          `(
            (
             ,@(loop for k from 1 to na collect
                     `(("counter" ,(object "a" k)) "initial"))
             ,@(loop for k from 1 to na collect
                     `(("active_state" ,(object "a" k)) "initialState"))
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "step")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "stepCheck")
             (("counter" ,(object "a" 1)) ,(object "a" 1))
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "selfCheck")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "step")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "stepCheck")
             (("counter" ,(object "a" 1)) ,(object "a" 2))
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "selfCheck")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "collisionCheck")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "step")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "stepCheck")
             (("counter" ,(object "a" 1)) ,(object "a" 3))
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "selfCheck")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "collisionCheck")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "step")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "stepCheck")
             (("counter" ,(object "a" 1)) "failure")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "criticalSection")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "initialState")
             (("active_agent") ,(object "a" 1))
             )
            (
             (("active_state" ,(object "a" 1)) "step")
             (("counter" ,(object "a" 1)) "initial")
             (("active_agent") "initial")
             )
            ))
        )
      filename :if-exists :supersede)))
