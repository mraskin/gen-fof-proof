include('dijkstra-mutex-typed-2.gen.ax').

tpi(set_log, setenv, '=TPI_EXECUTE_LOG' = '/tmp/tpi-$getenv(USER)/tpi-exec-log').
tpi(set_log, setenv, '=TPI_EXECUTE_LOG' = '$getenv(=EMPTY)').
tpi(set_log, setenv, '=TPI_DUMP_LOG' = '/tmp/tpi-$getenv(USER)/tpi-dump').
tpi(set_log, setenv, '=TPI_PROOF_LOG' = '/tmp/tpi-$getenv(USER)/tpi-proof').
tpi(set_exec_quiet, setenv, '=TPI_EXECUTE_QUIET' = '-').
tpi(set_threads, setenv, '=TPI_CHECK_LEMMA_THREADS' = '$getenv(TPI_PROVER_THREADS)').
tpi(clear_log, execute, 'rm -f -v $getenv(=TPI_EXECUTE_LOG)').
tpi(touch_vars, write, '$getenv(TPI_PROVER) $getenv(TPI_PROVER_FILE_PREFIX)%s ').

tpi(setup_equiassumption, setenv, '=TPI_CASE_ANALYSIS_SKIP_EQUIASSUMPTION' = '$getenv(TPI_CAT_SKIP_EQUIASSUME)').

tpi(save_prover, setenv,
  'TPI_PROVER_DEFAULT' = '$getenv(TPI_PROVER)').
tpi(restore_default_options, set_lemma_check_group_env,
  '$' => ('TPI_PROVER' = '$getenv(TPI_PROVER_DEFAULT)')).
tpi(set_simple_options, set_lemma_check_group_env,
  '$trivial' => ('TPI_PROVER' = '$getenv(TPI_PROVER) $getenv(TPI_PROVER_SIMPLE_OPTIONS)')).
tpi(set_expansion_options, set_lemma_check_group_env,
  '$textual' => ('TPI_PROVER' = '$getenv(TPI_PROVER) $getenv(TPI_PROVER_EXPANSION_OPTIONS)')).

fof(define_safety_for, checked_definition,
  ![T,A1,A2]: (safe_for(T,A1,A2)<=>(
    (active_state(T,A1)=criticalSection & active_state(T,A2)=criticalSection)
       => A1=A2))).

fof(define_safety, checked_definition,
  ![T]: (safe(T)<=>(![A1,A2]:
    safe_for(T,A1,A2)))).

fof(define_inside, checked_definition,
  ![T,A]: (inside(T,A) <=> (
    is_moment(T) & is_agent(A) &
    (
    active_state(T,A) = startCheck |
    active_state(T,A) = selfCheck |
    active_state(T,A) = stepCheck |
    active_state(T,A) = collisionCheck |
    active_state(T,A) = step |
    active_state(T,A) = criticalSection
    )
  )
  )
).

fof(define_counter_scope, checked_definition,
  ![T,A]: (counter_scope(T,A) <=> (
    is_moment(T) & is_agent(A) &
    (
    active_state(T,A) = selfCheck |
    active_state(T,A) = stepCheck |
    active_state(T,A) = collisionCheck |
    active_state(T,A) = step |
    active_state(T,A) = criticalSection
    )
  )
  )
).

fof(define_inside_correct_for, checked_definition,
  ![T, A]: (
    inside_correct_for(T, A) <=> (inside(T,A) => outside(T,A) = false)
  )
).

fof(define_inside_correct, checked_definition,
  ![T]: (
    inside_correct(T) <=> (![A]: (inside_correct_for(T,A)))
  )
).

fof(define_instant_agent_pair, checked_definition,
  ![T,A,B]: (
    instant_agent_pair(T, A, B) <=> (is_moment(T) & is_agent(A) & is_agent(B)
    & A!=B)
  )
).

fof(define_passed, checked_definition,
  ![T,A,B]: (passed(T,A,B)<=>(
    instant_agent_pair(T,A,B) & counter_scope(T, A) &
    (leq(B,counter(T,A))) & (B=counter(T,A) => active_state(T,A)=step)
  ))).

fof(define_passed_exclusive_for, checked_definition,
  ![T, A, B]: (passed_exclusive_for(T, A, B) <=>
     ~(passed(T,A,B) & passed(T,B,A)))).

fof(define_passed_exclusive, checked_definition,
  ![T]: (passed_exclusive(T) <=> (![A, B]: (passed_exclusive_for(T, A, B))))).

fof(define_passed_in_critical_for, checked_definition,
  ![T, A]: (passed_in_critical_for(T, A) <=>
    ![B]: (
      (
        active_state(T, A) = criticalSection
        & instant_agent_pair(T, A, B)
      ) => (
        passed(T, A, B)
      )
    )
  )
).

fof(define_passed_in_critical, checked_definition,
  ![T]: (passed_in_critical(T) <=>
    ![A]: (passed_in_critical_for(T, A)))).

fof(define_critical_overflow_for, checked_definition,
  ![T,A]: (critical_overflow_for(T,A) <=>
   (active_state(T,A)=criticalSection => counter(T,A)=failure))).
fof(define_critical_overflow, checked_definition,
  ![T]: (critical_overflow(T) <=> ![A]: critical_overflow_for(T,A))).

%%% The invariant to prove by induction
fof(define_invariant, checked_definition,
  ![T]: (invariant(T)<=>(
     inside_correct(T)
     & passed_exclusive(T)
     & passed_in_critical(T)
     & critical_overflow(T)
     & safe(T)
  ))).

tpi(run_checks_for_defs, index_definitions, 'dg_').
tpi(run_checks_for_defs, check_definitions, '-').


fof(counter_change, checked_lemma,
   ![T,A,B]:((
        is_moment(T) & is_agent(A) & is_agent(B)
        & counter(T,A)!=counter(next_moment(T),A)
     ) => 
     (
     active_state(T,A)=step
     |
     active_state(T,A)=startCheck
     ))
   ).

fof(counter_contiguous_change, checked_lemma,
   ![T,A,B]:((
        is_moment(T) & is_agent(A) & is_agent(B)
        & ~leq(B,counter(T,A))
        & leq (B,counter(next_moment(T),A))
     ) => counter(T,A)!=counter(next_moment(T),A))
   ).

fof(counter_contiguous_step, checked_lemma,
   ![T,A,B]:((
        is_moment(T) & is_agent(A) & is_agent(B)
        & ~leq(B,counter(T,A))
        & leq (B,counter(next_moment(T),A))
     ) => next_agent(counter(T,A))=counter(next_moment(T),A))
   ).


fof(counter_contiguous, checked_lemma,
   ![T,A,B]:((
        is_moment(T) & is_agent(A) & is_agent(B)
        & ~leq(B,counter(T,A))
        & leq (B,counter(next_moment(T),A))
     ) => B=counter(next_moment(T),A))
   ).

fof(critical_overflow_implies_overflow, checked_lemma,
  ![T,A,B]:((
    invariant(T)
    & active_state(T,A)=criticalSection
    & is_agent(B)
  ) => (
    counter(T,A)=failure
  ))
  ).

fof(overflow_larger_than_agent, checked_lemma,
  ![A]: (is_agent(A) => ~leq(failure,A))
).

fof(critical_overflow_implies_pass, checked_lemma,
  ![T,A,B]:((
    invariant(T)
    & active_state(T,A)=criticalSection
    & is_agent(B)
  ) => (
    ~leq(counter(T,A),B)
  ))
  ).

fof(double_pass_cases, checked_lemma,
  ![T,A,B]:((
    is_moment(T)
    & is_agent(A)
    & is_agent(B)
    & passed(T,A,B)
    & ~passed(T,B,A)
    & inside_correct_for(T,A)
  ) => (
    active_agent(T)!=B
    | (active_agent(T)=B & active_state(T,B)=step)
    | (active_agent(T)=B & active_state(T,B)!=collisionCheck & active_state(T,B)!=step)
    | (active_agent(T)=B & active_state(T,B)=collisionCheck & counter(T,B)!=A)
    | (active_agent(T)=B & active_state(T,B)=collisionCheck & counter(T,B)=A & outside(T,A)=true)
    | (active_agent(T)=B & active_state(T,B)=collisionCheck & counter(T,B)=A & outside(T,A)!=true)
  ))
).

fof(no_double_pass_typed, checked_lemma,
  ![T,A,B]:((
    is_moment(T)
    & is_agent(A)
    & is_agent(B)
    & passed(T,A,B)
    & ~passed(T,B,A)
    & inside_correct_for(T,A)
  ) => (
    ~passed(next_moment(T),B,A)
  ))
).

fof(passed_exclusive_preserved_local, checked_lemma,
  ![T,A,B]: ((
    inside_correct_for(T,A)
    & inside_correct_for(T,B)
    & passed_exclusive_for(T,A,B)
  ) => (
    passed_exclusive_for(next_moment(T),A,B)
  ))
).

fof(safety_conditions_local, checked_lemma,
  ![T,A,B]: ((passed_exclusive_for(T,A,B) 
                & passed_in_critical_for(T,A)
                & passed_in_critical_for(T,B)
              ) => safe_for(T,A,B))).

fof(safety_conditions, checked_lemma,
  ![T]: ((passed_exclusive(T) & passed_in_critical(T)) => safe(T))).

fof(invariant_preserved, checked_lemma,
  ![T]: (invariant(T) => invariant(next_moment(T)))).

fof(invariant_preserved_plus_safety, checked_lemma,
  ![T]: (invariant(T) => (invariant(next_moment(T)) & safe(next_moment(T))))).

tpi(ed_invariant_preserved, expand_definitions_in_conclusion_once, invariant_preserved).

tpi(ca_no_double_pass_typed, add_cases, double_pass_cases = no_double_pass_typed).

tpi(sc_all, split_all_conjunctions, '-').
%tpi(dump, dump, stdout).

tpi(run_checks, check_lemmas_strict, '$TPI_PROVER ${TPI_PROVER_FILE_PREFIX}%s ').
tpi(exit, exit, '0').

tpi(dump, dump, stdout).
tpi(trust_me, trust_me, trust_me).
tpi(dump, dump, stdout).
