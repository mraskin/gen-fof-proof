(let*
  ((filename-ax "data/mutex-proofs/dijkstra-mutex-typed-2.gen.ax")
   (states
     (list
       "initialState"
       "forbidSteal" "checkTurn" "failureToEnter" "grabTurn" "cannotGrab"
       "declareEntry" "startCheck"
       "selfCheck" "collisionCheck" "step" "stepCheck"
       "criticalSection"
       "yieldTurn" "declareExit" "permitSteal"
       ))
   (booleans (list "true" "false"))
   (state-transitions
     `(
       ("initialState" t "forbidSteal" nil)
       ("forbidSteal" t "checkTurn" nil)
       ("checkTurn" "turn(T)=A" "declareEntry" "failureToEnter")
       ("failureToEnter" "stealable(T, turn(T))=true" "grabTurn"
        "cannotGrab")
       ("grabTurn" t "cannotGrab" nil)
       ("cannotGrab" t "checkTurn" nil)
       ("declareEntry" t "startCheck" nil)
       ("startCheck" t "selfCheck" nil)
       ("selfCheck" "counter(T,A) != A" "collisionCheck" "step")
       ("collisionCheck" "outside(T,counter(T,A))=true" "step"
        "checkTurn")
       ("step" t "stepCheck" nil)
       ("stepCheck" "counter(T, A) = failure" "criticalSection"
        "selfCheck")
       ("criticalSection" t "yieldTurn" nil)
       ("yieldTurn" t "declareExit" nil)
       ("declareExit" t "permitSteal" nil)
       ("permitSteal" t "initialState" nil)
       ))
   (counter-transitions
     `(
       (("active_state(T,A) = step") "next_agent(counter(T,A))")
       (("active_state(T,A) = startCheck") "next_agent(initial)")
       ))
   (turn-transitions
     `(
       (("active_state(T, active_agent(T)) = grabTurn") "active_agent(T)")
       (("active_state(T, active_agent(T)) = yieldTurn") "initial")
       ))
   (stealable-transitions
     `(
       (("active_state(T,A) = forbidSteal") "false")
       (("active_state(T,A) = permitSteal") "true")
       ))
   (outside-transitions
     `(
       (("active_state(T,A) = failureToEnter") "true")
       (("active_state(T,A) = declareEntry") "false")
       (("active_state(T,A) = declareExit") "true")
       ))
   )
  (let* ((filename filename-ax))
    (alexandria:write-string-into-file
      (tptp-concat
        (make-agent-stub
          states
          (append booleans))
        (define-list-predicate "boolean" booleans)
        (define-timed-function-with-transitions
          "counter" "agent_or_initial_or_failure"
          () () counter-transitions
          :local t)
        (define-timed-function-with-transitions
          "turn" "agent_or_initial"
          () () turn-transitions
          :total t)
        (define-timed-function-with-transitions
          "stealable" "boolean"
          () () stealable-transitions
          :local t :local-type "agent_or_initial"
          :total t)
        (define-timed-function-with-transitions
          "outside" "boolean"
          () () outside-transitions
          :local t :total t)
        (define-state-transitions "active_state_step" state-transitions)
        (tptp-fof-statement
          "initial_stealable"
          "![T]:(is_moment(T) => stealable(T, initial) = true)")
        )
      filename :if-exists :supersede))
  (let* ((filename "data/mutex-proofs/dijkstra-mutex-typed-2.example-1.ax")
         (na 3)
         (agents (loop for k from 1 to na collect (object "a" k)))
         (activity
           `(
             1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
             1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2 1 2
             1 2 1 3 1 2 1 3 1 2 1 3 1 2 1 3 1 2 1 3 1 2 1 3 1 2 1 3 1 2 1 3
             1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
             1 3 2 2 1 3 2 2 2 2 1 3 2 2 2 2 2 2 2 2 1 3 2 2 2 2 2 2 2 2 2 2
             2 2 2 2 2 2 1 3 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
             2 2 2 2 2 2 2 2 1 3 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
             2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
             2 2 2 2 2 2 2 2 2 2 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1 2 3 1
             ))
         (dt (1- (length activity)))
         (moments (loop for k from 0 to dt collect (object "t" k)))
         (known-values (define-self-evaluation
                         (append
                           states
                           booleans
                           (list "initial" "failure" "freeze")
                           agents moments)
                         nil))
         (agent-seq `("initial" ,@ agents "failure" "failure"))
         (time-seq `(,@ moments "freeze" "freeze"))
         )
    (loop for a in agents do
          (setf (gethash (list "is_agent" a) known-values) :true))
    (loop for m in moments do
          (setf (gethash (list "is_moment" m) known-values) :true))
    (loop for x in '("initial" "failure")
          do (setf (gethash (list "is_moment" x) known-values) :false)
          do (setf (gethash (list "is_agent" x) known-values) :false))
    (loop for tail on agent-seq
          for cur := (first tail)
          for next := (second tail)
          while next
          do (setf (gethash (list "next_agent" cur) known-values) next))
    (loop for tail on time-seq
          for cur := (first tail)
          for next := (second tail)
          while next
          do (setf (gethash (list "next_moment" cur) known-values) next))
    (setf (gethash (list "turn" (object "t" 0)) known-values) "initial")
    (loop for a in (append (list "initial") agents)
          do
          (setf (gethash (list "stealable" (object "t" 0) a) known-values)
                "true"))
    (loop for a in agents
          do
          (setf (gethash (list "outside" (object "t" 0) a) known-values)
                "true")
          do
          (setf (gethash (list "counter" (object "t" 0) a) known-values)
                "initial")
          do
          (setf (gethash (list "active_state" (object "t" 0) a) known-values)
                "initialState")
          )
    (loop for m in moments
          for an in activity
          do (setf (gethash (list "active_agent" m)
                            known-values)
                   (object "a" an)))
    (loop for m in moments
          do (calculate-next-value "turn" m () () turn-transitions
                                   known-values)
          do (loop for a in (append (list "initial") agents)
                   do (calculate-next-value
                        "stealable" m () () stealable-transitions
                        known-values :agent a))
          do (loop for a in agents
                   do (calculate-next-value
                        "outside" m () () outside-transitions
                        known-values :agent a)
                   do (calculate-next-value
                        "counter" m () () counter-transitions
                        known-values :agent a)
                   do (calculate-next-state
                        m a state-transitions known-values)))
    (alexandria:write-string-into-file
      (tptp-concat
        (alexandria:read-file-into-string filename-ax)
        (define-known-values "evaluated" known-values)
        )
      filename :if-exists :supersede))
  )

