fof(leq_reflexive, axiom, ![X]: leq(X,X)).

fof(leq_antisymmetric, axiom, ![X,Y]: ((leq(X,Y)&leq(Y,X))=>X=Y)).

fof(leq_transitive, axiom, ![X,Y,Z]: ((leq(X,Y)&leq(Y,Z))=>leq(X,Z))).

fof(leq_linear, axiom, ![X,Y]: (leq(X,Y)|leq(Y,X))).


fof(type_list, axiom, (![X]:~(is_initial(X)&is_moment(X))) & (![X]:~(is_initial(X)&is_agent(X))) & (![X]:~(is_initial(X)&is_state(X))) & (![X]:~(is_initial(X)&is_failure(X))) & (![X]:~(is_moment(X)&is_agent(X))) & (![X]:~(is_moment(X)&is_state(X))) & (![X]:~(is_moment(X)&is_failure(X))) & (![X]:~(is_agent(X)&is_state(X))) & (![X]:~(is_agent(X)&is_failure(X))) & (![X]:~(is_state(X)&is_failure(X)))).

fof(failure_definition, axiom, ![X]: (is_failure(X)<=>((X = failure)))).
fof(initial_definition, axiom, ![X]: (is_initial(X)<=>((X = initial)))).
fof(freeze_is_moment, axiom, is_moment(freeze)).

fof(type_order_initial_leq_moment, axiom, ![X,Y]: ((is_initial(X)&is_moment(Y))=>leq(X,Y))).

fof(type_order_initial_leq_agent, axiom, ![X,Y]: ((is_initial(X)&is_agent(Y))=>leq(X,Y))).

fof(type_order_initial_leq_state, axiom, ![X,Y]: ((is_initial(X)&is_state(Y))=>leq(X,Y))).

fof(type_order_initial_leq_failure, axiom, ![X,Y]: ((is_initial(X)&is_failure(Y))=>leq(X,Y))).

fof(type_order_moment_leq_agent, axiom, ![X,Y]: ((is_moment(X)&is_agent(Y))=>leq(X,Y))).

fof(type_order_moment_leq_state, axiom, ![X,Y]: ((is_moment(X)&is_state(Y))=>leq(X,Y))).

fof(type_order_moment_leq_failure, axiom, ![X,Y]: ((is_moment(X)&is_failure(Y))=>leq(X,Y))).

fof(type_order_agent_leq_state, axiom, ![X,Y]: ((is_agent(X)&is_state(Y))=>leq(X,Y))).

fof(type_order_agent_leq_failure, axiom, ![X,Y]: ((is_agent(X)&is_failure(Y))=>leq(X,Y))).

fof(type_order_state_leq_failure, axiom, ![X,Y]: ((is_state(X)&is_failure(Y))=>leq(X,Y))).


fof(next_moment_type, axiom, ![X]: (((is_moment(X)) & is_moment(next_moment(X)))|next_moment(X)=failure)).


fof(next_moment_monotonous, axiom, ![X,Y]: ((moment(X)&moment(Y)) => ((leq(X,Y)=>leq(next_moment(X),next_moment(Y)))))).

fof(next_moment_increasing, axiom, ![X]: (moment(X)=>leq(X,next_moment(X)))).

fof(next_moment_exhaustive, axiom, ![X,Y]: ((is_moment(X)&is_moment(Y)&leq(X,Y)&leq(Y,next_moment(X)))=>(X=Y | next_moment(X)=Y))).


fof(freeze_is_frozen, axiom, next_moment(freeze)=freeze).

fof(freeze_is_last_moment, axiom, ![T]: (is_moment(T)=>leq(T,freeze))).

fof(next_moment_is_total, axiom, ![T]: (is_moment(T)=>is_moment(next_moment(T)))).

fof(agent_or_initial_definition, axiom, ![X]: (is_agent_or_initial(X)<=>((is_agent(X)) | (is_initial(X))))).

fof(agent_or_initial_or_failure_definition, axiom, ![X]: (is_agent_or_initial_or_failure(X)<=>((is_agent(X)) | (is_initial(X)) | (is_failure(X))))).

fof(next_agent_type, axiom, ![X]: (((is_agent_or_initial_or_failure(X)) & is_agent_or_initial_or_failure(next_agent(X)))|next_agent(X)=failure)).


fof(next_agent_monotonous, axiom, ![X,Y]: ((agent_or_initial_or_failure(X)&agent_or_initial_or_failure(Y)) => ((leq(X,Y)=>leq(next_agent(X),next_agent(Y)))))).

fof(next_agent_increasing, axiom, ![X]: (agent_or_initial_or_failure(X)=>leq(X,next_agent(X)))).

fof(next_agent_exhaustive, axiom, ![X,Y]: ((is_agent_or_initial_or_failure(X)&is_agent_or_initial_or_failure(Y)&leq(X,Y)&leq(Y,next_agent(X)))=>(X=Y | next_agent(X)=Y))).


fof(initial_agent, axiom, is_agent(next_agent(initial))).

fof(active_agent_type, axiom, ![T]: (((is_moment(T)) & is_agent_or_initial(active_agent(T)))|active_agent(T)=failure)).

fof(active_agent_totality, axiom, ![T]: (((is_moment(T))) => is_agent_or_initial(active_agent(T)))).


fof(active_state_type, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A)) & is_state(active_state(T,A)))|active_state(T,A)=failure)).

fof(active_state_totality, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A))) => is_state(active_state(T,A)))).


fof(state_definition, axiom, ![X]: (is_state(X)<=>((X = initialState) | (X = step) | (X = stepCheck) | (X = selfCheck) | (X = collisionCheck) | (X = criticalSection)))).
fof(object_list_distinct_1, axiom, initial!=failure).
fof(object_list_distinct_2, axiom, initial!=freeze).
fof(object_list_distinct_3, axiom, initial!=initialState).
fof(object_list_distinct_4, axiom, initial!=step).
fof(object_list_distinct_5, axiom, initial!=stepCheck).
fof(object_list_distinct_6, axiom, initial!=selfCheck).
fof(object_list_distinct_7, axiom, initial!=collisionCheck).
fof(object_list_distinct_8, axiom, initial!=criticalSection).
fof(object_list_distinct_9, axiom, failure!=freeze).
fof(object_list_distinct_10, axiom, failure!=initialState).
fof(object_list_distinct_11, axiom, failure!=step).
fof(object_list_distinct_12, axiom, failure!=stepCheck).
fof(object_list_distinct_13, axiom, failure!=selfCheck).
fof(object_list_distinct_14, axiom, failure!=collisionCheck).
fof(object_list_distinct_15, axiom, failure!=criticalSection).
fof(object_list_distinct_16, axiom, freeze!=initialState).
fof(object_list_distinct_17, axiom, freeze!=step).
fof(object_list_distinct_18, axiom, freeze!=stepCheck).
fof(object_list_distinct_19, axiom, freeze!=selfCheck).
fof(object_list_distinct_20, axiom, freeze!=collisionCheck).
fof(object_list_distinct_21, axiom, freeze!=criticalSection).
fof(object_list_distinct_22, axiom, initialState!=step).
fof(object_list_distinct_23, axiom, initialState!=stepCheck).
fof(object_list_distinct_24, axiom, initialState!=selfCheck).
fof(object_list_distinct_25, axiom, initialState!=collisionCheck).
fof(object_list_distinct_26, axiom, initialState!=criticalSection).
fof(object_list_distinct_27, axiom, step!=stepCheck).
fof(object_list_distinct_28, axiom, step!=selfCheck).
fof(object_list_distinct_29, axiom, step!=collisionCheck).
fof(object_list_distinct_30, axiom, step!=criticalSection).
fof(object_list_distinct_31, axiom, stepCheck!=selfCheck).
fof(object_list_distinct_32, axiom, stepCheck!=collisionCheck).
fof(object_list_distinct_33, axiom, stepCheck!=criticalSection).
fof(object_list_distinct_34, axiom, selfCheck!=collisionCheck).
fof(object_list_distinct_35, axiom, selfCheck!=criticalSection).
fof(object_list_distinct_36, axiom, collisionCheck!=criticalSection).


fof(counter_type, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A)) & is_agent_or_initial_or_failure(counter(T,A)))|counter(T,A)=failure)).

fof(counter_totality, axiom, ![T,A]: (((is_moment(T)) & (is_agent(A))) => is_agent_or_initial_or_failure(counter(T,A)))).


fof(active_state_structure_step_1, axiom, ![T,A]: (((((~is_moment(next_moment(T))) | (~is_agent(A))))) => active_state(next_moment(T),A)=failure)).

fof(active_state_structure_step_2, axiom, ![T,A]: (((~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_agent(T)!=A))) => active_state(next_moment(T),A)=active_state(T,A))).

fof(active_state_structure_step_3, axiom, ![T,A]: (((~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=initialState))) => active_state(next_moment(T),A)=step)).

fof(active_state_structure_step_4, axiom, ![T,A]: (((~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=step))) => active_state(next_moment(T),A)=stepCheck)).

fof(active_state_structure_step_5, axiom, ![T,A]: (((~((active_state(T,A)=step))) & (~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=selfCheck) & (counter(T,A)!=A))) => active_state(next_moment(T),A)=collisionCheck)).

fof(active_state_structure_step_6, axiom, ![T,A]: (((~((active_state(T,A)=selfCheck) & (counter(T,A)!=A))) & (~((active_state(T,A)=step))) & (~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=selfCheck))) => active_state(next_moment(T),A)=step)).

fof(active_state_structure_step_7, axiom, ![T,A]: (((~((active_state(T,A)=selfCheck))) & (~((active_state(T,A)=selfCheck) & (counter(T,A)!=A))) & (~((active_state(T,A)=step))) & (~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=stepCheck) & (counter(T,A)!=failure))) => active_state(next_moment(T),A)=selfCheck)).

fof(active_state_structure_step_8, axiom, ![T,A]: (((~((active_state(T,A)=stepCheck) & (counter(T,A)!=failure))) & (~((active_state(T,A)=selfCheck))) & (~((active_state(T,A)=selfCheck) & (counter(T,A)!=A))) & (~((active_state(T,A)=step))) & (~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=stepCheck))) => active_state(next_moment(T),A)=criticalSection)).

fof(active_state_structure_step_9, axiom, ![T,A]: (((~((active_state(T,A)=stepCheck))) & (~((active_state(T,A)=stepCheck) & (counter(T,A)!=failure))) & (~((active_state(T,A)=selfCheck))) & (~((active_state(T,A)=selfCheck) & (counter(T,A)!=A))) & (~((active_state(T,A)=step))) & (~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=collisionCheck) & (counter(T,counter(T,A))=initial))) => active_state(next_moment(T),A)=step)).

fof(active_state_structure_step_10, axiom, ![T,A]: (((~((active_state(T,A)=collisionCheck) & (counter(T,counter(T,A))=initial))) & (~((active_state(T,A)=stepCheck))) & (~((active_state(T,A)=stepCheck) & (counter(T,A)!=failure))) & (~((active_state(T,A)=selfCheck))) & (~((active_state(T,A)=selfCheck) & (counter(T,A)!=A))) & (~((active_state(T,A)=step))) & (~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=collisionCheck))) => active_state(next_moment(T),A)=initialState)).

fof(active_state_structure_step_11, axiom, ![T,A]: (((~((active_state(T,A)=collisionCheck))) & (~((active_state(T,A)=collisionCheck) & (counter(T,counter(T,A))=initial))) & (~((active_state(T,A)=stepCheck))) & (~((active_state(T,A)=stepCheck) & (counter(T,A)!=failure))) & (~((active_state(T,A)=selfCheck))) & (~((active_state(T,A)=selfCheck) & (counter(T,A)!=A))) & (~((active_state(T,A)=step))) & (~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=criticalSection))) => active_state(next_moment(T),A)=initialState)).

fof(active_state_structure_step_12, axiom, ![T,A]: (((~((active_state(T,A)=criticalSection))) & (~((active_state(T,A)=collisionCheck))) & (~((active_state(T,A)=collisionCheck) & (counter(T,counter(T,A))=initial))) & (~((active_state(T,A)=stepCheck))) & (~((active_state(T,A)=stepCheck) & (counter(T,A)!=failure))) & (~((active_state(T,A)=selfCheck))) & (~((active_state(T,A)=selfCheck) & (counter(T,A)!=A))) & (~((active_state(T,A)=step))) & (~((active_state(T,A)=initialState))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A)))))) => active_state(next_moment(T),A)=active_state(T,A))).


fof(counter_structure_step_1, axiom, ![T,A]: (((((~is_moment(next_moment(T))) | (~is_agent(A))))) => counter(next_moment(T),A)=failure)).

fof(counter_structure_step_2, axiom, ![T,A]: (((~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_agent(T)!=A))) => counter(next_moment(T),A)=counter(T,A))).

fof(counter_structure_step_3, axiom, ![T,A]: (((~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=step))) => counter(next_moment(T),A)=next_agent(counter(T,A)))).

fof(counter_structure_step_4, axiom, ![T,A]: (((~((active_state(T,A)=step))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A))))) & ((active_state(T,A)=initialState))) => counter(next_moment(T),A)=initial)).

fof(counter_structure_step_5, axiom, ![T,A]: (((~((active_state(T,A)=initialState))) & (~((active_state(T,A)=step))) & (~((active_agent(T)!=A))) & (~(((~is_moment(next_moment(T))) | (~is_agent(A)))))) => counter(next_moment(T),A)=counter(T,A))).


