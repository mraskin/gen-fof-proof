(defun tptp-multiop (op expressions)
  (format nil (concatenate 'string "~{(~a)~#[~:; " op " ~]~}") expressions))

(defun tptp-one-of (object values)
  (tptp-multiop "|" (mapcar (lambda (v)
                              (format nil "~a = ~a" object v))
                            values)))

(defun define-list-predicate (name elements)
  (format nil "fof(~a_definition, axiom, ![X]: (is_~a(X)<=>(~a)))."
          name name (tptp-one-of "X" elements)))

(defun tptp-fof-statement (name formula &key (kind "axiom"))
  (format nil "fof(~a, ~a, ~a).~%" name kind formula))

(defun tptp-universal-quantification (variables)
  (format nil "![~{~a~#[~:;,~]~}]" variables))

(defun tptp-funcall (name arguments)
  (format nil "~a(~{~a~#[~:;,~]~})" name arguments))

(defun tptp-concat (&rest args)
  (format nil "~{~a~%~}" (alexandria:flatten args)))

(defun define-set-type (type)
  (let ((set-type (format nil "~a_set" type))
        (set-type-test (format nil "is_~a_set" type))
        (type-test (format nil "is_~a" type))
        (complete-set-object (format nil "complete_~a_set" type)))
    (tptp-concat
      (tptp-fof-statement
        (format nil "~a_typing" set-type)
        (format nil "![S, E]: ((~a(S) & is_member_of(S, E)) => ~a(E))"
                set-type-test
                type-test))
      (tptp-fof-statement
        (format nil "~a_complete_object" set-type)
        (format nil "![E]: ((~a(E)) <=> is_member_of(~a, E))"
                type-test complete-set-object)
        :kind "definition"
        )
      (tptp-fof-statement
        (format nil "~a_type" set-type)
        (format nil "![S, E]: ((~a(E) & is_member_of(S, E)) => ~a(S))"
                type-test set-type-test))
      (tptp-fof-statement
        (format nil "~a_choice_function_type" set-type)
        (format nil "![S]: ((~a(S)) => (is_~a_or_failure(choice_~a(S))))"
                set-type-test type set-type))
      (tptp-fof-statement
        (format nil "~a_choice_function_member_of_set" set-type)
        (format nil "![S]: (((~a(S)) & (choice_~a(S) != failure)) => (is_member_of(S, choice_~a(S))))"
                set-type-test set-type set-type))
      (tptp-fof-statement
        (format nil "~a_choice_function_mistyped" set-type)
        (format nil "![S]: ((~~~a(S)) => (choice_~a(S) = failure))"
                set-type-test set-type))
      (tptp-fof-statement
        (format nil "~a_choice_function_welltyped_failure" set-type)
        (format nil "![S]: ((~a(S)) => ((choice_~a(S) = failure) <=> (![E]: is_member_of(S,E))))"
                set-type-test set-type)
        :kind "definition")
      (tptp-fof-statement
        (format nil "~a_timed_choice_function_type" set-type)
        (format nil "![S, T]: ((~a(S) & is_moment(T)) => (is_~a_or_failure(timed_choice_~a(T, S))))"
                set-type-test type set-type))
      (tptp-fof-statement
        (format nil "~a_timed_choice_function_member_of_set" set-type)
        (format nil "![S, T]: (((~a(S) & is_moment(T)) & (timed_choice_~a(T, S) != failure)) => (is_member_of(S, timed_choice_~a(T, S))))"
                set-type-test set-type set-type))
      (tptp-fof-statement
        (format nil "~a_timed_choice_function_mistyped" set-type)
        (format nil "![S, T]: ((~~~a(S) | ~~is_moment(T)) => (timed_choice_~a(T, S) = failure))"
                set-type-test set-type))
      (tptp-fof-statement
        (format nil "~a_timed_choice_function_welltyped_failure" set-type)
        (format nil "![S, T]: ((~a(S) & is_moment(T)) => ((timed_choice_~a(T, S) = failure) <=> (![E]: ~is_member_of(S,E))))"
                set-type-test set-type)))))

(defun define-function-type (name type failure arguments argument-types
                                  &key total)
  (let* ((arguments-good
           (tptp-multiop
             "&"
             (loop for a in arguments
                   for at in argument-types
                   collect
                   (format nil "is_~a(~a)" at a)))))
    (tptp-concat
      (tptp-fof-statement
        (format nil "~a_type" name)
        (format nil
                "![~{~a~#[~:;,~]~}]: ((~a & ~a)|~a=~a)"
                arguments
                arguments-good
                (format nil "is_~a(~a)"
                        type
                        (tptp-funcall name arguments))
                (tptp-funcall name arguments)
                failure))
      (when total
        (tptp-fof-statement
          (format nil "~a_totality" name)
          (format nil "~a: ((~a) => is_~a(~a))"
                  (tptp-universal-quantification arguments)
                  arguments-good
                  type
                  (tptp-funcall name arguments)))))))

(defun define-disjoint-types (names &key
                                    (name "type_list")
                                    (include-sets-for ())
                                    )
  (let ((names-and-sets (append names
                                (mapcar (lambda (x) (format nil "~a_set" x))
                                        include-sets-for))))
    (tptp-fof-statement
      name
      (tptp-multiop "&"
                    (loop for rest on
                          (mapcar (lambda (x) (format nil "is_~a" x))
                                  names-and-sets)
                          for head := (first rest)
                          for tail := (rest rest)
                          append
                          (loop for second in tail
                                collect
                                (format nil "![X]:~~(~a(X)&~a(X))"
                                        head second)))))))

(defun define-distinctness (name objects)
  (format nil "~{fof(~{~a_distinct_~a, axiom, ~a!=~a~}).~%~}"
          (loop with n := 0
                for rest on objects
                for head := (car rest)
                for tail := (cdr rest)
                append
                (loop for second in tail
                      do (incf n)
                      collect (list name n head second)))))

(defun define-order (name &key linear)
  (tptp-concat
    (list
      (tptp-fof-statement
        (format nil "~a_reflexive" name)
        (format nil "![X]: ~a(X,X)" name))
      (tptp-fof-statement
        (format nil "~a_antisymmetric" name)
        (format nil "![X,Y]: ((~a(X,Y)&~a(Y,X))=>X=Y)" name name))
      (tptp-fof-statement
        (format nil "~a_transitive" name)
        (format nil "![X,Y,Z]: ((~a(X,Y)&~a(Y,Z))=>~a(X,Z))"
                name name name)))
    (when linear
      (list
        (tptp-fof-statement
          (format nil "~a_linear" name)
          (format nil "![X,Y]: (~a(X,Y)|~a(Y,X))" name name))))))

(defun define-type-order (order types)
  (tptp-concat
    (loop for iter on types
          for head := (first iter)
          for tail := (rest iter)
          append
          (loop for second in tail
                collect
                (tptp-fof-statement
                  (format nil "type_order_~a_~a_~a" head order second)
                  (format nil "![X,Y]: ((is_~a(X)&is_~a(Y))=>~a(X,Y))"
                          head second order))))))

(defun define-next (name order type failure)
  (tptp-concat
    (define-function-type name type failure (list "X") (list type))
    (tptp-fof-statement
      (format nil "~a_monotonous" name)
      (format nil "![X,Y]: ((~a(X)&~a(Y)) => ((~a(X,Y)=>~a(~a(X),~a(Y)))))"
              type type order order name name))
    (tptp-fof-statement
      (format nil "~a_increasing" name)
      (format nil "![X]: (~a(X)=>~a(X,~a(X)))"
              type order name))
    (tptp-fof-statement
      (format nil "~a_exhaustive" name)
      (format nil "![X,Y]: ((is_~a(X)&is_~a(Y)&~a(X,Y)&~a(Y,~a(X)))=>(X=Y | ~a(X)=Y))"
              type type order order name name))))

(defun define-union-type (name names)
  (tptp-fof-statement
    (format nil "~a_definition" name)
    (format nil "![X]: (is_~a(X)<=>(~a))"
            name (tptp-multiop "|"
                               (loop for n in names collect
                                     (format nil "is_~a(X)" n))))))

(defun define-function-structure (name rule failure 
                                       arguments argtypes default assignments)
  (let* ((free-vars
           (remove-duplicates
             (reduce
               'append
               (loop for a in arguments
                     collect
                     (fof-free-vars (esrap:parse 'tptp-syntax::fof_term a))))
             :test 'equal))
         (typerror-condition
           (tptp-multiop
             "|" (loop 
                   for a in arguments
                   for at in argtypes
                   collect
                   (format nil "~~is_~a(~a)" at a))))
         (full-assignment-list
           `(((,typerror-condition) ,failure)
             ,@ assignments
             (())))
         (cases
           (loop with negconditions := nil
                 for a in full-assignment-list
                 for cl := (first a)
                 for c := (when cl (tptp-multiop "&" cl))
                 for v := (or (second a) default)
                 collect
                 (format
                   nil "(~a) => ~a=~a"
                   (tptp-multiop
                     "&" (append
                           negconditions
                           (when c (list c))))
                   (tptp-funcall name arguments) v)
                 while c
                 do (push (format nil "~~(~a)" c) negconditions))))
    (tptp-concat
      (loop for case in cases
            for n upfrom 1
            collect
            (tptp-fof-statement
              (format nil "~a_structure_~a_~a" name rule n)
              (format
                nil "![~{~a~#[~:;,~]~}]: (~a)"
                free-vars case))))))

(defun make-transition-assignment (object value condition yes no)
  (cond
    ((eq condition t) `(((,(format nil "~a=~a" object value)) ,yes)))
    ((eq condition nil) `(((,(format nil "~a=~a" object value)) ,no)))
    (t `(((,(format nil "~a=~a" object value) ,condition) ,yes)
         ((,(format nil "~a=~a" object value)) ,no)))))

(defun make-transition-assignments (object clauses)
  (loop for c in clauses append (apply 'make-transition-assignment object c)))

(defun make-agent-stub (states extra-objects &key (include-sets-for ()))
  (let ((basic-types (list "initial" "moment" "agent" "state" "failure"))
        (complete-set-objects (mapcar (lambda (set-type)
                                         (format nil "complete_~a_set" set-type))
                                       include-sets-for)))
    (tptp-concat
      (define-order "leq" :linear t)
      (define-disjoint-types basic-types :include-sets-for include-sets-for)
      (define-list-predicate "failure" (list "failure"))
      (define-list-predicate "initial" (list "initial"))
      (tptp-fof-statement "freeze_is_moment" "is_moment(freeze)")
      (define-type-order "leq" basic-types)
      (define-next "next_moment" "leq" "moment" "failure")
      (tptp-fof-statement "freeze_is_frozen" "next_moment(freeze)=freeze")
      (tptp-fof-statement "freeze_is_last_moment" "![T]: (is_moment(T)=>leq(T,freeze))")
      (tptp-fof-statement "next_moment_is_total" "![T]: (is_moment(T)=>is_moment(next_moment(T)))")
      (define-union-type "agent_or_initial" (list "agent" "initial"))
      (define-union-type "agent_or_initial_or_failure"
                         (list "agent" "initial" "failure"))
      (if include-sets-for
        (define-union-type "set"
                           (mapcar (lambda (set-type)
                                   (format nil "~a_set" set-type))
                                 include-sets-for))
        (tptp-fof-statement "set_definition"
                            "![X]: (is_set(X))"))
      (define-next "next_agent" "leq" "agent_or_initial_or_failure" "failure")
      (tptp-fof-statement "initial_agent" "is_agent(next_agent(initial))")
      (define-function-type "active_agent" "agent_or_initial" "failure" (list "T") (list "moment") :total t)
      (define-function-type "active_state" "state" "failure" (list "T" "A") (list "moment" "agent") :total t)
      (loop for ground-type in include-sets-for
            collect
            (define-set-type ground-type))
      (tptp-fof-statement "remove_function_for_sets"
                          "![S1, E1, S2]:
                          (remove_member(S1, E1) = S2 <=>
                          (![E2]: ((E2 = E1 & ~is_member_of(S2, E1)) |
                                   (E2 != E1 & (is_member_of(S2, E2) <=> is_member_of(S1, E2))))))"
                          :kind "definition")
      (tptp-fof-statement "empty_predicate"
                          "![S]: (is_empty(S) <=> (is_set(S) & ![E]:
                                                   (~is_member_of(S, E))))"
                          :kind "definition")
      (define-list-predicate "state" states)
      (define-distinctness
        "object_list"
        (append
          (list
            "initial"
            "failure"
            "freeze"
            )
          states
          extra-objects
          complete-set-objects)))))

(defun object (name number) (format nil "~a~6,'0d" name number))

(defun define-sequence (name func objects)
  (tptp-concat
    (loop for o in objects
          for next in (cdr objects)
          for k upfrom 1
          collect (tptp-fof-statement
                    (format nil "~a_~a" name k)
                    (format nil "~a(~a)=~a"
                            func o next)))))

(defun define-values (name func fixed-arguments arguments values)
  (cond ((and (null arguments) (not (listp values)))
         (tptp-fof-statement
           name
           (format nil "~a=~a"
                   (tptp-funcall func fixed-arguments)
                   values)))
        ((or (null arguments) (not (listp values)))
         (error "Argument/value shape mismatch"))
        (t (tptp-concat
             (loop for k upfrom 1
                 for raw-a in arguments
                 for a := (if (listp raw-a) raw-a (list raw-a))
                 for arg := (first a)
                 for fa := (append fixed-arguments (list arg))
                 for rest-args := (second a)
                 for v in values
                 for subname := (format nil "~a_~a" name k)
                 collect
                 (define-values
                   subname func fa
                   rest-args v))))))

(defun define-sticky-values (name common-sequence local-assignments)
  (tptp-concat
    (loop with current-values := (make-hash-table :test 'equal)
          with results := nil
          with j := 0
          for assignments in local-assignments
          for common in common-sequence
          do (loop for assignment in assignments
                   for arguments := (first assignment)
                   for value := (second assignment)
                   do (if value (setf (gethash arguments current-values)
                                      value)
                        (remhash arguments current-values)))
          do (maphash (lambda (k v)
                        (let* ((merged-arguments
                                 (loop with insertions := (subseq k 0)
                                       with result := nil
                                       for x in common
                                       do (if x (push x result)
                                            (push (pop insertions) result))
                                       finally (return
                                                 (append (reverse result)
                                                         insertions)))))
                        (push
                          (tptp-fof-statement
                            (format nil "~a_~a" name (incf j))
                            (format nil "~a=~a"
                                    (tptp-funcall (first merged-arguments)
                                                  (rest merged-arguments))
                                    v))
                          results)))
                      current-values)
          finally (return (reverse results)))))

(defun define-state-transitions (name steps)
  (tptp-concat
    (tptp-fof-statement
      (format nil "~a_0" name)
      "![T,A]: ((is_moment(T) & is_agent(A) & active_agent(T)!=A)
      => active_state(next_moment(T),A) = active_state(T,A))")
      (loop for k upfrom 1
            for step in steps
            for source := (first step)
            for condition := (second step)
            for ok := (third step)
            for fail := (fourth step)
            for direct := (or (eq condition t)
                              (eq condition nil))
            for precondition :=
            (format
              nil
              "is_moment(T) & is_agent(A)
              & active_agent(T)=A & active_state(T,A)=~a"
              source)
            for direct-formula :=
            (format nil "![T,A]: ((~a) => active_state(next_moment(T),A)=~a)"
                    precondition (if condition ok fail))
            for ok-formula :=
            (format
              nil "![T,A]: ((~a & ~a)
                            => active_state(next_moment(T),A)=~a)"
              precondition condition ok)
            for fail-formula :=
            (format
              nil "![T,A]: ((~a & ~~ ~a)
                            =>
                            active_state(next_moment(T),A)=~a)"
              precondition condition fail)
            when direct collect (tptp-fof-statement
                                  (format nil "~a_~a" name k)
                                  direct-formula)
            unless direct collect (tptp-fof-statement
                                    (format nil "~a_~a_ok" name k)
                                    ok-formula)
            unless direct collect (tptp-fof-statement
                                    (format nil "~a_~a_fail" name k)
                                    fail-formula))))

(defun define-timed-function-with-transitions
  (name type arguments arg-types transitions &key
        total local (local-type "agent"))
  (tptp-concat
    (define-function-type
      name type "failure"
      `("T"
        ,@(when local `("A"))
        ,@ arguments)
      `("moment"
        ,@(when local (list local-type))
        ,@ arg-types)
      :total total)
    (define-function-structure
      name "at_next_moment" "failure"
      `("next_moment(T)"
        ,@(when local `("A"))
        ,@ arguments)
      `("moment"
        ,@(when local (list local-type))
        ,@ arg-types)
      (tptp-funcall name `("T" 
                           ,@(when local `("A"))
                           ,@ arguments))
      `(,@(when local
            `(
              (("active_agent(T)!=A"))
              ))
        ,@ transitions))))

(defun calculate-next-value (function time arguments values transitions
                                      known-values
                                      &key agent all-objects variables 
                                      verbose)
  (let* ((agent-arglist (when agent (list "A")))
         (agent-varlist (when agent `(("A" . ,agent))))
         (base-varlist (loop for a in arguments
                             for v in values
                             collect (cons a v)))
         (time-varlist (list (cons "T" time)))
         (varlist (append time-varlist agent-varlist base-varlist))
         (next-moment (evaluate-fof "next_moment(T)" known-values
                                    :variables time-varlist))
         (passive (and agent
                       (eq :false
                           (evaluate-fof "active_agent(T) = A" known-values
                                         :variables
                                         (append varlist variables)))))
         (arglist (append (list "T") agent-arglist arguments))
         (next-arg-values (append (list next-moment)
                                  (when agent (list agent))
                                  values))
         (default (evaluate-fof (tptp-funcall function arglist) known-values
                                :variables varlist))
         (value
           (if passive default
             (loop for transition in transitions
                   for preconditions :=
                   (loop for c in (first transition)
                         for cv := (evaluate-fof
                                     c known-values
                                     :variables (append varlist variables)
                                     :all-objects all-objects
                                     :verbose verbose)
                         when verbose do
                         (format *trace-output*
                                 "Evaluating next value of ~a at ~a (for ~a)~%In transition ~s condition ~s was evaluated to ~s~%"
                                 function time next-moment
                                 transition c cv)
                         when (eq cv :undefined) return :undefined
                         when (eq cv :false) return :false
                         finally (return :true))
                   when (eq preconditions :undefined) return :undefined
                   when (eq preconditions :true) return
                   (evaluate-fof (second transition) known-values
                                 :variables (append varlist variables))
                   finally (return default)))))
    (when verbose (format *trace-output* "Checking assertions on value: ~a and next-moment: ~a~%"
                          value next-moment))
    (assert (not (eq :undefined value)))
    (assert (not (eq :undefined next-moment)))
    (setf (gethash (cons function next-arg-values) known-values) value)
    value))

(defun calculate-next-state (time agent transitions known-values
                                   &key all-objects variables verbose)
  (let* ((time-varlist (list (cons "T" time)))
         (agent-varlist (list (cons "A" agent)))
         (varlist (append time-varlist agent-varlist variables))
         (next-moment (evaluate-fof "next_moment(T)" known-values
                                    :variables time-varlist))
         (state (evaluate-fof "active_state(T,A)" known-values
                              :variables (append time-varlist agent-varlist)))
         (passive 
           (eq :false
               (evaluate-fof "active_agent(T) = A" known-values
                             :variables varlist)))
         (line (find state transitions :key 'first :test 'equal))
         (condition (second line))
         (condition-value
           (cond (passive :true)
                 ((eq condition t) :true)
                 ((eq condition nil) :false)
                 (t (evaluate-fof condition known-values
                                  :all-objects all-objects
                                  :variables varlist))))
         (chosen-branch
           (ecase condition-value
             ((:true) (third line))
             ((:false) (fourth line))))
         (value (if passive state
                  (evaluate-fof chosen-branch known-values
                                :variables varlist))))
    (when verbose
      (format *trace-output* "Evaluating next state for ~a at ~a (for ~a) from ~a (~a) to ~s~%"
              agent time next-moment state (if passive "passive" "active") value))
    (assert (not (eq :undefined state)))
    (assert (not (eq :undefined next-moment)))
    (assert (not (eq :undefined value)))
    (assert line)
    (setf (gethash (list "active_state" next-moment agent) known-values)
          value)
    value))

(defun define-self-evaluation (names known-values)
  (let* ((known-values (or known-values (make-hash-table :test 'equal))))
    (loop for n in names do (setf (gethash n known-values) n))
    known-values))

(defun define-known-values (name known-values)
  (tptp-concat
    (let ((res nil))
      (maphash
        (lambda (k v)
          (push 
            (case v
              ((:undefined))
              ((:true)
               (if (listp k) (tptp-funcall (first k) (rest k)) k))
              ((:false)
               (format
                 nil "~~~a"
                 (if (listp k) (tptp-funcall (first k) (rest k)) k)))
              (t
                (format nil "~a = ~a"
                        (if (listp k) (tptp-funcall (first k) (rest k)) k)
                        v)))
            res))
        known-values)
      (loop for f in (sort (remove nil res) 'string<)
            for k upfrom 1
            collect (tptp-fof-statement
                      (object name k)
                      f)))))

