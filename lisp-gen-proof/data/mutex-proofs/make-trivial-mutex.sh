#! /bin/sh

cd "$(dirname "$0")/../.."

./nix-load-lisp.sh --load "data/mutex-proofs/make-models.lisp" --load "data/mutex-proofs/make-trivial-mutex.lisp" --eval "(quit)"
