#! /bin/sh

cd "$(dirname "$0")/../.."

./nix-load-lisp.sh \
        --load "data/mutex-proofs/make-models-typed.lisp" \
        --load "data/mutex-proofs/make-dijkstra-typed.lisp" \
        --eval "(quit)"
