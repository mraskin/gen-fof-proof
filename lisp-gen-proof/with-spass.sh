#! /bin/sh

export spass="$(which spass 2>/dev/null  || which SPASS || echo "$(nix-build --no-out-link '<nixpkgs>' -A spass)/bin/SPASS")"

time="${1:-15}"
args="$2"
command="$3"

export PS1="<TPI> $PS1"

shift; shift; shift

TPI_PROVER="$spass -TPTP -TimeLimit=$time -PProblem=0 -PGiven=0 $args" TPI_CAT_SKIP_EQUIASSUME="1" "${command:-$SHELL}" "$@"
