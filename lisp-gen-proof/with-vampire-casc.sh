#! /bin/sh

export vampire="$(which vampire 2>/dev/null  || echo "$(NIXPKGS_ALLOW_UNFREE=1 nix-build --no-out-link '<nixpkgs>' -A vampire)/bin/vampire")"

time="${1:-15}"
args="$2"
command="$3"

export PS1="<TPI> $PS1"

shift; shift; shift

TPI_PROVER="$vampire -t $time --proof off --mode casc $args" "${command:-$SHELL}" "$@"
