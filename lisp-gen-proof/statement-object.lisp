(in-package :gen-fof-proof)

(defclass tptp-statement ()
  (
   (name :accessor name :initarg :name :initform (error "Statement must have a name"))
   (content :accessor content :initarg :content :initform (error "Statement must have content"))
   (kind :accessor kind :initarg :kind :initform (error "Statement must have a kind"))
   (role :accessor role :initarg :role :initform (error "Statement must have a role"))
   (source :accessor source :initarg :source :initform nil)
   (extra-annotations :accessor extra-annotations :initarg :extra-annotations :initform nil)
   (deps :accessor deps :initarg :deps :initform nil)
   (rule :accessor rule :initarg :rule :initform nil)
   ))

(defclass tptp-include-statement ()
  ((path :accessor path :initarg :path :initform (error "Include statement must have a path"))
   ; include taking an empty subset of formulas is useless, so NIL = no restriction
   (args :accessor args :initarg :args :initform nil)))

(defun tptp-extract-deps (tree)
  (let* ((op (first tree))
         (args (second (second tree))))
    (case op
      ((tptp-syntax::general_term
         tptp-syntax::source)
       (when (symbolp (first args))
         (tptp-extract-deps args)))
      ((tptp-syntax::general_data)
       (tptp-extract-deps args))
      ((tptp-syntax::atomic_word tptp-syntax::number)
       (list (tptp-true-text (tptp-no-space tree))))
      ((tptp-syntax::general_list)
       (reduce 'append (mapcar 'tptp-extract-deps
                               (tptp-general-term-as-funcall tree))))
      ((tptp-syntax::general_function)
       (multiple-value-bind (parameters name)
         (tptp-general-term-as-funcall tree)
         (when (equal name "inference")
           (tptp-extract-deps (third parameters)))))
      (t nil))))

(defun tptp-parse-tree-to-statement-object (f)
  (let* ((op (first f))
         (args (second (second f))))
    (ecase op
      ((tptp-syntax::tptp_input
         tptp-syntax::annotated_formula)
       (tptp-parse-tree-to-statement-object args))
      ((tptp-syntax::include)
       (make-instance 'tptp-include-statement
                      :path (tptp-true-text (nth 2 args))
                      :args (mapcar
                              'tptp-true-text
                              (linked-list-components
                                  (first-subtree
                                    f 'tptp-syntax::formula_selection)
                                  'tptp-syntax::name_list
                                  'tptp-syntax::name))))
      ((tptp-syntax::thf_annotated
         tptp-syntax::tff_annotated
         tptp-syntax::tcf_annotated
         tptp-syntax::fof_annotated
         tptp-syntax::cnf_annotated
         tptp-syntax::tpi_annotated)
       (let* ((lead-in (nth 0 args))
              (kind (cl-ppcre:regex-replace-all "[^a-z]" lead-in ""))
              (name (tptp-true-text (tptp-no-space (nth 2 args))))
              (role (tptp-true-text (tptp-no-space (nth 6 args))))
              (formula-tree (nth 10 args))
              (annotations-tree (nth 12 args))
              (source-tree (first-subtree annotations-tree
                                          'tptp-syntax::source))
              (optional-info-tree (first-subtree annotations-tree
                                                 'tptp-syntax::optional_info))
              (source-as-function (multiple-value-list
                                    (tptp-general-term-as-funcall
                                      (second (second
                                                source-tree)))))
              (rule (and (equal (second source-as-function) "inference")
                         (tptp-true-text
                           (tptp-no-space
                             (first (first source-as-function))))))
              (deps (tptp-extract-deps source-tree))
              )
         (make-instance 'tptp-statement
                        :name name :role role :kind kind
                        :content (tptp-parse-tree-to-object formula-tree)
                        :source source-tree
                        :extra-annotations optional-info-tree
                        :rule rule :deps deps))))))

(defmethod print-object ((obj tptp-include-statement) s)
  (format s "include('~a'~a~{~a~#[~:;,~]~}~a)."
          (cl-ppcre:regex-replace-all "[\\\\']" (path obj) "\\\\\\&")
          (if (args obj) ",[" "")
          (mapcar 'tptp-constant-name (args obj))
          (if (args obj) "]" "")))

(defmethod print-object ((obj tptp-statement) s)
  (format s "~a(~a,~a,~a~a~a~a)."
          (kind obj) (name obj) (role obj)
          (content obj) (if (source obj) "," "")
          (if (source obj)
            (extract-parse-tree-text (source obj) nil) "")
          (if (extra-annotations obj)
            (extract-parse-tree-text (extra-annotations obj) nil) "")))

(defun statement-parse-to-object (s)
  (tptp-parse-tree-to-statement-object
    (first (parse-tptp s))))
