(in-package :gen-fof-proof)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (import 'uiop:getenv))

(defun setenv (var value)
  (setf (getenv var) value))

(defun unsetenv (var)
  (setf (getenv var) nil))

(defvar *tpi-handlers*
  (make-hash-table :test 'equal))

(defvar *tpi-include-dir* nil)

(defmacro def-tpi-handler (s (&rest args) &rest body)
  (let ((sym (make-symbol s)))
    `(setf
       (gethash ,s *tpi-handlers*)
       (labels ((,sym (,@args) ,@body)) (function ,sym)))))

(defun run-tpi-handler (s data context &rest extra-args)
  (apply
    (or 
      (gethash s *tpi-handlers*)
      'values)
    data context
    extra-args))

(defmethod process-exit-value ((x null) context) 0)
(defmethod process-exit-value ((x string) context)
  (let* ((expanded (expand-getenv x context)))
    (if (equal expanded "") 0 (parse-integer expanded))))
(defmethod process-exit-value ((x tptp-predicate-call) context)
  (process-exit-value (first (args x)) context))
(defmethod process-exit-value ((x tptp-propositional-constant) context)
  (process-exit-value (name x) context))
(defmethod process-exit-value ((x tptp-object-constant) context)
  (process-exit-value (name x) context))

(defun process-tptp-include (x context)
  (let*
    (
     (file-name (path x))
     (formula_names (args x))
     )
    (run-tpi (alexandria:read-file-into-string
               (format nil "~a/~a" (or *tpi-include-dir* ".") file-name))
             :tptp-names formula_names 
             :initial-context context
             :exit-anyway t)))

(defun run-tpi-file (fn &rest args)
  (let* ((*tpi-include-dir*
           (or *tpi-include-dir*
               (cl-ppcre:regex-replace-all "/[^/]*$" fn ""))))
    (apply 'run-tpi (alexandria:read-file-into-string fn) args)))

(defun
  run-tpi
  (l &key initial-context args exit-anyway exit-processing tptp-names)
  (let*
    (
     (l (if (stringp l) (parse-tptp l) l))
     (context (unless initial-context (make-hash-table)))
     )
    (funcall
      (or exit-processing 'process-exit-value)
      (catch
        'run-tpi
        (if 
          initial-context
          (setf context initial-context)
          (progn
            (setf (gethash :formulae context) nil)
            (setf (gethash :inactive-formulae context)
                  (make-hash-table :test 'equal))
            (setf (gethash :groups context) (list "tpi"))
            (setf (gethash :args context) args)
            (setf (gethash :env context) (make-hash-table :test 'equal))
            ))

        (loop 
          for x in l
          for text := (extract-parse-tree-text x nil)
          for statement := (statement-parse-to-object text)
          for is-include = (progn
                             (assert statement)
                             (typep statement 'tptp-include-statement))
          for content := (unless is-include (content statement))
          for kind := (unless is-include (kind statement))
          if is-include
          do (process-tptp-include statement context)
          else
          if (equal kind "tpi")
          do (let*
               (
                (name (role statement))
                )
               (run-tpi-handler name content context)
               )
          else when (or (null tptp-names)
                        (find (name statement) tptp-names :test 'equal))
          do (push (list (gethash :groups context) text statement)
                   (gethash :formulae context))
          )
        (unless exit-anyway (error "No reasoning capabilities present"))
        )
      context)))

(def-tpi-handler 
  "exit" (x context)
  (loop for f in (gethash :temp-files context) do
	(delete-file f))
  (throw 'run-tpi x))

(defun getenv_value (var context)
  (let*
    (
     (var (or (ignore-errors (parse-integer var)) var))
     (env (or (gethash :env context) (make-hash-table :test 'equal)))
     )
    (if
      (integerp var)
      (nth var (gethash :args context))
      (multiple-value-bind (value success)
        (gethash var env)
        (if success value
          (setf (gethash var env) (getenv var)))))))

(defun setenv_value (var value context)
  (unless (stringp var) (error "cannot assign to this var"))
  ; POSIX prohibits "=" in environment variable names
  ; We can use these for internal purposes
  (unless (cl-ppcre:scan "=" var)
    (setenv var value))
  (setf (gethash var (gethash :env context)) value))

(defpackage :tpi-parsing (:use))

(let*
  ((*package* (find-package :tpi-parsing)))
  (esrap-peg:ast-eval
    (esrap-peg:basic-parse-peg
      "
         SafeChar <- (! '$') (! '(') (! ')') (! '%') .
	 Getenv <- '$getenv(' SafeChar+ ')'
	 GetGroups <- '$getgroups(' SafeChar+ ')'
	 GetArgv <- '$getargv(' SafeChar+ ')'
         Percent <- '%' [a-z]+
	 Any <- .
         TextChar <- Getenv / GetGroups / Percent / Any
	 Text <- TextChar*
      ")))

(esrap-peg:def-peg-matchers
  (
   (SafeChar (?x (first (last ?x))))
   (Getenv ((_ ?x _) `(:getenv ,(s+ (m! #'! ?x)))))
   (GetGroups ((_ ?x _) `(:getgroups ,(s+ (m! #'! ?x)))))
   (GetArgv ((_ ?x _) `(:getargv ,(s+ (m! #'! ?x)))))
   (Percent ((_ ?x) `(:percent ,(s+ ?x))))
   (Any (_ x))
   (TextChar (_ (! x)))
   (Text (_ 
	   (block 
	     nil
	     (loop 
	      with pending := '()
	      with res := '()
	      for y in (append (m! #'! x) (list nil))
	      if (listp y)
	      do (progn
		   (push (s+ (reverse pending)) res)
		   (when y (push y res))
		   (setf pending '())
		   )
	      else do
	      (push y pending)
	      finally (return (reverse res))
	      ))))
   )
  :abbreviations
  (
   (! (x) (esrap-peg:ast-eval x))
   (n1 (x) (first x)) (n2 (x) (second x)) (n3 (x) (third x))
   (n4 (x) (fourth x)) (n5 (x) (fifth x)) (n6 (x) (sixth x))
   (n7 (x) (seventh x)) (n8 (x) (eighth x)) (n9 (x) (ninth x))
   (s+ (x) (apply 'concatenate 'string x))
   (l+ (x) (apply 'append x))
   (m! (f x) (mapcar f x))
   )
  :package :tpi-parsing
  :arg x
  )

(defun formulae-strings-by-group (groups context)
  (loop with inactive-ht := (gethash :inactive-formulae context)
        for x in (reverse (gethash :formulae context))
        for statement := (third x)
        for name := (name statement)
        unless (and
                 (gethash name inactive-ht)
                 ; conjecture shouldn't be just skipped
                 (not (equal (role statement) "conjecture")))
        when (intersection groups (first x) :test 'equal)
        collect 
        (second x)))

(defun expand-getenv (s context)
  (apply
    'concatenate 'string
    (loop
      for x in
      (if (stringp s)
        (esrap-peg:ast-eval (esrap:parse 'tpi-parsing::Text s))
        s)
      collect
      (if
	(listp x)
        (case (first x)
          ((:getenv) (getenv_value (second x) context))
          ((:getargv) (getenv_value (second x) context))
          ((:percent)
           (assert (equal (second x) "s"))
           (expand-getenv `((:getgroups "tpi")) context))
          ((:getgroups)
           (let* ((fs (formulae-strings-by-group
                        (remove ""
                                (cl-ppcre:split
                                  "[^a-zA-Z_0-9]" (second x))
                                :test 'equal)
                        context))
                  (name (run-tpi-handler "mktemp" nil context fs)))
             (with-open-file (f name :direction :output :if-exists :supersede)
               (format f "~{~a~%~}" fs))
             name))
          (t (error "Impossible entry")))
	x))))

(def-tpi-handler
  "input" (name context)
  (run-tpi
    (alexandria:read-file-into-string 
      (expand-getenv
	(name name)
	context))
    :initial-context context
    :exit-anyway t))

(def-tpi-handler
  "output" (name context)
  (assert (typep name 'tptp-propositional-constant))
  (let*
    (
     (name (expand-getenv (name name) context))
     )
    (let*
      ((f
         (if 
           (equal name "stdout") 
           *standard-output*
           (progn
             (ensure-directories-exist name)
             (open name :direction :output :if-exists :supersede)))))
      (format f "~{~a~%~}"
              (formulae-strings-by-group (list "tpi") context))
      (unless (equal name "stdout") (close f))
      )))

(def-tpi-handler
  "dump" (name context)
  (assert (typep name 'tptp-propositional-constant))
  (let*
    (
     (name (expand-getenv (name name) context))
     )
    (format *trace-output* "Dump started. Saving formulae data to ~s~%" name)
    (let*
      ((f
         (if 
           (equal name "stdout") 
           *standard-output*
           (progn
             (assert (> (length name) 0))
             (ensure-directories-exist name)
             (open name :direction :output :if-exists :supersede)))))
      (format f "~{~a~%~}"
              (loop for x in (reverse (gethash :formulae context))
                    collect (third x)))
      (format f "~{~{tpi(ag_~a_assign_group_~a_~a,assign_group,~a=~a).~%~}~}"
              (loop for x in (reverse (gethash :formulae context))
                    for groups := (remove "tpi" (first x) :test 'equal)
                    for statement := (third x)
                    for name := (name statement)
                    collect
                    (loop for g in groups
                          for hash := (sha512 (list :assign-group name g))
                          collect name
                          collect g
                          collect hash
                          collect name
                          collect g)))
      (maphash (lambda (k v)
                 (when v
                   (format f "tpi(deactivate_~a, deactivate, ~a).~%" k k)))
               (gethash :inactive-formulae context))
      (unless (equal name "stdout") (close f)))
    (format *trace-output* "Dump complete~%")))

(def-tpi-handler 
  "delete" (name context)
  (let*
    (
     (name (name name))
     )
    (setf
      (gethash :formulae context)
      (remove-if
	(lambda (x)
	  (equal
	    name
	    (name (third x))))
	(gethash :formulae context)))))

(def-tpi-handler
  "start_group" (name context)
  (pushnew (name name) (gethash :groups context) :test 'equal))
(def-tpi-handler
  "end_group" (name context)
  (let*
    (
     (name (name name))
     )
    (when
      (equal name "tpi")
      (error "'tpi' group cannot be ended"))
    (setf (gethash :groups context)
	  (remove
	    name
	    (gethash :groups context)
	    :test 'equal
	    ))))
(def-tpi-handler
  "delete_group" (name context)
  (let*
    (
     (name (name name))
     )
    (setf
      (gethash :formulae context)
      (remove
	name
	(gethash :formulae context)
	:key 'first
	:test (lambda (x y) (find x y :test 'equal))
	))))
(def-tpi-handler
  "setenv" (data context)
  (setenv_value (name (first (args data)))
                (expand-getenv (name (second (args data))) context)
                context))
(def-tpi-handler
  "unsetenv" (data context)
  (let*
    ((var (name data)))
    (unsetenv var)
    (remhash var (gethash :env context))))

(defun temp-file-name (&optional marker
                                 (hash-length 10))
  (namestring
    (uiop:tmpize-pathname
      (format nil "/tmp/tpi-~a/tpi-lemma-check-~a.p"
              (uiop:getenv "USER")
              (subseq (sha512 (list marker (get-universal-time)))
                      0 hash-length)))))

(def-tpi-handler
  "mktemp" (data context &optional marker)
  (let*
    (
     (var (and data (name data)))
     (name (temp-file-name (list var marker)))
     )
    (when (> (length var) 0)
      (setenv_value var name context))
    (push name (gethash :temp-files context))
    name))

(defvar *execute-handlers* (make-hash-table :test 'equal))
(defmacro
  def-exec-handler (s (cmdname) &rest body)
  `(setf
     (gethash ,s *execute-handlers*)
     (lambda (,cmdname) ,@body)
     )
  )
(def-exec-handler
  "" (cmd)
  (with-output-to-string
    (s)
    (uiop:run-program cmd :output s)))
(defun
  execution-handler
  (command)
  (let* ((handler-name (cl-ppcre:regex-replace-all
                         "^(#([^#]*)#)?.*" command "\\2"))
         (cmd-text (cl-ppcre:regex-replace-all "^#[^#]*#" command ""))
         (executor (or (gethash handler-name *execute-handlers*) 'values)))
    (funcall executor cmd-text)))

(defvar *execution-result-handler* nil)

(defun prepare-execution (data context)
  (assert (or (typep data 'tptp-propositional-constant)
              (typep data 'tptp-predicate-infix)))
  (let* ((assignment
           (when (typep data 'tptp-predicate-infix)
             (assert (equal (name data) "="))
             (args data)))
         (cmd-arg (if assignment (second assignment) data))
         (assign-var (when assignment (name (first assignment))))
         (command (expand-getenv (name cmd-arg) context)))
    (values command assign-var)))

(defun run-execution (command &key quiet log-file)
  (unless quiet
    (format *trace-output* "Executing command:~%~a~%" command))
  (when (> (length log-file) 0)
    (ensure-directories-exist log-file)
    (alexandria:write-string-into-file
      (concatenate 'string command (list #\Newline))
      log-file
      :if-does-not-exist :create :if-exists :append))
  (execution-handler command))

(defun run-traced-execution (command assign-var context
                                     &key 
                                     trace-context log-file quiet
                                     execution-result-handler)
  (handler-case
    (let* ((s (run-execution command
                             :quiet quiet
                             :log-file log-file))
           (s (or (and execution-result-handler
                       (funcall execution-result-handler s))
                  s)))
      (cond (assign-var (setenv_value assign-var s context))
            (quiet nil)
            (t (format *standard-output* "~a~%" s)))
      s)
    (serious-condition
      (e)
      (error "Execution failed with error:~%~a~%In context:~%~a~%"
             e trace-context))))

(def-tpi-handler
  "execute" (data context
                  &key
                  trace-context
                  (execution-result-handler
                    nil
                    execution-result-handler-specified-p)
                  (log-file nil log-file-specified-p)
                  (quiet nil quiet-specified-p))
  (multiple-value-bind
    (command assign-var) (prepare-execution data context)
    (let* ((log-file
             (if log-file-specified-p log-file
               (getenv_value "=TPI_EXECUTE_LOG" context)))
           (quiet
             (if quiet-specified-p quiet
               (getenv_value "=TPI_EXECUTE_QUIET" context)))
           (execution-result-handler
             (if execution-result-handler-specified-p
               execution-result-handler
               *execution-result-handler*)))
      (run-traced-execution
        command assign-var context
        :execution-result-handler execution-result-handler
        :trace-context trace-context
        :quiet quiet
        :log-file log-file))))

(def-tpi-handler
  "filter" (data context)
  (let*
    ((assignment (typep data 'tptp-propositional-infix)))
    (when assignment (error "Unimplemented SZS status filtering yet"))
    (let* ((command (expand-getenv (name data) context)))
      (setf (gethash :formulae context) nil)
      (run-tpi (execution-handler command)
               :exit-anyway t
               :initial-context context))))

(def-tpi-handler
  "assert" (data context)
  (assert (typep data 'tptp-propositional-infix) () "Unknown assertion type")
  (cond ((equal (name data) "=")
         (assert
           (equal
             (expand-getenv (first (args data)) context)
             (expand-getenv (second (args data)) context))))
        ((equal (name data) "!=")
         (assert
           (not
             (equal
               (expand-getenv (first (args data)) context)
               (expand-getenv (second (args data)) context)))))
        (t (error "Unknown assertion type: ~a" (name data)))))

(def-tpi-handler
  "write" (data context)
  (format *standard-output* "~a~%"
          (expand-getenv (name data) context)))

(def-tpi-handler
  "write_file" (data context)
  (assert (typep data 'tptp-predicate-infix))
  (assert (equal (name data) "="))
  (let* ((file (expand-getenv (name (first (args data))) context))
         (content (expand-getenv (name (second (args data))) context)))
    (ensure-directories-exist file)
    (alexandria:write-string-into-file content file
                                       :if-exists :supersede)))

(def-tpi-handler
  "append_file" (data context)
  (assert (typep data 'tptp-predicate-infix))
  (assert (equal (name data) "="))
  (let* ((file (expand-getenv (name (first (args data))) context))
         (content (expand-getenv (name (second (args data))) context)))
    (ensure-directories-exist file)
    (alexandria:write-string-into-file content file
                                       :if-exists :append
                                       :if-does-not-exist :create)))
