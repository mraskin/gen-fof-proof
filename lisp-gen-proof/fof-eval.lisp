(in-package :gen-fof-proof)

(defun evaluate-fof (formula known-values
                             &key all-objects variables verbose)
  (when (stringp formula)
    (return-from evaluate-fof
                 (evaluate-fof
                   (esrap:parse 'tptp-syntax::fof_formula formula)
                   known-values :all-objects all-objects
                   :variables variables :verbose verbose)))
  (let* ((op (first formula))
         (args (second (second formula))))
    (ecase op
      ((tptp-syntax::fof_formula
         tptp-syntax::fof_logic_formula
         tptp-syntax::fof_binary_formula
         tptp-syntax::fof_binary_assoc
         tptp-syntax::fof_atomic_formula
         tptp-syntax::fof_defined_atomic_formula
         tptp-syntax::fof_plain_atomic_formula
         tptp-syntax::fof_unit_formula
         tptp-syntax::fof_term
         tptp-syntax::fof_function_term
         )
       (evaluate-fof args known-values :all-objects all-objects
                     :variables variables :verbose verbose))
      ((tptp-syntax::fof_unitary_formula)
       (if (equal (first args) "(" #+nil ")") 
         (evaluate-fof (third args) known-values 
                       :all-objects all-objects
                       :variables variables :verbose verbose)
         (evaluate-fof args known-values :all-objects all-objects
                       :variables variables :verbose verbose)))
      ((tptp-syntax::fof_binary_nonassoc)
       (let* ((arguments
                (list (first args)
                      (fifth args)))
              (op (tptp-true-text (tptp-no-space (third args))))
              (values
                (loop for a in arguments
                      collect (evaluate-fof a known-values
                                            :all-objects all-objects
                                            :variables variables :verbose verbose)))
              (value
                (cond
                  ((equal op "<=>")
                   (cond ((find :undefined values) :undefined)
                         ((apply 'equal values) :true)
                         (t :false)))
                  ((equal op "<~>")
                   (cond ((find :undefined values) :undefined)
                         ((apply 'equal values) :false)
                         (t :true)))
                  ((equal op "=>")
                   (cond ((find :undefined values) :undefined)
                         ((equal values '(:true :false)) :false)
                         (t :true)))
                  ((equal op "<=")
                   (cond ((find :undefined values) :undefined)
                         ((equal values '(:false :true)) :false)
                         (t :true)))
                  ((equal op "~|")
                   (cond ((find :true values) :false)
                         ((find :undefined values) :undefined)
                         (t :true)))
                  ((equal op "~&")
                   (cond ((find :false values) :true)
                         ((find :undefined values) :undefined)
                         (t :false)))
                  (t (error "This should not be possible: unknown op: ~a"
                            op)))))
         (assert (every (lambda (x) (find x '(:true :false :undefined)))
                        values))
         value))
      ((tptp-syntax::fof_or_formula
         tptp-syntax::fof_and_formula)
       (let* ((arguments
                (cons (first args)
                      (mapcar 'third (second args))))
              (op (tptp-true-text (tptp-no-space (first (first (second args))))))
              (values
                (loop for a in arguments
                      collect (evaluate-fof a known-values
                                            :all-objects all-objects
                                            :variables variables :verbose verbose)))
              (value
                (cond
                  ((equal op "|")
                   (cond ((find :true values) :true)
                         ((find :undefined values) :undefined)
                         (t :false)))
                  ((equal op "&")
                   (cond ((find :false values) :false)
                         ((find :undefined values) :undefined)
                         (t :true)))
                  (t (error "This should not be possible: unknown op: ~a"
                            op)))))
         (assert (every (lambda (x) (find x '(:true :false :undefined)))
                        values))
         value))
      ((tptp-syntax::fof_infix_unary
         tptp-syntax::fof_defined_infix_formula)
       (let* ((op (tptp-true-text (tptp-no-space (third args))))
              (arguments (list (first args) (fifth args)))
              (values
                (loop for a in arguments
                      collect (evaluate-fof a known-values
                                            :all-objects all-objects
                                            :variables variables :verbose verbose)))
              (value
                (cond ((find :undefined values) :undefined)
                      ((or
                         (and (equal op "=")
                              (apply 'equal values))
                         (and (equal op "!=")
                              (not (apply 'equal values))))
                       :true)
                      (t :false))))
         (when verbose
           (format *trace-output* "Evaluating FOF. Applying ~s to ~s -> ~s~%"
                   op values value))
         (assert (find op '("=" "!=") :test 'equal))
         (assert (every (lambda (x) (or (stringp x)
                                        (eq x :undefined)))
                        values))
         value))
      ((tptp-syntax::fof_plain_term)
       (if (eq (first args) 'tptp-syntax::constant)
         (let* ((name (tptp-true-text (tptp-no-space args)))
                (value (gethash name known-values :undefined)))
           (when verbose (format *trace-output* "Evaluating FOF. ~a -> ~s~%"
                                 name value))
           value)
         (let* ((name (tptp-true-text (tptp-no-space (first args))))
                (arguments (evaluate-fof (fifth args) known-values
                                         :all-objects all-objects
                                         :variables variables :verbose verbose))
                (request (cons name arguments))
                (value (gethash request known-values :undefined)))
           (when verbose
             (format *trace-output* "Evaluating FOF. ~s -> ~s~%"
                     request value))
           value)))
      ((tptp-syntax::fof_arguments)
       (if (eq (first args) 'tptp-syntax::fof_term)
         (list
           (evaluate-fof args known-values :all-objects all-objects
                         :variables variables :verbose verbose))
         (cons (evaluate-fof (first args) known-values
                             :all-objects all-objects
                             :variables variables :verbose verbose)
               (evaluate-fof (fifth args) known-values
                             :all-objects all-objects
                             :variables variables :verbose verbose))))
      ((tptp-syntax::fof_variable_list)
       (if (eq (first args) 'tptp-syntax::variable)
         (list (tptp-true-text (tptp-no-space args)))
         (cons (tptp-true-text (tptp-no-space (first args)))
               (evaluate-fof (fifth args) known-values
                             :all-objects all-objects
                             :variables variables :verbose verbose))))
      ((tptp-syntax::variable)
       (cdr (or (assoc (tptp-true-text (tptp-no-space formula))
                       variables :test 'equal)
                (cons :undefined :undefined))))
      ((tptp-syntax::fof_quantified_formula)
       (let* ((quantifier (tptp-true-text (tptp-no-space (nth 0 args))))
              (q-variables (evaluate-fof (nth 4 args)
                                       known-values
                                       :all-objects all-objects
                                       :variables variables :verbose verbose))
              (content (nth 10 args))
              (blind (evaluate-fof content known-values
                                   :all-objects all-objects
                                   :variables (append
                                                (loop
                                                  for v in q-variables
                                                  collect (cons
                                                            v :undefined))
                                                variables) :verbose verbose)))
         (if (and (eq blind :undefined) all-objects)
           (labels
             ((try (q-vars vars)
                   (if q-vars 
                     (loop with var := (first q-vars)
                           with undef := nil
                           for value in all-objects
                           for evaluation := (try (rest q-vars)
                                                  (cons (cons var value)
                                                        vars))
                           do (assert
                                (find evaluation
                                      '(:true :false :undefined)))
                           when (equal evaluation :undefined)
                           do (setf undef t)
                           when (and (equal quantifier "!")
                                     (eq evaluation :false))
                           return :false
                           when (and (equal quantifier "?")
                                     (eq evaluation :true))
                           return :true
                           finally
                           (return (cond (undef :undefined)
                                         ((equal quantifier "!") :true)
                                         ((equal quantifier "?") :false)
                                         (t (error
                                              "This should not be possible: unknown quantifier: ~a"
                                              quantifier)))))
                     (evaluate-fof content known-values
                                   :all-objects all-objects
                                   :variables vars :verbose verbose))))
             (try q-variables variables))
           blind)))
      ((tptp-syntax::fof_unary_formula)
       (if (eq (first args) 'tptp-syntax::fof_infix_unary)
         (evaluate-fof args known-values :all-objects all-objects
                       :variables variables :verbose verbose)
         (let* ((op (tptp-true-text (tptp-no-space (first args))))
                (content (third args))
                (value
                  (evaluate-fof content known-values
                                :all-objects all-objects
                                :variables variables :verbose verbose)))
           (assert (equal op "~"))
           (ecase value
             ((:true) :false)
             ((:undefined) :undefined)
             ((:false) :true)))))
      )))
