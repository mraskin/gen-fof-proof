(in-package :gen-fof-proof)

(defmethod fof-constant-names ((f tptp-leaf)) (list))
(defmethod fof-constant-names ((f tptp-internal-node))
  (reduce (lambda (x y) (union x y :test 'equal))
          (mapcar 'fof-constant-names (args f))))
(defmethod fof-constant-names ((f tptp-content-node))
  (fof-constant-names (content f)))

(defmethod fof-constant-names ((f tptp-propositional-constant))
  (list (name f)))
(defmethod fof-constant-names ((f tptp-object-constant))
  (list (name f)))
(defmethod fof-constant-names ((f tptp-function-call))
  (cons (name f) (call-next-method)))
(defmethod fof-constant-names ((f tptp-predicate-call))
  (cons (name f) (call-next-method)))

(defmethod fof-rename-constants (f (replacements list))
  (let* ((ht (make-hash-table :test 'equal)))
    (loop for r in replacements
          do (setf (gethash (car r) ht) (cdr r)))
    (fof-rename-constants f ht)))

(defmethod fof-rename-constants ((f tptp-leaf) (replacements hash-table)) f)
(defmethod fof-rename-constants ((f tptp-internal-node) (replacements hash-table))
  (make-instance (class-of f) :name (name f)
                 :args (loop for a in (args f) collect
                             (fof-rename-constants a replacements))))
(defmethod fof-rename-constants ((f tptp-content-node) (replacements hash-table))
  (make-instance (class-of f) :name (name f)
                 :args (loop for a in (args f) collect
                             (fof-rename-constants a replacements))
                 :content (fof-rename-constants (content f) replacements)))

(defmethod fof-rename-constants ((f tptp-object-constant) (replacements hash-table))
  (let* ((name (name f))
         (new-name (gethash name replacements)))
    (if new-name
      (make-instance (class-of f) :name new-name)
      f)))
(defmethod fof-rename-constants ((f tptp-propositional-constant) (replacements hash-table))
  (let* ((name (name f))
         (new-name (gethash name replacements)))
    (if new-name
      (make-instance (class-of f) :name new-name)
      f)))
(defmethod fof-rename-constants ((f tptp-function-call) (replacements hash-table))
  (let* ((name (name f))
         (new-name (gethash name replacements name)))
    (make-instance (class-of f)
                   :name new-name
                   :args (loop for a in (args f) collect
                               (fof-rename-constants a replacements)))))
(defmethod fof-rename-constants ((f tptp-predicate-call) (replacements hash-table))
  (let* ((name (name f))
         (new-name (gethash name replacements name)))
    (make-instance (class-of f)
                   :name new-name
                   :args (loop for a in (args f) collect
                               (fof-rename-constants a replacements)))))

(defun rename-all-fof-constants (text)
  (let* ((parse (parse-tptp text))
         (statements (mapcar 'tptp-parse-tree-to-statement-object parse))
         (fofs (remove-if (lambda (x)
                            (and (not (equal (kind x) "fof"))
                                 (not (equal (kind x) "cnf"))))
                          statements))
         (constant-names
           (reduce
             (lambda (x y) (union x y :test 'equal))
             (mapcar 'fof-constant-names
                     (mapcar 'content fofs))))
         (renamings
           (loop for c in constant-names
                 for k upfrom 1
                 for nn := (format nil "object_~8,'0d" k)
                 collect (cons c nn)))
         (renamed
           (loop for s in statements
                 collect
                 (if (and (or (equal (kind s) "fof")
                              (equal (kind s) "cnf")))
                   (make-instance
                     (class-of s)
                     :content (fof-rename-constants (content s) renamings)
                     :name (name s)
                     :role (role s)
                     :kind (kind s))
                   s))))
    (format nil "~{~a~%~}" renamed)))
