(defpackage :gen-fof-proof
  (:use :iterate :common-lisp
	:cl-ppcre)
  (:export 
    #:prove-by-steps
    #:read-fof-file
    #:invoke-some-prover
    #:invoke-eprover
    #:load-tptp-syntax-bnf
    #:load-tptp-file
    #:parse-tptp
    #:test-tptp-file-loading
    #:extract-parse-tree-text
    #:alter-parse-tree
    #:run-tpi
    #:run-tpi-file
    #:def-tpi-handler
    #:tptp-true-text
    #:fof-free-vars
    #:tptp-proof-to-dot
    #:tptp-proof-to-vue
    #:*tpi-include-dir*
    #:evaluate-fof
    )
  )
