(in-package :gen-fof-proof)

(defun write-vue-lead-in (stream)
  (format stream "<?xml version=\"1.0\" encoding=\"US-ASCII\"?>~%")
  (format stream "<LW-MAP xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">~%")
  (format stream "<fillColor>#FFFFFF</fillColor>~%"))
(defun write-vue-lead-out (stream)
  (format stream "</LW-MAP>~%"))

(defun string-to-entities (s)
  (format nil "~{&#x~x;~}" (map 'list 'char-code s)))

(defun string-to-vue (s)
  (let* ((lines (cl-ppcre:split (format nil "[~{~a~}]" (list #\Newline #\Return)) s))
         (lines-encoded (mapcar 'string-to-entities lines))
         (full (format nil "~{~a~#[~:;%nl;~]~}" lines-encoded)))
    full))

(defun write-vue-node (stream num label &key (scale 100) (fontsize 10) notes color shape shape-args x y)
  (format
    stream "<child ID=\"~a\" label=\"~a\" x=\"~a\" y=\"~a\" strokeWidth=\"1\" autoSized=\"true\" xsi:type=\"node\"> <font>SansSerif-plain-~a</font> ~{ ~a~}</child>~%"
    num (string-to-vue label) (or x (* num scale)) (or y 0) fontsize
    (list
      (if notes (format nil "<notes>~a</notes>" (string-to-vue notes)) "")
      (if color (format nil "<fillColor>~a</fillColor>" color) "")
      (if shape (format nil "shape xsi:type=\"~a\" ~{ ~a=\"~a\"~}/>" shape shape-args) "")
      )
    )
  )
(defun write-vue-edge (stream num label num-s num-e &key (fontsize 10) notes)
  (format
    stream "<child ID=\"~a\" label=\"~a\" strokeWidth=\"1.0\" autoSized=\"true\" arrowState=\"2\" xsi:type=\"link\"> <font>SansSerif-plain-~a</font> <ID1 xsi:type=\"node\">~a</ID1> <ID2 xsi:type=\"node\">~a</ID2> ~{ ~a~}</child>~%"
    num (string-to-vue label) fontsize num-s num-e
    (list (if notes (format nil "<notes>~a</notes>" (string-to-vue notes)) ""))))

