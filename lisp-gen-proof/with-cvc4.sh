#! /bin/sh

export cvc4="$(which cvc4 2>/dev/null || echo "$(nix-build --no-out-link '<nixpkgs>' -A cvc4)/bin/cvc4")"

time="${1:-15}"
args="$2"
command="$3"

export PS1="<TPI> $PS1"

shift; shift; shift

TPI_PROVER="$cvc4 -L tptp --tlimit=${time}000 $args" TPI_CAT_SKIP_EQUIASSUME="1" "${command:-$SHELL}" "$@"
