(in-package :gen-fof-proof)

(defparameter *alter-parse-tree-recursion* nil)

(defun alter-parse-tree (tree function-list)
  (let*
    ((ht nil)
     (*alter-parse-tree-recursion* function-list))
    (if 
      (hash-table-p function-list)
      (setf ht function-list)
      (progn
        (setf ht (make-hash-table))
        (loop for f in function-list do
              (setf (gethash (first f) ht) (cdr f)))))
    (cond
      ((stringp tree) 
       (funcall (or (first (gethash :default-string ht)) 'identity) tree))
      ((not (listp tree)) (error "Tree format"))
      ((and
         (first tree)
         (symbolp (first tree)))
       (let*
         ((f (or
               (gethash (first tree) ht)
               (gethash :default ht)
               )))
         (if
           f 
           (if 
             (second f)
             (cons (first tree) 
                   (mapcar (lambda (x) 
                             (funcall
                               (first f) (alter-parse-tree x ht)))
                           (cdr tree)))
             (funcall (first f) 
                      (if
                        (third f)
                        (list
                          (first tree)
                          (mapcar
                            (lambda (x) (alter-parse-tree x ht))
                            (cdr tree))
                          )
                        tree
                        )
                      )
             )
           (cons (first tree) 
                 (mapcar (lambda (x) (alter-parse-tree x ht)) 
                         (cdr tree)))
           )))
      (t 
        (funcall
          (or (first (gethash :default-list ht)) 'identity)
          (mapcar (lambda (x) (alter-parse-tree x ht)) tree))))))

(defun extract-parse-tree-text (tree function-list)
  (let*
    ((ht nil))
    (if 
      (hash-table-p function-list)
      (setf ht function-list)
      (progn
        (setf ht (make-hash-table))
        (loop for f in function-list do
              (setf (gethash (first f) ht) (cdr f)))))
    (cond
      ((stringp tree) tree)
      ((not (listp tree)) (error "Tree format"))
      ((symbolp (first tree))
       (let*
         ((f (gethash (first tree) ht)))
         (if
           f 
           (if 
             (second f)
             (funcall 
               (first f) 
               (cons (first tree) 
                     (mapcar (lambda (x) (extract-parse-tree-text x ht)) 
                             (cdr tree))))
             (funcall (first f) tree)
             )
           (apply 
             'concatenate 'string
             (mapcar (lambda (x) (extract-parse-tree-text x ht)) 
                     (cdr tree))))))
      (t (apply
           'concatenate 
           'string
           (mapcar (lambda (x) 
                     (extract-parse-tree-text x ht))
                   tree))))))

(defparameter
  *reduce-space*
  `(
    (tptp-syntax::Sp 
      ,(lambda (x)
         (if
           (position #\Newline (extract-parse-tree-text x nil))
           (format nil "~%")
           " "
           ))
      t)
    ))

(defparameter 
  *kill-space*
  `(
    (tptp-syntax::Sp ,(constantly nil))
    ))

(defun just-take-contents (x)
  (let*
    ((res
       (list
         (first x)
         (extract-parse-tree-text x nil))))
    res
    ))

(defparameter
  *coalesce-identifiers*
  `(
    (tptp-syntax::lower_word just-take-contents)
    (tptp-syntax::upper_word just-take-contents)
    (tptp-syntax::atomic_defined_word just-take-contents)
    (tptp-syntax::atomic_system_word just-take-contents)
    (tptp-syntax::integer just-take-contents)
    (tptp-syntax::rational just-take-contents)
    (tptp-syntax::real just-take-contents)
    (tptp-syntax::single_quoted 
      ,(lambda (x) 
         (let* ((s (extract-parse-tree-text x nil)))
           (list 
             (first x)
             "'"
             (subseq s 1 (- (length s) 1))
             "'"))))
    )
  )

(defun explode-list (l)
  (if
    (listp l)
    (if (= (length l) 1)
      (first l)
      l)
    l))
(defun remove-nil (l)
  (if
    (listp l)
    (remove nil l)
    l))

(defun remove-level (x)
  (explode-list (cdr x)))

(defparameter 
  *clean-nil*
  `(
    (:default 
      ,(lambda (x) 
         (let* 
           (
            (y (remove-nil (mapcar 'remove-nil x)))
            (res (cons (car y) (mapcar 'explode-list (cdr y))))
            )
           res)) 
      nil t)
    (:default-list
      ,(lambda (x) 
         (let*
           ((y (remove-nil x))
            (res (explode-list (mapcar 'explode-list y)))
            )
           res)))
    )
  )

(defun first-subtree (tree marker &key exceptions)
  (cond 
    ((not (listp tree)) nil)
    ((equal (first tree) marker) tree)
    ((null tree) nil)
    ((find tree exceptions) nil)
    (t (or (first-subtree (car tree) marker :exceptions exceptions)
           (first-subtree (cdr tree) marker :exceptions exceptions)))))

(defun dfs-process-subtrees 
  (tree marker function &key 
        (counter 0) (incf-on-recurse nil) (force-ignore-top nil))
  ; protocol: the processing function gets the subtree and its DFS number
  ; return values: replacement, finalp, abortp, recursep
  ;
  ; the entire function returns the resulting tree and the new counter value 
  ; or nil in case of abort or t in case of the final value being found
  (let*
    (
     (finish nil)
     (c counter)
     (res nil)
     )
    (labels
      ((recurse (x)
                (multiple-value-bind 
                  (replacement new-counter)
                  (dfs-process-subtrees
                    x marker function 
                    :counter c :incf-on-recurse incf-on-recurse)
                  (when (eq new-counter t)
                    (return-from dfs-process-subtrees 
                                 (values replacement t)))
                  (unless new-counter 
                    (setf finish t))
                  (setf c new-counter)
                  (push replacement res)))
       (marker-match (x)
                     (cond
                       ((symbolp marker) 
                        (eq (and (listp x) (first x)) marker))
                       ((functionp marker) (funcall marker x))
                       (t (equal x marker))
                       )
                     ))
      (unless
        (and
          (listp tree)
          (or
            force-ignore-top
            (not (marker-match tree))))
        (return-from 
          dfs-process-subtrees
          (multiple-value-bind
            (replacement new-counter)
            (dfs-process-subtrees 
              (list tree) 
              marker function 
              :counter counter :incf-on-recurse incf-on-recurse)
            (if
              (eq new-counter t)
              (values replacement new-counter)
              (values (first replacement) new-counter))
            )))
      (loop
        for x in tree
        for remainder on tree
        for kind := (and (listp x) (first x))
        for marker-match := (marker-match x)
        when finish do 
        (return-from dfs-process-subtrees 
                     (values (append (reverse res) remainder) nil))
        do
        (cond 
          (marker-match 
            (multiple-value-bind
              (replacement finalp abortp recursep)
              (funcall function x c)
              (when finalp 
                (return-from dfs-process-subtrees (values replacement t)))
              (when
                (or
                  (not recursep)
                  incf-on-recurse)
                (incf c))
              (when abortp 
                (setf finish t))
              (if recursep (recurse replacement) 
                (push replacement res))))
          ((listp x)
           (recurse x))
          (t (push x res)))))
    (values 
      (reverse res) 
      (cond
        (finish nil)
        (t c)))))

(defun all-subtrees (tree marker &key 
                          (recurse nil) (notop nil))
  (let*
    ((res nil))
    (dfs-process-subtrees
      tree marker 
      (if recurse 
        (lambda (x n)
          n
          (let*
            ((subres 
               (all-subtrees x marker 
                             :notop t :recurse t)))
            (loop
              for y in (or subres (list x))
              do (push y res)))
          x)
        (lambda (x n) n (push x res) x))
      :force-ignore-top notop)
    (reverse res)))

(defun function-arguments (tree)
  (all-subtrees tree 'tptp-syntax::term :notop t))

(defun pure-functional-term-to-sexp (tree)
  (let*
    ((arguments (function-arguments tree))
     (main (tptp-true-text (first-subtree 
                             tree 'tptp-syntax::functor))))
    (if arguments
      (cons main (mapcar 'pure-functional-term-to-sexp arguments))
      main)))

(defun tptp-single-quoted-true-content (x)
  (let*
    ((s (extract-parse-tree-text x nil))
     (s (subseq s 1 (- (length s) 1)))
     (s (cl-ppcre:regex-replace-all "\\\\([\\\\'])" s "\\1"))
     )
    s))

(defparameter
  *true-text*
  `(
    (tptp-syntax::single_quoted 
      ,(lambda (x) 
         `(tptp-syntax::text-content 
            ,(tptp-single-quoted-true-content x))))
    ))

(defun
  tptp-true-text (x)
  (extract-parse-tree-text
    (alter-parse-tree
      x *true-text*) nil))

(defun
  extract-subtrees (tree marker)
  (cond
    ((not (listp tree)) nil)
    ((null tree) nil)
    ((equal (first tree) marker) (list tree))
    (t (append
         (extract-subtrees (car tree) marker)
         (extract-subtrees (cdr tree) marker)))))

(defun
  first-non-proxy (tree)
  (block
    nil
    (loop
      with l := tree
      for clean-l := (remove nil l)
      for ultra-clean-l := (remove-if 'symbolp l)
      for next-head := (first ultra-clean-l)
      while 
      (and
        clean-l
        (<= (length clean-l) 2)
        (symbolp (first l))
        (and
          (listp next-head)
          (symbolp (first next-head)))
        )
      do (setf l next-head)
      finally (return next-head)
      )))

(defun list-entries (f)
  (let*
    ((argdata (second (second f))))
    (cond
      ((listp (first argdata))
       (let*
         ((list-data
            (remove-if
              (lambda (x) 
                (or (null x) (stringp x) (symbolp x) 
                    (equal (first x) 'tptp-syntax::Sp))) 
              argdata)))
         (cons (first list-data) (list-entries (second list-data)))))
      (t (list argdata)))))

(defparameter *fof-free-var-extraction* 
  `(
    (tptp-syntax::variable ,(lambda (x) (list (tptp-true-text x))) nil nil)
    (tptp-syntax::fof_quantified_formula 
      ,(lambda (f)
         (let*
           ((var-list (first-subtree f 'tptp-syntax::fof_variable_list))
            (content (first-subtree f 'tptp-syntax::fof_unitary_formula)))
           (set-difference (alter-parse-tree content  *alter-parse-tree-recursion*) 
                           (alter-parse-tree var-list *alter-parse-tree-recursion*)
                           :test 'equal))))
    (:default-string ,(constantly nil))
    (:default ,(lambda (x)
                 (reduce (lambda (&optional a b) (union a b :test 'equal)) 
                         (second x)
                         :initial-value nil))
              nil t)
    (:default-list
      ,(lambda (x) (reduce (lambda (&optional a b) (union a b :test 'equal)) 
                           x :initial-value nil)))
    ))

(defun fof-free-vars (tree)
  (alter-parse-tree
    tree
    *fof-free-var-extraction*
    ))

(defun tptp-entries (l)
  (let* (
         (l (if (stringp l) (parse-tptp l) l))
         (l (alter-parse-tree
              l
              (append
                *kill-space*
                *coalesce-identifiers*
                *clean-nil*
                )))
         (l (if
              (equal (first l) 'tptp-syntax::tptp_file)
              (second l)
              l
              ))
         (l (if
              (equal (first l) 'tptp-syntax::tptp_input)
              (list l)
              l
              ))
         )
    l))

(defun tptp-entries-plist (entries)
  (let* ((entries (tptp-entries entries)))
    (loop for x in entries
          for content := (second (second x))
          for kind := (first content)
          for name := (first-subtree content 'tptp-syntax::name)
          for role := (first-subtree content 'tptp-syntax::formula_role)
          for formula := (find-if
                           (lambda (x)
                             (and (listp x)
                                  (find (first x)
                                        `(
                                          tptp-syntax::fof_formula
                                          tptp-syntax::cnf_formula
                                          tptp-syntax::thf_formula
                                          tptp-syntax::tff_formula
                                          tptp-syntax::tcf_formula
                                          tptp-syntax::tpi_formula
                                          ))))
                           (second content))
          for annotations := (first-subtree content 'tptp-syntax::annotations)
          collect (list :kind kind :name name :role role :formula formula
                        :annotations annotations))))

(defun tptp-no-space (l) (alter-parse-tree l *kill-space*))

(defun fof-extract-binary-assoc (f &key clean)
  (when (listp f)
    (case (first f)
      ((tptp-syntax::fof_formula
         tptp-syntax::fof_logic_formula
         tptp-syntax::fof_binary_formula
         tptp-syntax::fof_binary_assoc)
       (fof-extract-binary-assoc
         (if clean (second f)
           (second (second f)))
         :clean clean))
      ((tptp-syntax::fof_and_formula
         tptp-syntax::fof_or_formula)
       f)
      ((tptp-syntax::fof_unitary_formula)
       (let* ((args (if clean (second (second f)) (second f))))
         (if (equal (first args) "(" #+nil")")
           (fof-extract-binary-assoc
             (nth (if clean 1 2) args)
             :clean clean)
           nil)))
      (t nil))))

(defun fof-assoc-binop (f)
  (let* ((args (second f))
         (args (if (null (first args)) (second args) args))
         (first-tail (first (second args)))
         (binop (if (and (listp first-tail)
                         (listp (second first-tail)))
                  (first first-tail) first-tail))
         (binop (tptp-true-text (tptp-no-space binop))))
    binop))

(defun fof-assoc-components (f)
  (let* ((args (second f))
         (args (if (null (first args)) (second args) args))
         (first-arg (first args))
         (rest-args (second args))
         (rest-args (if (and (listp (first rest-args))
                             (listp (second (first rest-args))))
                      rest-args (list rest-args)))
         (rest-args (loop for a in rest-args
                          collect
                          (if (and (second a)
                                   (not (eq (first (second a))
                                            'tptp-syntax::Sp)))
                            (second a) (third a)))))
    (cons first-arg rest-args)))

(defun linked-list-components (f list-marker head-marker &key taken-head)
  (let* ((root (first-subtree f list-marker :exceptions (list taken-head))))
    (when root
      (let* ((head (first-subtree root head-marker)))
        (cons head
              (linked-list-components
                (second root)
                list-marker head-marker
                :taken-head head))))))

(defun tptp-general-term-as-funcall (tree)
  (let* ((op (first tree))
         (args (second (second tree))))
    (case op
      ((tptp-syntax::general_term
         tptp-syntax::general_data)
       (when (symbolp (first args))
         (tptp-general-term-as-funcall args)))
      ((tptp-syntax::general_list)
       (if (equal "[]" args) nil
         (linked-list-components args
                                 'tptp-syntax::general_terms
                                 'tptp-syntax::general_term)))
      ((tptp-syntax::general_function)
       (values
         (linked-list-components args
                                 'tptp-syntax::general_terms
                                 'tptp-syntax::general_term)
         (tptp-true-text (tptp-no-space (first args)))))
      (t nil))))
