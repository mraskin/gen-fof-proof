(in-package :gen-fof-proof)

(defun filter-tpi-lemmas (from unused-list)
  (let* ((unused-lemmas (make-hash-table :test 'equal)))
    (loop for statement in
          (parse-tptp unused-list)
          for st-obj := (tptp-parse-tree-to-statement-object statement)
          for formula := (content st-obj)
          for ft := (normalise-fof-formula (format nil "~a" formula))
          do (setf (gethash ft unused-lemmas) t))
    (format
      nil "~{~a~}"
      (loop 
        for statement in (parse-tptp from)
        for st-obj := (tptp-parse-tree-to-statement-object statement)
        for formula := (ignore-errors (content st-obj))
        for ft := (and formula
                       (normalise-fof-formula (format nil "~a" formula)))
        for skip := (and (typep st-obj 'tptp-statement)
                    (cl-ppcre:scan
                      "^checked_lemma(_[a-z0-9A-Z_]+)?$" (role st-obj))
                    (gethash ft unused-lemmas))
        when skip do
        (format *trace-output* "Skipping ~a because its formula ~a is in the unused list~%"
                (name st-obj) ft)
        unless skip
        collect (extract-parse-tree-text statement nil)))))

