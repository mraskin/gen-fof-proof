#! /bin/sh

export eprover="$(which eprover 2>/dev/null || echo "$(nix-build --no-out-link '<nixpkgs>' -A eprover)/bin/eprover")"

time="${1:-15}"
args="$2"
command="$3"

export PS1="<TPI> $PS1"

shift; shift; shift

TPI_PROVER="$eprover --auto --soft-cpu-limit=$time -s $args" TPI_PROVER_SIMPLE_OPTIONS=' --auto-schedule ' TPI_PROVER_EXPANSION_OPTIONS=' --auto-schedule --no-preprocessing ' "${command:-$SHELL}" "$@"
