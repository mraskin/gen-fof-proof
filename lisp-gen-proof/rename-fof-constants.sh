#! /bin/sh

file="$1"
shift

if test -n "$file"; then
./nix-load-lisp.sh --eval "(progn $*)" --eval '(alexandria:write-string-into-file (gen-fof-proof::rename-all-fof-constants (alexandria:read-file-into-string "'"$file"'")) "'"$file"'.renamed" :if-exists :supersede)' --eval '(quit)'
else
        ./nix-load-lisp.sh --eval "(progn $*)" --eval '(format t "~a" (gen-fof-proof::rename-all-fof-constants "'"$(cat)"'"))' --eval '(quit)' | grep -E -A 999999999 '^(tpi|fof|cnf)[(]' --line-buffered
fi
