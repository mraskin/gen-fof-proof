#! /bin/sh

target_dir=""
source=""
for i in "$@"; do
        target_dir="$target_dir/$source"
        source="$i"
done

mkdir -p "$target_dir/"

echo '% SZS status Theorem '
echo '% Hopefully... check later'
echo "% Target directory: [[ $target_dir ]]"

cp "$source" "$target_dir/"
