./nix-load-lisp.sh

(test-tptp-file-loading
  (format 
    nil 
    "~a/.nix-personal/personal-unfree-result/tptp/share/tptp/Axioms/"
    (uiop:getenv "HOME")))

(run-tpi-file "data/test/test.tpi")
