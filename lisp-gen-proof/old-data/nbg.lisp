#.(in-package :gen-fof-proof)

#.(ax-failure-excluded "set" 1)
#.(ax-failure-excluded "class" 1)
#.(ax-type-preconditions "in" "set" "class")
#.(ax-subtype "set" "class")
#.(fof-axiom "everythingIsAClass" ("!" ("X") ("|" ("=" "X" "failure") ("class" "X"))))

#.(ax-failure-excluded 'subclass 2)
#.(fof-axiom
    subset-definition
    #.(st-forall-typed
	((class -x) (class -y))
	(<=>
	  (subclass -x -y)
	  (! (-z)
	     (=>
	       (in -z -x)
	       (in -z -y))))))

#.(fof-axiom "extensionality"
	     #.(st-forall-typed (("class" "X") ("class" "Y"))
				("=>"
				 #.(st-forall-typed
				     (("set" "Z"))
				     ("<=>"
				      ("in" "Z" "X")
				      ("in" "Z" "Y")))
				 ("=" "X" "Y"))))

#.(fof-axiom "pairing"
	    #.(st-forall-typed 
		((set -x) (set -y) (set -z))
		(<=>
		  (in -z (pairing-set -x -y))
		  (\|
		    (= -z -x)
		    (= -z -y)))))

#.(fof-axiom 
    union-set-definition
    #.(st-forall-typed 
	((set -x) (set -y))
	(<=>
	  (in -y (union-set -x))
	  (? (-z) (& (in -z -x) (in -y -z))))))

#.(fof-axiom
    power-set-definition
    #.(st-forall-typed
	((set -x) (set -y))
	(<=>
	  (in -y (power-set -x))
	  (! (-z)
	     (<=>
	       (in -z -y)
	       (subclass -z -x))))))

#.(fof-axiom
    empty-set-defintion
    (! (-x) (~ (in -x empty-set))))
#.(fof-axiom
    infinity-has-empty-set
    (in infinity empty-set))
#.(fof-axiom
    infinity-is-inductive
    #.(st-forall-typed
	((set -x))
	(=>
	  (in -x infinity)
	  (in (union-set -x (pairing-set -x -x)) infinity))))

#.(fof-axiom
    regularity
    #.(st-forall-typed
	((class -x))
	#.(st-exists-typed
	    ((set -y))
	    (&
	      (in -y -x)
	      #.(st-forall-typed 
		  ((set -z))
		  (=>
		    (in -z -y)
		    (~ (in -z -x))))))))

#.(fof-axiom
    universe-definition
    #.(st-forall-typed
	((set -x))
	(in -x universe)))

#.(fof-axiom
    size-limitation
    #.(st-forall-typed
	((class -x))
	(<=>
	  (set -x)
	  (~
	    (

#.(fof-axiom "universalClassIncludesAll" 
	     #.(st-forall-typed (("set" "X")) ("in" "X" "universal_class")))
#.(fof-axiom "negatedClassDefinition"
	     #.(st-forall-typed (("set" "X") ("class" "Y")) 
				("<=>" ("in" "X" "Y") ("~" ("in" "X" ("negated_class" "Y"))))))
#.(fof-axiom "unionClassDefinition"
	     #.(st-forall-typed (("set" "X") ("class" "Y") ("class" "Z")) 
				("<=>" ("|" ("in" "X" "Y") ("in" "X" "Z")) ("in" "X" ("union_class" "Y" "Z")))))
#.(fof-axiom "intersectionClassDefinition"
	     #.(st-forall-typed (("set" "X") ("class" "Y") ("class" "Z")) 
				("<=>" ("&" ("in" "X" "Y") ("in" "X" "Z")) ("in" "X" ("intersection_class" "Y" "Z")))))


