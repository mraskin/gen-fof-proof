#.(in-package :gen-fof-proof)

#+nil
(fof-lemma
  -define-diagonal-low-level
  (! (-x -f)
     (=>
       (&
	 (member -x universal_class)
	 (function -f)
	 )
       (? (-y)
	  (= 
	    -y
	    (intersection
	      -x
	      (complement
		(compose
		  (inverse element_relation) -f
		  )
		)
	      ))
	  ))))

#+nil
(fof-lemma
  check-identity
  (! (-y)
     (=>
       (member -y universal_class)
       (member (ordered_pair -y -y) identity_relation)
       )
     )
  )

#.
(fof-lemma 
  pair-idea
  (! (-x -y)
     (=>
       (member -x universal_class)
       (member -x (unordered_pair -x -y)))))

#.
(fof-lemma 
  check-pair
  (! (-x -y -u -v)
     (=>
       (&
	 (member -x universal_class)
	 (member -y universal_class)
	 (member -u universal_class)
	 (member -v universal_class)
	 (= (ordered_pair -x -y) (ordered_pair -u -v))
	 )
       (&
	 (= -x -u)
	 (= -y -v)
	 )
       )
     )
  )

#.
(fof-lemma
  check-sum-class-weakest
  (! (-x -y)
     (=>
       (&
	 (member -x universal_class)
	 (member -y universal_class)
	 )
       (=> (member -y -x) (member -y (sum_class (singleton -x)))))))

#.
(fof-lemma
  check-sum-class-weaker
  (! (-x -y)
     (=>
       (&
	 (member -x universal_class)
	 (member -y universal_class)
	 )
       (<= (member -y -x) (member -y (sum_class (singleton -x)))))))

#.
(fof-lemma
  check-sum-class-weak
  (! (-x -y)
     (=>
       (&
	 (member -x universal_class)
	 (member -y universal_class)
	 )
       (<=> (member -y -x) (member -y (sum_class (singleton -x)))))))

#.
(fof-lemma
  check-sum-class-weak-ish
  (! (-x)
     (=>
       (member -x universal_class)
       (! (-y) (<=> (member -y -x) (member -y (sum_class (singleton -x))))))))

#.
(fof-lemma
  check-extensionality
  (! (-x -y)
     (=> 
       (! (-a) 
	  (<=> (member -a -x) (member -a -y)))
       (= -x -y))))

#.
(fof-logic
  check-trivial-sum-class
  (=>
    (&
      (! (-x -y)
	 (=> 
	   (! (-a) 
	      (<=> (member -a -x) (member -a -y)))
	   (= -x -y)))
      (! (-x)
	 (=>
	   (member -x universal_class)
	   (! (-y) (<=> (member -y -x) (member -y (sum_class (singleton -x)))))))
      )
    (! (-x)
       (=>
	 (member -x universal_class)
	 (= -x (sum_class (singleton -x))))))
  )

#.
(fof
  recent/3
  check-sum-class
  (! (-x)
     (=>
       (member -x universal_class)
       (= -x (sum_class (singleton -x))))))

#.
(fof-lemma
  check-restrict-2
  (! (-f -x -y)
     (=>
       (&
	 (member -x universal_class)
	 (member -y universal_class)
	 (member (ordered_pair -x -y) -f)
	 )
       (? (-p)
	  (member -p (restrict -f (singleton -x) (singleton -y)))
	  )
       )
     )
  )

#.
(fof
  lemma
  check-restrict-3
  (! (-f -x -y)
     (=>
       (&
	 (member -x universal_class)
	 (member -y universal_class)
	 (member (ordered_pair -x -y) -f)
	 )
       (~ (= null_class (restrict -f (singleton -x) (singleton -y))))
       )
     )
  )

#.
(fof
  lemma
  check-restrict-1
  (! (-f -x -y)
     (=>
       (&
	 (= null_class (restrict -f (singleton -x) (singleton -y)))
	 (member -x universal_class)
	 (member -y universal_class)
	 )
       (~ (member (ordered_pair -x -y) -f))
       )
     )
  )

#.
(fof
  lemma
  check-restrict-4
  (! (-f -x)
     (=>
       (&
	 (= null_class (restrict -f (singleton -x) universal_class))
	 (member -x universal_class)
	 )
       (! (-y)
	  (=>
	    (member -y universal_class)
	    (~ (member (ordered_pair -x -y) -f))
	    )
	  )
       )
     )
  )

#.
(fof-lemma
  check-cros-product-1
  (! (-x -y)
     (=>
       (&
	 (member -x universal_class)
	 (member -y universal_class)
	 )
       (member (ordered_pair -x -y) (cross_product (singleton -x) universal_class))
       )
     )
  )

#.
(fof-lemma
  check-null-1
  (! (-x)
     (=>
       (&
	 (~ (= null_class -x))
	 )
       (? (-y)
	  (member -y -x))))
  )

#.
(fof-lemma
  check-cross-product-1
  (! (-x -z)
     (=>
       (&
	 (member -x universal_class)
	 (member -z (cross_product (singleton -x) universal_class))
	 )
       (? (-y)
	  (&
	    (member -y universal_class)
	    (= -z (ordered_pair -x -y))
	    )
	  )
       ))
  )

#.
(fof
  lemma
  check-restrict-5
  (! (-f -x)
     (=>
       (&
	 (member -x universal_class)
	 (~ (= null_class (restrict -f (singleton -x) universal_class)))
	 )
       (? (-y)
	  (&
	    (member -y universal_class)
	    (member (ordered_pair -x -y) -f)
	    )
	  )
       )
     )
  )

#.
(fof-lemma
  check-domain-of-l
  (! 
    (-f -x)
    (=>
      (&
	(member -x universal_class)
	(member -x (domain_of -f))
	)
      (? (-y)
	 (&
	   (member -y universal_class)
	   (member (ordered_pair -x -y) -f))))))

#.
(fof-lemma
  check-domain-of-r
  (! 
    (-f -x)
    (=>
      (member -x universal_class)
      (<=
	(member -x (domain_of -f))
	(? (-y)
	   (&
	     (member -y universal_class)
	     (member (ordered_pair -x -y) -f)))))))

#.
(fof-lemma
  check-domain-of
  (! 
    (-f -x)
    (=>
      (member -x universal_class)
      (<=>
	(member -x (domain_of -f))
	(? (-y)
	   (&
	     (member -y universal_class)
	     (member (ordered_pair -x -y) -f)))))))

#.
(fof-lemma
  check-inverse-1
  (!
    (-f -x -y)
    (=>
      (&
	(member -x universal_class)
	(member -y universal_class)
	)
      (<=>
	(member (ordered_pair -x -y) -f)
	(member (ordered_pair -y -x) (inverse -f))
	)
      )
    )
  )

#+nil
(fof-lemma
  check-range-of
  (! 
    (-f -x)
    (=>
      (member -x universal_class)
      (<=>
	(member -x (range_of -f))
	(? (-y)
	   (&
	     (member -y universal_class)
	     (member (ordered_pair -y -x) -f)))))))

#+nil
(fof-lemma
  check-image
  (! (-f -x -y)
     (=>
       (& 
	 (member -x universal_class)
	 (member -y universal_class)
	 )
       (<=>
	 (member -y (image -f -x))
	 (member
	   (ordered_pair -x -y)
	   -f)))))

#+nil
(fof-lemma
  check-function-many-to-one
  (! (-f -x)
     (=>
       (&
	 (member -x universal_class)
	 (function -f)
	 )
       (! (-y1 -y2)
	  (=> 
	    (&
	      (member (ordered_pair -x -y1) -f)
	      (member (ordered_pair -x -y2) -f)
	      )
	    (= -y1 -y2))))))

#+nil
(fof-lemma
  check-function-image-single-element
  (! (-f -x)
     (=>
       (& 
	 (member -x universal_class)
	 (function -f)
	 )
       (! (-y1 -y2)
	  (=> 
	    (&
	      (member -y1 (image -f -x))
	      (member -y2 (image -f -x))
	      )
	    (= -y1 -y2)
	    )
	  )
       )
     )
  )

#+nil
(fof-lemma
  check-apply
  (! (-x -f)
     (=>
       (&
	 (member -x universal_class)
	 (function -f)
	 )
       (! (-y)
	  (=>
	    (member -y universal_class)
	    (member (apply -f -y) (image -f (singleton -y)))
	    )))))

#+nil
(fof-lemma
  check-diagonal
  (! (-x -f)
     (=>
       (&
	 (member -x universal_class)
	 (function -f)
	 )
       (! (-y)
	  (=>
	    (member -y universal_class)
	    (<=>
	      (member 
		(ordered_pair -y -y)
		(compose
		  element_relation (inverse -f)
		  )
		)
	      (member -y (apply -f -y))
	      ))))))

#+nil
(fof-lemma
  -define-diagonal
  (! (-x -f)
     (=>
       (&
	 (member -x universal_class)
	 (function -f)
	 )
       (? (-y)
	  (! (-z)
	     (<=>
	       (member -z -y)
	       (&
		 (member -z -x)
		 (~ (member -z 
			    (image -f (singleton -z))
			    ))
		 )
	       )
	     )))))

#+nil
(fof-lemma 
  -cantor-theorem 
  (! (-x -f)
     (=>
       (&
	 (member -x universal_class)
	 (function -f)
	 )
       (? (-y)
	  (&
	    (member -y (power_class -x))
	    (~ (member -y (image -f -x))))))))
