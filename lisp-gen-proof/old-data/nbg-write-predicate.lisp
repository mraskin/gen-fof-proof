(in-package :gen-fof-proof)

(defun
  write-nbg-list-rewind-1 (x)
  (format
    nil
    "flip(rotate(inverse(~a)))"
    x))

(defun write-nbg-list-rewind-n (x n)
  (if
    (<= n 0)
    x
    (write-nbg-list-rewind-n
      (write-nbg-list-rewind-1 x) (1- n))
    ))

(defun nbg-cross-product (vars)
  (cond
    ((null vars) "singleton(null_class)")
    ((= (length vars) 1) (first vars))
    (t (format nil "cross-product(~a,~a)"
	       (first vars) (nbg-cross-product (cdr vars))))
    ))

(defun initialize-nbg-var-list (n)
  (nbg-cross-product
    (cons 
      (nbg-cross-product nil)
      (loop for k from 0 to n
	    collect
	    (if 
	      (< k n) 
	      "universal_class"
	      (nbg-cross-product nil))))))

(defun formula-variables (f)
  (mapcar
    (lambda (x) (extract-parse-tree-text x nil))
    (extract-subtrees f 'tptp-syntax::variable)))
