(defun nbg-flip (x)
  (list (list (second (first x)) (first (first x)))
	(second x)))

(defun nbg-rotate (x)
  (list (list (second x) (first (first x))) (second (first x))))

(defun nbg-inverse (x)
  (list (second x) (first x)))
