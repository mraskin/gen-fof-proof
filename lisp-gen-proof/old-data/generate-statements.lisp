(in-package :gen-fof-proof)

(defun uniq (l)
  (let*
    ((ht (make-hash-table :test 'equal))
     res)
    (loop for x in l do (setf (gethash x ht) t))
    (maphash (lambda (k v) (declare (ignorable v)) (push k res)) ht)
    res))

(defun ax-failure-stability (f n)
  (loop for k from 1 to n collect
	`("fof" ,(format nil "failureStableFor_~a_~a" f k)
	  "axiom" 
	  ("=" (,f 
		 ,@(loop for s from 1 to (1- k) collect 
			   (format nil "X~a" s))
		 "failure"
		 ,@(loop for s from (1+ k) to n collect 
			   (format nil "X~a" s))
		   )
	   "failure"))))

(defun ax-failure-excluded (p n)
  (loop for k from 1 to n collect
	`("fof" ,(format nil "failureExclusionFor_~a_~a" p k)
	  "axiom"
	  ("~" (,p
		 ,@(loop for s from 1 to (1- k) collect 
			   (format nil "X~a" s))
		 "failure"
		 ,@(loop for s from (1+ k) to n collect 
			   (format nil "X~a" s))
		   )))))

(defun fof-fold (op &rest args)
  (let*
    ((res (car args)))
    (loop for a in (cdr args) do
	  (setf res (list op a res)))
    res))

(defun ax-type-preservation (f rt &rest types)
  `(
    ("fof"
     ,(format nil "typeBestCaseFor_~a" f)
     "axiom"
     ("=>"
      ,(apply
	 'fof-fold "&"
	 (loop for at in types
	       for k upfrom 1
	       collect
	       `(,at ,(format nil "X~a" k))
	       ))
      (,rt (,f ,@(loop for k from 1 to (length types)
		       collect (format nil "X~a" k))))
      )
     )
    ))
(defun ax-type-requirements (f rt &rest types)
  `(
    ,@(apply 'ax-type-preservation f rt types)
    ("fof"
     ,(format nil "typeFailureCaseFor_~a" f)
     "axiom"
     ("=>"
      ,(apply
	 'fof-fold "|"
	 (loop for at in types
	       for k upfrom 1
	       collect
	       `("~" (,at ,(format nil "X~a" k)))
	       )
	 )
      ("=" "failure" (,f ,@(loop for k from 1 to (length types)
				 collect (format nil "X~a" k))))
      )
     )
    ))

(defun ax-type-preconditions (p &rest types)
  `(
    ("fof"
     ,(format nil "typeFailureCaseFor_~a" p)
     "axiom"
     ("=>"
      ,(apply
	 'fof-fold "|"
	 (loop for at in types
	       for k upfrom 1
	       collect
	       `("~" (,at ,(format nil "X~a" k)))
	       )
	 )
      ("~" (,p ,@(loop for k from 1 to (length types)
		       collect (format nil "X~a" k))))
      )
     )
    ))

(defmacro st-forall-typed (vars body)
  ``("!" ,(mapcar 'second ',vars) 
     ("=>" 
      ,(apply 'fof-fold "&" ',vars)
      ,',body)))

(defmacro st-exists-typed (vars body)
  ``("?" ,(mapcar 'second ',vars) 
     ("&" 
      ,(apply 'fof-fold "&" ',vars)
      ,',body)))

(defun ax-subtype (sub sup)
  `("fof"
    ,(format nil "type_~a_isSubtypeOf_~a" sub sup)
    "axiom"
    ("!" ("X") ("=>" (,sub "X") (,sup "X")))))

(defmacro fof-axiom (name body)
  ``("fof" ,',name "axiom" ,',body))

(defmacro fof-conjecture (name body)
  ``("fof" ,',name "conjecture" ,',body))

(defmacro fof-plain (name body)
  ``("fof" ,',name "plain" ,',body))

(defmacro fof-lemma (name body)
  ``("fof" ,',name "lemma" ,',body))

(defmacro fof-logic (name body)
  ``("fof" ,',name "logic" ,',body))

(defmacro fof-theorem (name body)
  ``("fof" ,',name "theorem" ,',body))

(defmacro fof (kind name body)
  ``("fof" ,',name ,',kind ,',body))

(defmacro fof-fun-abbreviation (name args body)
  ``("fof" ,,(format nil "definitionOf_~a" name)
     "axiom"
     ,',(if args `(! ,args (= (,name ,args) ,body))
	  `(= ,name ,body))))
