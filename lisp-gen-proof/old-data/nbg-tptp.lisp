;------------------------------------------------------------------------------
; File     : SET005+0 : TPTP v5.0.0. Released v2.2.0.
; Domain   : Set Theory
; Axioms   : Set theory axioms based on NBG set theory
; Version  : [Quaife, 1992] axioms : Reduced & Augmented > Complete.
; English  :

; Refs     : [Qua92] Quaife (1992), Automated Deduction in von Neumann-Bern
;          : [BL+86] Boyer et al. (1986), Set Theory in First-Order Logic:
; Source   : [Qua92]
; Names    :

; Status   : Satisfiable
; Syntax   : Number of formulae    :   41 (  16 unit)
;            Number of atoms       :   93 (  16 equality)
;            Maximal formula depth :    7 (   4 average)
;            Number of connectives :   57 (   5 ~  ;   3  |;  23  &)
;                                         (  18 <=>;   8 =>;   0 <=)
;                                         (   0 <~>;   0 ~|;   0 ~&)
;            Number of predicates  :    6 (   0 propositional; 1-2 arity)
;            Number of functors    :   26 (   5 constant; 0-3 arity)
;            Number of variables   :   82 (   0 singleton;  78 !;   4 ?)
;            Maximal term depth    :    4 (   1 average)
; SPC      : 

; Comments :
;          : tptp2X -f sex nbg-tptp.ax 
;------------------------------------------------------------------------------
("fof" "subclass_defn" "axiom" ("!" ("X" "Y") ("<=>" ("subclass" "X" "Y") ("!" ("U") ("=>" ("member" "U" "X") ("member" "U" "Y"))))))

("fof" "class_elements_are_sets" "axiom" ("!" ("X") ("subclass" "X" "universal_class")))

("fof" "extensionality" "axiom" ("!" ("X" "Y") ("<=>" ("=" "X" "Y") ("&" ("subclass" "X" "Y") ("subclass" "Y" "X")))))

("fof" "unordered_pair_defn" "axiom" ("!" ("U" "X" "Y") ("<=>" ("member" "U" ("unordered_pair" "X" "Y")) ("&" ("member" "U" "universal_class") ("|" ("=" "U" "X") ("=" "U" "Y"))))))

("fof" "unordered_pair" "axiom" ("!" ("X" "Y") ("member" ("unordered_pair" "X" "Y") "universal_class")))

("fof" "singleton_set_defn" "axiom" ("!" ("X") ("=" ("singleton" "X") ("unordered_pair" "X" "X"))))

("fof" "ordered_pair_defn" "axiom" ("!" ("X" "Y") ("=" ("ordered_pair" "X" "Y") ("unordered_pair" ("singleton" "X") ("unordered_pair" "X" ("singleton" "Y"))))))

("fof" "cross_product_defn" "axiom" ("!" ("U" "V" "X" "Y") ("<=>" ("member" ("ordered_pair" "U" "V") ("cross_product" "X" "Y")) ("&" ("member" "U" "X") ("member" "V" "Y")))))

("fof" "cross_product" "axiom" ("!" ("X" "Y" "Z") ("=>" ("member" "Z" ("cross_product" "X" "Y")) ("=" "Z" ("ordered_pair" ("first" "Z") ("second" "Z"))))))

("fof" "element_relation_defn" "axiom" ("!" ("X" "Y") ("<=>" ("member" ("ordered_pair" "X" "Y") "element_relation") ("&" ("member" "Y" "universal_class") ("member" "X" "Y")))))

("fof" "element_relation" "axiom" ("subclass" "element_relation" ("cross_product" "universal_class" "universal_class")))

("fof" "intersection" "axiom" ("!" ("X" "Y" "Z") ("<=>" ("member" "Z" ("intersection" "X" "Y")) ("&" ("member" "Z" "X") ("member" "Z" "Y")))))

("fof" "complement" "axiom" ("!" ("X" "Z") ("<=>" ("member" "Z" ("complement" "X")) ("&" ("member" "Z" "universal_class") ("~" ("member" "Z" "X"))))))

("fof" "restrict_defn" "axiom" ("!" ("X" "XR" "Y") ("=" ("restrict" "XR" "X" "Y") ("intersection" "XR" ("cross_product" "X" "Y")))))

("fof" "null_class_defn" "axiom" ("!" ("X") ("~" ("member" "X" "null_class"))))

("fof" "domain_of" "axiom" ("!" ("X" "Z") ("<=>" ("member" "Z" ("domain_of" "X")) ("&" ("member" "Z" "universal_class") ("~" ("=" ("restrict" "X" ("singleton" "Z") "universal_class") "null_class"))))))

("fof" "rotate_defn" "axiom" ("!" ("X" "U" "V" "W") ("<=>" ("member" ("ordered_pair" ("ordered_pair" "U" "V") "W") ("rotate" "X")) ("&" ("member" ("ordered_pair" ("ordered_pair" "U" "V") "W") ("cross_product" ("cross_product" "universal_class" "universal_class") "universal_class")) ("member" ("ordered_pair" ("ordered_pair" "V" "W") "U") "X")))))

("fof" "rotate" "axiom" ("!" ("X") ("subclass" ("rotate" "X") ("cross_product" ("cross_product" "universal_class" "universal_class") "universal_class"))))

("fof" "flip_defn" "axiom" ("!" ("U" "V" "W" "X") ("<=>" ("member" ("ordered_pair" ("ordered_pair" "U" "V") "W") ("flip" "X")) ("&" ("member" ("ordered_pair" ("ordered_pair" "U" "V") "W") ("cross_product" ("cross_product" "universal_class" "universal_class") "universal_class")) ("member" ("ordered_pair" ("ordered_pair" "V" "U") "W") "X")))))

("fof" "flip" "axiom" ("!" ("X") ("subclass" ("flip" "X") ("cross_product" ("cross_product" "universal_class" "universal_class") "universal_class"))))

("fof" "union_defn" "axiom" ("!" ("X" "Y" "Z") ("<=>" ("member" "Z" ("union" "X" "Y")) ("|" ("member" "Z" "X") ("member" "Z" "Y")))))

("fof" "successor_defn" "axiom" ("!" ("X") ("=" ("successor" "X") ("union" "X" ("singleton" "X")))))

("fof" "successor_relation_defn1" "axiom" ("subclass" "successor_relation" ("cross_product" "universal_class" "universal_class")))

("fof" "successor_relation_defn2" "axiom" ("!" ("X" "Y") ("<=>" ("member" ("ordered_pair" "X" "Y") "successor_relation") ("&" ("member" "X" "universal_class") ("&" ("member" "Y" "universal_class") ("=" ("successor" "X") "Y"))))))

("fof" "inverse_defn" "axiom" ("!" ("Y") ("=" ("inverse" "Y") ("domain_of" ("flip" ("cross_product" "Y" "universal_class"))))))

("fof" "range_of_defn" "axiom" ("!" ("Z") ("=" ("range_of" "Z") ("domain_of" ("inverse" "Z")))))

("fof" "image_defn" "axiom" ("!" ("X" "XR") ("=" ("image" "XR" "X") ("range_of" ("restrict" "XR" "X" "universal_class")))))

("fof" "inductive_defn" "axiom" ("!" ("X") ("<=>" ("inductive" "X") ("&" ("member" "null_class" "X") ("subclass" ("image" "successor_relation" "X") "X")))))

("fof" "infinity" "axiom" ("?" ("X") ("&" ("member" "X" "universal_class") ("&" ("inductive" "X") ("!" ("Y") ("=>" ("inductive" "Y") ("subclass" "X" "Y")))))))

("fof" "sum_class_defn" "axiom" ("!" ("U" "X") ("<=>" ("member" "U" ("sum_class" "X")) ("?" ("Y") ("&" ("member" "U" "Y") ("member" "Y" "X"))))))

("fof" "sum_class" "axiom" ("!" ("X") ("=>" ("member" "X" "universal_class") ("member" ("sum_class" "X") "universal_class"))))

("fof" "power_class_defn" "axiom" ("!" ("U" "X") ("<=>" ("member" "U" ("power_class" "X")) ("&" ("member" "U" "universal_class") ("subclass" "U" "X")))))

("fof" "power_class" "axiom" ("!" ("U") ("=>" ("member" "U" "universal_class") ("member" ("power_class" "U") "universal_class"))))

("fof" "compose_defn1" "axiom" ("!" ("XR" "YR") ("subclass" ("compose" "YR" "XR") ("cross_product" "universal_class" "universal_class"))))

; Fixing?
("fof" "compose_defn2" "axiom" ("!" ("XR" "YR" "U" "V") ("<=>" ("member" ("ordered_pair" "U" "V") ("compose" "YR" "XR")) ("&" ("member" "U" "universal_class") ("member" "V" ("image" "XR" ("image" "YR" ("singleton" "U"))))))))

("fof" "function_defn" "axiom" ("!" ("XF") ("<=>" ("function" "XF") ("&" ("subclass" "XF" ("cross_product" "universal_class" "universal_class")) ("subclass" ("compose" "XF" ("inverse" "XF")) "identity_relation")))))

("fof" "replacement" "axiom" ("!" ("X" "XF") ("=>" ("&" ("member" "X" "universal_class") ("function" "XF")) ("member" ("image" "XF" "X") "universal_class"))))

("fof" "disjoint_defn" "axiom" ("!" ("X" "Y") ("<=>" ("disjoint" "X" "Y") ("!" ("U") ("~" ("&" ("member" "U" "X") ("member" "U" "Y")))))))

("fof" "regularity" "axiom" ("!" ("X") ("=>" ("~" ("=" "X" "null_class")) ("?" ("U") ("&" ("member" "U" "universal_class") ("&" ("member" "U" "X") ("disjoint" "U" "X")))))))

("fof" "apply_defn" "axiom" ("!" ("XF" "Y") ("=" ("apply" "XF" "Y") ("sum_class" ("image" "XF" ("singleton" "Y"))))))

("fof" "choice" "axiom" ("?" ("XF") ("&" ("function" "XF") ("!" ("Y") ("=>" ("member" "Y" "universal_class") ("|" ("=" "Y" "null_class") ("member" ("apply" "XF" "Y") "Y")))))))

;------------------------------------------------------------------------------

; Obvious fix?
#.(in-package :gen-fof-proof)

#.
(fof-axiom
  ordered_pair_predicate-definition
  (! (-x)
     (<=>
       (ordered_pair_predicate -x)
       (?
	 (-y -z)
	 (= -x (ordered_pair -y -z))
	 )
       )
     )
  )

#.
(fof-axiom
  -first-defn
  (! (-x -y)
     (<=>
       (&
	 (ordered_pair_predicate -x)
	 (= -y (first -x))
	 )
       (? (-z) (= -x (ordered_pair -y -z)))
       )
     )
  )
#.
(fof-axiom
  -second-defn
  (! (-x -y)
     (<=>
       (&
	 (ordered_pair_predicate -x)
	 (= -y (second -x))
	 )
       (? (-z) (= -x (ordered_pair -z -y)))
       )
     )
  )

#.
(fof-axiom
 -identity-relation-1
 (! (-x -y)
  (<=>
   (member (ordered_pair -x -y) identity_relation)
   (&
    (member -x universal_class)
    (member -y universal_class)
    (= -x -y)
   )
  )
 )
 )

#.
(fof-axiom
  -identity-relation-2
  (! (-z)
     (=>
       (member -z identity_relation)
       (ordered_pair_predicate -z)
       )
     )
  )

#.
(fof-axiom
  -identity-relation-3
  (! (-x)
     (=>
       (member -x identity_relation)
       (ordered_pair_predicate -x)
       ))
  )
