#.(in-package :gen-fof-proof)

; The main notion is set
#.(ax-failure-excluded 'set 1)

; The main predicate on sets beyond equality is being an element of another set
#.(ax-type-preconditions 'in 'set 'set)

; 1. Extensionality
#.(fof-axiom 
    'extensionality
    #.(st-forall-typed
	((set -x) (set -y))
	(=>
	  (! (-z)
	     (<=>
	       (in -z -x)
	       (in -z -y)))
	  (= -x -y))))

; 2. Foundation/regularity
#.(fof-axiom
    'regularity
    #.(st-forall-typed
	((set -X))
	(=>
	  (? (-a) (in -a -x))
	  (? (-y)
	     (&
	       (in -y -x)
	       (! 
		 (? (-z)
		    (in -z -y)
		    (in -z -x))))))))

; Predicates
#.(ax-failure-excluded 'set-property)
#.(ax-type-preconditions 
    'set-satisifies-property
    'set 'set-property)

; Operations on predicates
#.(ax-type-requirements
    'set-property-union)

; 3. Specification/separation
#.(fof-axiom
    'separation
    #.(st-forall-typed
	((set -x) (set-property -p))
	#.(st-forall-typed
	    ((set -z))
	    (<=>
	      (in -z (filter-set -x -p))
	      (&
		(in -z -x)
		(set-satisifies-property -z -p))))))
