(in-package :gen-fof-proof)

(defun fof-formula-string (x)
  (if 
    (stringp x) x
    (let*
      ((op (first x))
       (opk (intern op :keyword)))
      (case opk
	((:! :?) 
	 (format 
	   nil 
	   "~a [~{~a~#[~:;,~]~}]: (~a)"
	   op
	   (second x) (fof-formula-string (third x))))
	((:= :!=)
	 (format nil "~a~a~a"
		 (fof-formula-string (second x))
		 op
		 (fof-formula-string (third x))))
	(( :& :\|)
	 (format nil "~{~a~}"
		 (cdr
		   (loop for y in (cdr x)
			 collect opk
			 collect "("
			 collect (fof-formula-string y)
			 collect ")"))))
	((:<= :=> :<=> :<~> :~& :~\|)
	 (format nil "(~a)~a(~a)"
		 (fof-formula-string (second x))
		 op
		 (fof-formula-string (third x))))
	((:~) 
	 (format nil "~a(~a)" op (fof-formula-string (second x))))
	(t (format nil "~a(~{~a~#[~:;,~]~})" op 
		   (mapcar 'fof-formula-string (cdr x))))
	))))

(defun tptp-clause-type (x)
  (cond
    ((equal x "fof") "fof")
    ((equal x "input_clause") "cnf")
    ))

(defun cnf-formula-string (x)
  (format nil "(~{~a~#[~:;|~]~})"
	  (loop for c in x collect
		(if (equal (first c) "++")
		  (fof-formula-string (second c))
		  (concatenate 'string "~" 
			       (fof-formula-string (second c)))))))

(defun fof-top-level-string (x)
  (format 
    nil
    "~a(~a, ~a, ~a)."
    (tptp-clause-type (first x))
    (second x)
    (third x)
    (funcall 
      (symbol-function
	(intern (concatenate 'string 
			     (string-upcase (tptp-clause-type (first x))) 
			     "-FORMULA-STRING")
		:gen-fof-proof))
      (fourth x))))

(defun write-fof-file (fn entries)
  (with-open-file (f fn :direction :output :if-exists :supersede)
    (loop for x in entries do
	  (format f "~a~%" (fof-top-level-string x)))))

#+sbcl (defun temp-file-name (template)
	 (let (name-pair)
	   (unwind-protect
	     (setf name-pair (multiple-value-list (sb-posix:mkstemp template)))
	     (sb-posix:close (first name-pair))
	     )
	   (second name-pair)))

#+(or sbcl clisp) (defun temp-dir-name (template)
  (
   #+sbcl sb-posix:mkdtemp 
   #+clisp sb-posix:mkdtemp 
   template))

(eval-when (:compile-toplevel :load-toplevel :execute)
  #+sbcl (import 'sb-posix:getenv)
  #+clisp (import 'ext:getenv)
  )

(defun temp-fof-file (entries)
  (let*
    ((dn (temp-dir-name (concatenate 
			  'string 
			  (or (getenv "TMPDIR") (getenv "TMP") "/tmp")
			  "/gen-fof-proof-dir-XXXXXXXX")))
     (fn (concatenate 'string dn "/fof.tptp")))
    (write-fof-file fn entries)
    fn))

(defun file-contents (fn &key enc)
  (with-open-file 
    (f fn :external-format (or enc :utf-8))
    (let* ((out-data 
	     (make-array 
	       (file-length f) 
	       :element-type 'character)))
      (read-sequence out-data f)
      out-data)))

(defun file-lines (fn &key enc)
  (with-open-file 
    (f fn :external-format (or enc :utf-8))
    (iterate (for x := (read-line f nil nil)) 
	     (while x) (collect x))))

(defun read-sexp-file (fn &key symbols-to-strings)
  (let*
    (
     (unreadable (gensym))
     (res
       (apply 
	 'append
	 (with-open-file (f fn)
	   (loop for x := (read f nil unreadable) 
		 until (equal x unreadable) when (and x (listp x))
		 collect 
		 (if (listp (first x)) x (list x)))))
       )
     (res (if symbols-to-strings (symbols-to-strings res) res))
     )
    res
    ))

(defun capitalize (s)
  (if (> (length s) 0)
    (concatenate
      'string
      (string-upcase (string (elt s 0)))
      (subseq s 1))
    s))

(defun symbol-to-camelcase-string (x)
  (let*
    (
     (s (cl-ppcre:split "-" (string-downcase (symbol-name x))))
     (st (cdr s))
     (stcc (mapcar 'capitalize st))
     )
    (if 
      s
      (format nil "~a~{~a~}" (car s) stcc)
      "")))

(defun symbols-to-strings (tr)
  (loop for x in tr collect
	(cond
	  ((stringp x) x)
	  ((listp x) (symbols-to-strings x))
	  ((and
	     (symbolp x)
	     (not (cl-ppcre:scan "[a-zA-Z]" (symbol-name x))))
	   (symbol-name x))
	  ((symbolp x) (symbol-to-camelcase-string x))
	  (t (format nil "~a" x)))))

(defun convert-to-sexp (fn dn)
  (uiop:run-program `("tptp2X" "-f" "sex" "-d" ,dn ,fn)))

(defun tptp4X-refine (fn dn)
  (uiop:run-program `("tptp4X" "-d" ,dn ,fn)))

(defun read-fof-file (fn)
  (let*
    ((dn (temp-dir-name (concatenate 
			  'string 
			  (or (getenv "TMPDIR") (getenv "TMP") "/tmp")
			  "/gen-fof-proof-dir-XXXXXXXX"))))
    (convert-to-sexp fn dn)
    (let* 
      ((new-fn
	 (first 
	   (directory (concatenate 'string dn "/*.sex")))))
      (read-sexp-file new-fn)
      )))

(defun invoke-eprover (fin fou tlim &rest extra-args)
  (declare (ignorable extra-args))
  (uiop:run-program
    `("eprover"  
      "--proof-object=1"
      "--tstp-in" "--tstp-out"
      ,(format nil "--output-file=~a" fou)
      ,(format nil "--soft-cpu-limit=~a" tlim)
      ,fin))
  (uiop:run-program
    `("sed" "-e" "/^#/d" "-i" ,fou)))

(defun invoke-some-prover (entries invoker tlim &key 
				   extra-args raw-proof
				   show-dir one-bit
				   one-bit-marked
				   )
  (let*
    ((dn (temp-dir-name (concatenate 
			  'string 
			  (or (getenv "TMPDIR") (getenv "TMP") "/tmp")
			  "/gen-fof-proof-dir-XXXXXXXX")))
     (fin (concatenate 'string dn "/input.tptp"))
     (fou (concatenate 'string dn "/output.tptp"))
     (fou2 (concatenate 'string dn "/output.tptp.tptp"))
     (sexpfou (concatenate 'string dn "/output.tptp.sex"))
     )
    (write-fof-file fin entries)
    (apply invoker fin fou tlim extra-args)
    (when show-dir
      (format *error-output* 
	      "Temporary directory: ~s~%" dn))
    (cond
      ((and
	 one-bit
	 one-bit-marked
	 )
       (list
	 (second
	   (find "conjecture" entries
		 :test 'equal :key 'third)
	   )
	 (not (null (file-lines fou)))))
      (one-bit (not (null (file-lines fou))))
      (raw-proof
	(file-lines fou))
      (t (progn
	   (tptp4X-refine fou dn)
	   (convert-to-sexp fou2 dn)
	   (read-sexp-file sexpfou))))))

(defun replace-type (expr typ)
  `(,(first expr) ,(second expr) ,typ ,@(cdddr expr)))

(defun prove-by-steps (axioms steps invoker tlim &key
			      extra-args raw-proof show-dir
			      one-bit one-bit-marked
			      (offset 0))
  (let*
    (
     (axioms (typecase axioms
	       (pathname (read-sexp-file axioms
					 :symbols-to-strings t))
	       (t axioms)))
     (steps (typecase steps
	      (pathname (read-sexp-file steps
					:symbols-to-strings t))
	      (t steps)))
     )
    (iterate
      (for n upfrom 1)
      (for step in steps)
      (for lemmas initially nil then (cons (replace-type step "lemma") lemmas))
      (for step-conj := (replace-type step "conjecture"))
      (for step-task := (cond
			  ((equal (third step) "logic")
			   (list step-conj))
			  ((scan "recent/[0-9]+" (third step))
			   (append
			     (subseq 
			       lemmas 0
			       (parse-integer 
				 (regex-replace 
				   "recent/" (third step) "")))
			     (list step-conj)
			     ))
			  (t 
			    (append 
			      lemmas axioms 
			      (list step-conj)))))
      (format *error-output* 
	      "Proving: ~s~%"
	      (second step))
      (for 
	result :=
	(if
	  (> n offset)
	  (time (invoke-some-prover 
		  step-task invoker tlim 
		  :extra-args extra-args :raw-proof raw-proof
		  :show-dir show-dir :one-bit one-bit
		  :one-bit-marked one-bit-marked))
	  (list (second step) "assumed")
	  )
	)
      (format *error-output*
	      "Success: ~s~%" (not (null (if (and one-bit one-bit-marked) 
					   (second result) result))))
      (collect result)
      )))

(defun proof-to-hints (entries)
  (loop for x in entries
	when (equal (third x) "axiom") do (progn)
	when (equal (third x) "negated_conjecture") do (progn)
	when (equal (third x) "plain") 
	collect (append (list (first x) (second x) "assumption") (cdddr x))))
