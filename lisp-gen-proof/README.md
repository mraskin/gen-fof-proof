This is a collection of Common Lisp support code to work with first-order proofs
and provers.

Use can use it under the terms of GPLv3 (if you have an established open-source
project under a different license, contact us)

Authors:
Michael Raskin (2011-)
Christoph Welzel (2018-)

Some of these tools were used in research projects in:
LaBRI, University of Bordeaux
CS Department, Technical University of Munich
