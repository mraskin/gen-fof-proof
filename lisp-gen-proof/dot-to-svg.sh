#! /bin/sh

if test -n "$1"; then
        name="$1"
        shift
        dot -Tsvg "$name" "$@" > "$name.svg"
else
        shift
        dot -Tsvg "$@"
fi
