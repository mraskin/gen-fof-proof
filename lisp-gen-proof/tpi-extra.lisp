(in-package :gen-fof-proof)

(def-tpi-handler
  "load_lisp" (data context)
  (load (expand-getenv (name data) context)))

(def-tpi-handler
  "stash_group" (data context)
  (let*
    (
     (assignment (when (typep data 'tptp-predicate-infix)
                   (args data)))
     (stashed-group (name (or (second assignment) data)))
     (stashed-target (name (or (first assignment) data)))
     (stashed-formulae 
       (loop 
	 for x in (gethash :formulae context)
	 when (find stashed-group (first x) :test 'equal)
	 collect x
	 ))
     )
    (setf
      (gethash :stashed-groups context)
      (or
	(gethash :stashed-groups context)
	(make-hash-table :test 'equal)
	)
      )
    (setf (gethash stashed-target (gethash :stashed-groups context))
	  stashed-formulae)
    ))

(def-tpi-handler
  "unstash_group" (data context)
  (let*
    (
     (stash-name (name data))
     (stashed-formulae (gethash stash-name (gethash :stashed-groups context)))
     )
    (loop for x in (reverse stashed-formulae) do
	  (pushnew x (gethash :formulae context)))
    ))

(defun replace-formula-role (f role)
  (make-instance (class-of f)
                 :kind (kind f)
                 :role role
                 :name (name f)
                 :content (content f)))

(defun wrap-formula (f)
  `(tptp-syntax::tptp_input
     (tptp-syntax::annotated_formula
       ,f)))

(defun prover-output-success-p (res)
  (or
    (and
      (cl-ppcre:scan " SZS status [A-Za-z]*Theorem" res)
      (not
        (cl-ppcre:scan " SZS status [A-Za-z]*CounterTheorem" res)))
    (and
      (cl-ppcre:scan " SZS status [A-Za-z]*Conclusion" res)
      (not (cl-ppcre:scan " SZS status [A-Za-z]*CounterConclusion" res)))
    (cl-ppcre:scan " SZS status Tautology" res)
    (cl-ppcre:scan " SZS status Equivalent" res)
    (cl-ppcre:scan "SPASS beiseite: Proof found" res)
    ))

(defun contexts-for-lemma-checking (context)
  (loop with formulae := (gethash :formulae context)
        with lemma-setenvs := (gethash :lemma-check-group-env context)
        with true-env := (gethash :env context)
        for seen-entries := nil then (cons converted-entry seen-entries)
        for current-entry in (reverse formulae)
        for current-statement := (third current-entry)
        for current-kind := (kind current-statement)
        for current-role := (role current-statement)
        for current-name := (name current-statement)
        for current-formula := (content current-statement)
        for current-groups := (first current-entry)
        for fresh-context := (alexandria:copy-hash-table context)
        for fresh-env := (alexandria:copy-hash-table true-env)
        for checked-lemma-p := (cl-ppcre:scan
                                 "^checked_lemma(_|$)" current-role)
        for converted-statement := (if checked-lemma-p
                                     (replace-formula-role
                                       current-statement "lemma")
                                     current-statement)
        for converted-entry := (list current-groups
                                     (format nil "~a" converted-statement)
                                     converted-statement)
        for conjecture-statement := (if checked-lemma-p
                                      (replace-formula-role
                                        current-statement "conjecture")
                                      current-statement)
        for conjecture-entry := (list current-groups
                                      (format nil "~a" conjecture-statement)
                                      conjecture-statement)
        for full-dependencies-p := (equal "checked_lemma" current-role)
        for dependency-group := (unless full-dependencies-p
                                  (cl-ppcre:regex-replace
                                    "^checked_lemma_" current-role ""))
        when checked-lemma-p
        collect
        (progn
          (setf (gethash :env fresh-context) fresh-env)
          (loop for group in (cons "$" (sort current-groups 'string<))
                for setenvs := (gethash group lemma-setenvs)
                do (loop for setenv in setenvs
                         for var := (first setenv)
                         for value := (second setenv)
                         for expanded-value :=
                         (expand-getenv value fresh-context)
                         do (setenv_value var expanded-value fresh-context)))
          (unless full-dependencies-p
            (setf (gethash :inactive-formulae fresh-context)
                  (make-hash-table :test 'equal)))
          (setf (gethash :formulae fresh-context)
                (cons
                  conjecture-entry
                  (if full-dependencies-p
                    seen-entries
                    (remove-if
                      (lambda (entry)
                        (not (find dependency-group
                                   (first entry) :test 'equal)))
                      seen-entries))))
          (setf (gethash :lemma-to-check fresh-context) conjecture-entry)
          fresh-context)))

(def-tpi-handler
  "set_lemma_check_group_env" (data context)
  (assert (typep data 'tptp-propositional-infix))
  (assert (equal "=>" (name data)))
  (assert (typep (first (args data)) 'tptp-propositional-constant))
  (assert (typep (second (args data)) 'tptp-predicate-infix))
  (assert (equal "=" (name (second (args data)))))
  (setf (gethash :lemma-check-group-env context)
        (or (gethash :lemma-check-group-env context)
            (make-hash-table :test 'equal)))
  (push 
    (mapcar 'name (args (second (args data))))
    (gethash (name (first (args data)))
             (gethash :lemma-check-group-env context))))

(def-tpi-handler
  "check_lemmas_strict" (data context)
  (let* ((*execution-result-handler*
           (lambda (s)
             (unless (prover-output-success-p s)
               (format *trace-output* "Prover result:~%~a~%" s)
               (error "Prover did not report success")))))
    (run-tpi-handler "check_lemmas" data context)))

(defun spawn-worker-threads (n tasks lock &key basename)
  (let* ((tasks tasks))
    (loop for k from 1 to n
          collect
          (let* ((k k))
            (bordeaux-threads:make-thread
              (lambda ()
                (loop for next-task :=
                      (bordeaux-threads:with-lock-held (lock) (pop tasks))
                      while next-task
                      do (funcall next-task)))
              :name (format nil "~a-~8,'0d"
                            (or basename "Worker thread") k))))))

(def-tpi-handler
  "check_lemmas" (data context)
  (let* ((threads 
           (or
             (ignore-errors
               (parse-integer 
                 (expand-getenv
                   (getenv_value "=TPI_CHECK_LEMMA_THREADS" context)
                   context)))
             1))
         (proof-log (getenv_value "=TPI_PROOF_LOG" context))
         (proof-log (and (> (length proof-log) 0) proof-log)))
    (format *trace-output*
            "Checking lemmas in parallel using ~a threads and command: ~a~%"
            threads data)
    (when proof-log
      (ensure-directories-exist proof-log)
      (when (probe-file proof-log)
        (delete-file proof-log)))
    (format *trace-output* "Preparing contexts given ~a formulae~%"
            (length (gethash :formulae context)))
    (let*((contexts (contexts-for-lemma-checking context))
          (total (length contexts))
          (results (make-array total :initial-element :waiting))
          (cutoff (or (ignore-errors
                        (parse-integer
                          (getenv_value "=TPI_TRACE_CUTOFF" context)))
                      1000))
          (log-file (getenv_value "=TPI_EXECUTE_LOG" context))
          (execution-result-handler *execution-result-handler*)
          (tasks nil)
          (lock (bordeaux-threads:make-lock "TPI lemma checking lock")))
      (format *trace-output* "Prepared ~a contexts~%" total)
      (setf tasks
            (loop for current-context in contexts
                  for k upfrom 0
                  for checked-lemma :=
                  (gethash :lemma-to-check current-context)
                  for lemma-statement := (third checked-lemma)
                  for lemma-name := (name lemma-statement)
                  for lemma-text := (second checked-lemma)
                  for lemma-text-truncated :=
                  (subseq lemma-text 0 (min cutoff (length lemma-text)))
                  for trace-context :=
                  (format nil "Was proving lemma:~%~a~%~a~%"
                          lemma-name lemma-text-truncated)
                  for task := 
                  (let ((k k)
                        (current-context current-context)
                        (trace-context trace-context))
                    (lambda ()
                      (setf
                        (elt results k)
                        (multiple-value-list
                          (ignore-errors
                            (let* ((command
                                     (prepare-execution data current-context))
                                   (full-context
                                     (format nil "~a~%Used command:~%~a~%"
                                             trace-context command)))
                              (run-traced-execution
                                command
                                nil current-context
                                :log-file log-file
                                :quiet t
                                :trace-context full-context
                                :execution-result-handler
                                execution-result-handler)))))))
                  collect task))
      (format *trace-output* "Prepared ~a tasks~%" total)
      (spawn-worker-threads threads tasks lock
                            :basename "TPI lemma proving task")
      (loop for k from 0 to (1- total)
            for current-context in contexts
            for values := 
            (loop for first := t then nil
                  while (eq :waiting (elt results k))
                  when first do 
                  (format *trace-output*
                          "Reached step ~a of ~a:~%~a~%"
                          k total
                          (name (third (gethash
                                         :lemma-to-check
                                         current-context))))
                  do (sleep 1)
                  finally
                  (let* ((value (elt results k)))
                    (setf (elt results k) nil)
                    (return value)))
            for value := (first values)
            for error := (second values)
            when error do (error error)
            when proof-log do
            (alexandria:write-string-into-file
              value (format nil "~a-~8,'0d.proof" proof-log k)
              :if-exists :supersede
              :if-does-not-exist :create)))))

(def-tpi-handler
  "check_definitions" (data context)
  (declare (ignorable data))
  (let*
    (
     (formulae (gethash :formulae context))
     (stack nil)
     (seen-names (make-hash-table :test 'equal))
     )
    (loop 
      for x in (reverse formulae)
      for statement := (third x)
      for formula-language := (kind statement)
      for role := (role statement)
      for content := (content statement)
      when (cl-ppcre:scan "^checked_definition$" role)
      do (format t "Checking definition: ~s~%"
		 (name statement))
      do
      (cond
	((equal role "checked_definition")
	 (let*
	   ((top-formula content)
            (top-comparison
              (etypecase top-formula
                (tptp-fof-quantified
                 (assert (equal (name top-formula) "!"))
                 (assert (or (typep (content top-formula)
                                    'tptp-propositional-infix)
                             (typep (content top-formula)
                                    'tptp-predicate-infix)))
                 (content top-formula))
                (tptp-propositional-infix top-formula)
                (tptp-predicate-infix top-formula)))
            (lhs (first (args top-comparison)))
            (rhs (second (args top-comparison)))
            (defined-name (name lhs))
            (right-names (fof-constant-names rhs))
            (new-statement (replace-formula-role statement "definition")))
           (assert (or (equal "=" (name top-comparison))
                       (equal "<=>" (name top-comparison))))
	   (assert (not (find defined-name right-names :test 'equal)))
	   (assert (not (gethash defined-name seen-names)))
	   (push 
	     (list
               (first x)
               (format nil "~a" new-statement)
               new-statement)
	     stack)))
	(t 
           (push x stack))
	)
      do
      (loop for n in (fof-constant-names
                       (content statement)) do
	    (setf (gethash n seen-names) t))
      )
    (setf (gethash :formulae context) stack)))

(def-tpi-handler
  "forbid_free_vars" (data context)
  (declare (ignorable data))
  (loop for x in (gethash :formulae context) 
	for f := (third x)
	for formula-language := (kind f)
	for role := (role f)
	do (format t "Checking there no free vars:~%~s~%" (content f))
	when (equal formula-language "fof")
        do (assert (null (tptp-object-free-variables (content f))))))

(def-tpi-handler
  "trust_me" (data context &key silent)
  (declare (ignorable data))
  (let*
    (
     (formulae (gethash :formulae context))
     (stack nil)
     )
    (loop 
      for x in (reverse formulae)
      for f := (third x)
      for role := (and (typep f 'tptp-statement) (role f))
      for new-f :=
      (when (or (equal role "checked_lemma")
                (scan "^checked_lemma_" role))
        (make-instance (class-of f)
                       :kind (kind f)
                       :role "lemma"
                       :name (name f)
                       :content (content f)))
      for new-entry :=
      (when new-f
        (list
          (first x)
          (format nil "~a" new-f)
          new-f))
      unless silent
      when new-entry
      do (format *trace-output* "Trusting lemma: ~s~%" (name f))
      do (push (or new-entry x) stack))
    (setf (gethash :formulae context) stack)
    )
  )

(def-tpi-handler
  "eval" (data context)
  (run-tpi
    (expand-getenv (name data) context)
    :initial-context context
    :exit-anyway t
    ))

(def-tpi-handler
  "assign_group" (data context)
  (let*
    (
     (assignment (args data))
     (name (name (first assignment)))
     (group (name (second assignment)))
     )
    (setf
      (gethash :formulae context)
      (loop 
	for f in (gethash :formulae context)
	collect
	(if
	  (equal name (name (third f)))
	  (list
	    (cons group (first f))
	    (second f)
            (third f))
	  f)))))

(def-tpi-handler
  "unassign_group" (data context)
  (let*
    (
     (assignment (args data))
     (name (name (first assignment)))
     (group (name (second assignment)))
     )
    (setf
      (gethash :formulae context)
      (loop 
	for f in (gethash :formulae context)
	collect
	(if
	  (equal name (name (third f)))
	  (list
	    (remove group (first f) :test 'equal)
	    (second f)
            (third f))
	  f)))))

(def-tpi-handler
  "setenv_formula" (data context)
  (let*
    (
     (assignment (args data))
     (var (name (first assignment)))
     (name (name (second assignment)))
     (formula
       (find-if
	 (lambda (f)
	   (equal
	     name
	     (name (third f))))
	 (gethash :formulae context)))
     (formula-content (content formula)))
    (setenv_value var (format nil "~a" formula-content) context)))

(def-tpi-handler
  "generate" (data context)
  (run-tpi
    (execution-handler
      (expand-getenv (name data) context))
    :initial-context context
    :exit-anyway t))

(def-exec-handler
  "cat" (cmd)
  cmd)

(def-tpi-handler
  "set_deps" (data context)
  (let*
    (
     (name (name (first (args data))))
     (deps (second (args data)))
     (target (find-if
               (lambda (x) (equal name (name (third x))))
               (gethash :formulae context)))
     (group-name 
       (format 
         nil "ds_~a_dependency_set_~a"
         name (sha512 (third target))))
     (names nil)
     (groups nil)
     )
    (labels
      ((process-groups 
         (x)
         (loop 
           for y in (args x)
           do 
           (push (name y) groups)))
       (process-list 
         (x)
         (loop 
           for y in (args x)
           do 
           (etypecase y
             (tptp-object-constant
               (push (name y) names))
             (tptp-function-call
               (cond ((equal (name y) "group")
                      (process-groups y))
                     (t (error "Unknown dependency list entry: ~s" y))))))))
      (process-list deps))
    (setf
      (gethash :formulae context)
      (loop 
        for f in (gethash :formulae context)
        for fname := (name (third f)) 
        collect
        (cond
          ((equal name fname)
           (let* ((new-f
                    (replace-formula-role
                      (third f)
                      (format
                        nil "checked_lemma_~a" group-name)))
                  (new-ft (format nil "~a" new-f)))
             (list (first f) new-ft new-f)))
          ((or
             (intersection (first f) groups :test 'equal)
             (find  fname names :test 'equal)
             )
           (list 
             (cons group-name (first f))
             (second f)
             (third f)))
          (t f))))))

(def-tpi-handler
  "deactivate" (data context)
  (let* ((target-name (name data)))
    (setf (gethash target-name (gethash :inactive-formulae context)) t)))
(def-tpi-handler
  "activate" (data context)
  (let* ((target-name (name data)))
    (setf (gethash target-name (gethash :inactive-formulae context)) nil)))
(def-tpi-handler
  "deactivate_group" (data context)
  (let* ((target-name (name data))
         (inactive-ht (gethash :inactive-formulae context)))
    (loop for f in (gethash :formulae context)
          for name := (name (third f))
          when (find target-name (first f) :test 'equal)
          do (setf (gethash name inactive-ht) t))))
(def-tpi-handler
  "activate_group" (data context)
  (let* ((target-name (name data))
         (inactive-ht (gethash :inactive-formulae context)))
    (loop for f in (gethash :formulae context)
          for name := (name (third f))
          when (find target-name (first f) :test 'equal)
          do (setf (gethash name inactive-ht) nil))))
