#! /bin/sh

file="$1"
shift

if test -n "$file"; then
./nix-load-lisp.sh --eval "(progn $*)" --eval '(alexandria:write-string-into-file (gen-fof-proof::summarize-split-proof (alexandria:read-file-into-string "'"$file"'")) "'"$file"'.summary.proof" :if-exists :supersede)' --eval '(quit)'
else
        ./nix-load-lisp.sh --eval "(progn $*)" --eval '(format t "~a~%" (gen-fof-proof::summarize-split-proof "'"$(cat)"'"))' --eval '(quit)' | grep -A 999999999 '^fof(' --line-buffered
fi
