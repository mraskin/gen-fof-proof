#! /bin/sh

export z3_tptp="$(which z3-tptp 2>/dev/null || echo "$(nix-build --no-out-link '<nixpkgs>' -A z3-tptp)/bin/z3-tptp")"

time="${1:-15}"
args="$2"
command="$3"

export PS1="<TPI> $PS1"

shift; shift; shift

TPI_PROVER="$z3_tptp -t:$time $args" TPI_PROVER_FILE_PREFIX='-file:' TPI_CAT_SKIP_EQUIASSUME="1" "${command:-$SHELL}" "$@"
