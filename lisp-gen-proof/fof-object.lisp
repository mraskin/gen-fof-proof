(in-package :gen-fof-proof)

(defclass tptp-formula () ())
(defclass tptp-term () ())
(defclass tptp-leaf () ())
(defclass tptp-internal-node () ())
(defclass tptp-content-node (tptp-internal-node) ())

(defclass tptp-object-constant (tptp-term tptp-leaf)
  ((name :accessor name :initarg :name :initform (error "Constant must have a name"))))
(defclass tptp-propositional-constant (tptp-formula tptp-leaf)
  ((name :accessor name :initarg :name :initform (error "Constant must have a name"))))

(defclass tptp-object-variable (tptp-term tptp-leaf)
  ((name :accessor name :initarg :name :initform (error "Variable must have a name"))))

(defclass tptp-object-distinct (tptp-term tptp-leaf)
  ((name :accessor name :initarg :name :initform (error "Constant must have a name"))))
(defclass tptp-object-number (tptp-term tptp-leaf)
  ((name :accessor name :initarg :name :initform (error "Constant must have a name"))))

(defclass tptp-function-call (tptp-term tptp-internal-node)
  ((name :accessor name :initarg :name :initform (error "Call must have a function name"))
   (args :accessor args :initarg :args :initform (error "Call must have arguments"))))
(defclass tptp-predicate-call (tptp-formula tptp-internal-node)
  ((name :accessor name :initarg :name :initform (error "Call must have a function name"))
   (args :accessor args :initarg :args :initform (error "Call must have arguments"))))

(defclass tptp-propositional-infix (tptp-formula tptp-internal-node)
  ((name :accessor name :initarg :name :initform
         (error "Infix operation must have a name"))
   (args :accessor args :initarg :args :initform
         (error "Infix operation must have arguments"))))
(defclass tptp-predicate-infix (tptp-formula tptp-internal-node)
  ((name :accessor name :initarg :name :initform
         (error "Infix operation must have a name"))
   (args :accessor args :initarg :args :initform
         (error "Infix operation must have arguments"))))

(defclass tptp-propositional-prefix (tptp-formula tptp-internal-node)
  ((name :accessor name :initarg :name :initform
         (error "Prefix operation must have a name"))
   (args :accessor args :initarg :args :initform
         (error "Prefix operation must have arguments"))))

(defclass tptp-fof-quantified (tptp-formula tptp-content-node)
  ((name :accessor name :initarg :name :initform
         (error "Quantifier must have a name"))
   (args :accessor args :initarg :args :initform
         (error "Quantifier must have arguments"))
   (content :accessor content :initarg :content :initform
            (error "Quantifier must have content"))))

(defun tptp-constant-name (name)
  (cond
    ((integerp name) (format nil "~a" name))
    ((cl-ppcre:scan "^[$]?[a-z][a-zA-Z0-9_]*$" name) name)
    (t (format nil "'~a'"
               (cl-ppcre:regex-replace-all
                 "[\\\\']" name "\\\\\\&")))))

(defmethod print-object ((o tptp-object-constant) s)
  (princ (tptp-constant-name (name o)) s))

(defmethod print-object ((o tptp-propositional-constant) s)
  (princ (tptp-constant-name (name o)) s))

(defmethod print-object ((o tptp-object-variable) s)
  (princ (name o) s))

(defmethod print-object ((o tptp-object-distinct) s)
  (format s "\"~a\"" (name o)))

(defmethod print-object ((o tptp-object-number) s)
  (format s "~a" (name o)))

(defmethod print-object ((o tptp-function-call) s)
  (format s "~a(~{~a~#[~:;,~]~})"
          (tptp-constant-name (name o))
          (args o)))

(defmethod print-object ((o tptp-predicate-call) s)
  (format s "~a(~{~a~#[~:;,~]~})"
          (tptp-constant-name (name o))
          (args o)))

(defmethod print-object ((o tptp-propositional-infix) s)
  (format s "(~{~a~})"
          (loop for first := t then nil
                for a in (args o)
                unless first collect (name o)
                collect a)))
(defmethod print-object ((o tptp-predicate-infix) s)
  (format s "~{~a~}"
          (loop for first := t then nil
                for a in (args o)
                unless first collect (name o)
                collect a)))

(defmethod print-object ((o tptp-propositional-prefix) s)
  (format s "~a~a" (name o) (first (args o))))

(defmethod print-object ((o tptp-fof-quantified) s)
  (format s "~a[~{~a~#[~:;,~]~}]:~a"
          (name o) (args o) (content o)))

(defun tptp-parse-tree-to-object (f &key object)
  (let ((op (first f))
        (args (second (second f))))
    (ecase op
      ((tptp-syntax::Sp) nil)
      ((tptp-syntax::fof_formula
         tptp-syntax::tpi_formula
         tptp-syntax::fof_logic_formula
         tptp-syntax::fof_binary_formula
         tptp-syntax::fof_binary_assoc
         tptp-syntax::fof_atomic_formula
         tptp-syntax::fof_plain_atomic_formula
         tptp-syntax::fof_defined_plain_formula
         tptp-syntax::fof_defined_atomic_formula
         tptp-syntax::fof_system_atomic_formula
         tptp-syntax::fof_unit_formula)
       (tptp-parse-tree-to-object args))
      ((tptp-syntax::fof_function_term)
       (tptp-parse-tree-to-object args :object t))
      ((tptp-syntax::fof_and_formula
         tptp-syntax::fof_or_formula)
       (make-instance 'tptp-propositional-infix
                      :name (fof-assoc-binop f)
                      :args (mapcar 'tptp-parse-tree-to-object
                                    (fof-assoc-components f))))
      ((tptp-syntax::fof_binary_nonassoc)
       (make-instance 'tptp-propositional-infix
                      :name (tptp-true-text (tptp-no-space (nth 2 args)))
                      :args (list
                              (tptp-parse-tree-to-object (nth 0 args))
                              (tptp-parse-tree-to-object (nth 4 args)))))
      ((tptp-syntax::fof_unary_formula)
       (if (and
             (listp (first args))
             (eq (first (first args)) 'tptp-syntax::unary_connective))
         (make-instance 'tptp-propositional-prefix
                        :name (tptp-true-text (tptp-no-space (nth 0 args)))
                        :args (list (tptp-parse-tree-to-object (nth 2 args))))
         (tptp-parse-tree-to-object args)))
      ((tptp-syntax::fof_infix_unary
         tptp-syntax::fof_defined_infix_formula)
       (make-instance 'tptp-predicate-infix
                      :name (tptp-true-text (tptp-no-space (nth 2 args)))
                      :args (list
                              (tptp-parse-tree-to-object (nth 0 args) :object t)
                              (tptp-parse-tree-to-object (nth 4 args) :object t))))
      ((tptp-syntax::fof_quantified_formula)
       (let* ((quantifier (tptp-true-text
                            (tptp-no-space (nth 0 args))))
              (varlist (nth 4 args))
              (content (nth 10 args))
              (variables
                (loop for v in
                      (all-subtrees varlist 'tptp-syntax::variable)
                      collect (tptp-true-text (tptp-no-space v)))))
         (make-instance 'tptp-fof-quantified
                        :name quantifier
                        :args (loop for v in variables
                                    collect
                                    (make-instance
                                      'tptp-object-variable :name v))
                        :content 
                        (tptp-parse-tree-to-object content))))
      ((tptp-syntax::proposition
         tptp-syntax::defined_proposition
         tptp-syntax::system_proposition)
       (make-instance 'tptp-propositional-constant
                      :name (tptp-true-text (tptp-no-space f))))
      ((tptp-syntax::fof_plain_term
         tptp-syntax::fof_defined_plain_term)
       (cond
         ((find (first args) '(tptp-syntax::constant tptp-syntax::defined_constant))
          (tptp-parse-tree-to-object args :object object))
         ((equal (first (first args)) 'tptp-syntax::functor)
          (let* ((name (tptp-true-text (tptp-no-space (nth 0 args))))
                 (args (tptp-parse-tree-to-object
                         (nth 4 args) :object object)))
            (make-instance (if object 'tptp-function-call 'tptp-predicate-call)
                           :name name :args args)))
         (t (error "Unknown situation"))))
      ((tptp-syntax::fof_arguments)
       (if (eq (first args) 'tptp-syntax::fof_term)
         (list (tptp-parse-tree-to-object args :object t))
         (cons (tptp-parse-tree-to-object
                 (nth 0 args) :object t)
               (tptp-parse-tree-to-object
                 (nth 4 args)))))
      ((tptp-syntax::fof_term)
       (tptp-parse-tree-to-object args :object t))
      ((tptp-syntax::variable)
       (make-instance 'tptp-object-variable :name
                      (tptp-true-text (tptp-no-space f))))
      ((tptp-syntax::fof_unitary_formula)
       (if (symbolp (first args))
         (tptp-parse-tree-to-object args)
         (tptp-parse-tree-to-object (nth 2 args))))
      ((tptp-syntax::constant 
         tptp-syntax::defined_constant)
       (make-instance
         (if object 'tptp-object-constant 'tptp-propositional-constant)
         :name (tptp-true-text (tptp-no-space f))))
      ((tptp-syntax::cnf_formula)
       (if (eq (first args) 'tptp-syntax::disjunction)
         (tptp-parse-tree-to-object args)
         (tptp-parse-tree-to-object (nth 2 args))))
      ((tptp-syntax::disjunction)
       (if (eq (first args) 'tptp-syntax::literal)
         (tptp-parse-tree-to-object args)
         (make-instance 'tptp-propositional-infix
                        :name "|"
                        :args
                        (mapcar 'tptp-parse-tree-to-object
                                (fof-assoc-components f)))))
      ((tptp-syntax::literal)
       (cond ((symbolp (first args))
              (tptp-parse-tree-to-object args))
             ((equal (first args) "~")
              (make-instance 'tptp-propositional-prefix
                             :name "~"
                             :args (list (tptp-parse-tree-to-object
                                           (nth 2 args)))))
             (t (error "Tree format: ~s" f))))
      )))

(defmethod simplify-tptp-object (f) f)

(defmethod simplify-tptp-object ((f tptp-internal-node))
  (make-instance (class-of f)
                 :name (name f)
                 :args (mapcar 'simplify-tptp-object (args f))))
(defmethod simplify-tptp-object ((f tptp-content-node))
  (make-instance (class-of f)
                 :name (name f)
                 :args (mapcar 'simplify-tptp-object (args f))
                 :content (simplify-tptp-object (content f))))
(defmethod simplify-tptp-object ((f tptp-propositional-infix))
  (let* ((op (name f)))
    (if (find op '("&" "|") :test 'equal)
      (loop with res := '()
            with stack := (args f)
            while stack
            for current := (pop stack)
            do (if (and (typep current 'tptp-propositional-infix)
                        (equal op (name current)))
                 (setf stack (append (args current) stack))
                 (push current res))
            finally (return (make-instance 
                              'tptp-propositional-infix
                              :name op :args
                              (sort (mapcar 'simplify-tptp-object res)
                                    'string< :key
                                    (lambda (x) (format nil "~a" x))))))
      (call-next-method))))
(defmethod simplify-tptp-object ((f tptp-predicate-infix))
  (let* ((op (name f)))
    (if (find op '("!=" "=") :test 'equal)
      (loop with res := '()
            with stack := (args f)
            while stack
            for current := (pop stack)
            do (if (and (typep current 'tptp-propositional-infix)
                        (equal op (name current)))
                 (setf stack (append (args current) stack))
                 (push current res))
            finally (return (make-instance 
                              'tptp-propositional-infix
                              :name op :args
                              (sort (mapcar 'simplify-tptp-object res)
                                    'string< :key
                                    (lambda (x) (format nil "~a" x))))))
      (call-next-method))))
(defmethod simplify-tptp-object ((f tptp-propositional-prefix))
  (let* ((op (name f))
         (arg (first (args f))))
    (if (equal op "~")
      (typecase arg
        (tptp-propositional-prefix
          (if (equal (name arg) "~")
            (simplify-tptp-object (first (args arg)))
            (call-next-method)))
        (tptp-predicate-infix
          (cond
            ((equal (name arg) "=")
             (simplify-tptp-object
               (make-instance 'tptp-predicate-infix
                              :name "!="
                              :args (args arg))))
            ((equal (name arg) "!=")
             (simplify-tptp-object
               (make-instance 'tptp-predicate-infix
                              :name "="
                              :args (args arg))))
            (t (call-next-method))))
        (t (call-next-method)))
      (call-next-method))))

(defmethod tptp-object-free-variables-internal ((f tptp-leaf) bound (free hash-table)) nil)
(defmethod tptp-object-free-variables-internal ((f tptp-internal-node) bound (free hash-table))
  (loop for a in (args f)
        do (tptp-object-free-variables-internal a bound free))
  (let* ((res nil))
    (maphash (lambda (k v) v (push k res)) free)
    (sort res 'string<)))
(defmethod tptp-object-free-variables-internal ((f tptp-content-node) bound (free hash-table))
  (loop for a in (args f)
        do (tptp-object-free-variables-internal a bound free))
  (tptp-object-free-variables-internal (content f) bound free)
  (let* ((res nil))
    (maphash (lambda (k v) v (push k res)) free)
    (sort res 'string<)))
(defmethod tptp-object-free-variables-internal ((f tptp-object-variable) bound (free hash-table))
  (if (find (name f) bound :test 'equal)
    nil
    (progn (setf (gethash (name f) free) t)
           (list (name f)))))
(defmethod tptp-object-free-variables-internal ((f tptp-fof-quantified) bound (free hash-table))
  (tptp-object-free-variables-internal (content f)
                              (append (mapcar 'name (args f)) bound)
                              free))
(defmethod tptp-object-free-variables-internal (f bound (free null))
  (tptp-object-free-variables-internal f bound (make-hash-table :test 'equal)))
(defmethod tptp-object-free-variables (f)
  (tptp-object-free-variables-internal f nil nil))

(defmethod tptp-object-binding-count ((f tptp-leaf)) 0)
(defmethod tptp-object-binding-count ((f tptp-internal-node))
  (reduce '+ (mapcar 'tptp-object-binding-count (args f))))
(defmethod tptp-object-binding-count ((f tptp-content-node))
  (reduce '+ (mapcar 'tptp-object-binding-count (cons (content f) (args f)))))
(defmethod tptp-object-binding-count ((f tptp-fof-quantified))
  (+ (length (args f)) (tptp-object-binding-count (content f))))

(defmethod rename-tptp-object-variables-internal ((f tptp-leaf) bound (free hash-table) (number-length integer) (used integer)) (values f used))
(defmethod rename-tptp-object-variables-internal ((f tptp-internal-node) bound (free hash-table) (number-length integer) (used integer))
  (let ((new-args nil)
        (new-used used))
    (loop for a in (args f)
          for pa := (multiple-value-list (rename-tptp-object-variables-internal a bound free number-length new-used))
          do (push (first pa) new-args)
          do (setf new-used (second pa)))
    (values
      (make-instance (class-of f)
                     :name (name f)
                     :args (reverse new-args))
      new-used)))
(defmethod rename-tptp-object-variables-internal ((f tptp-content-node) bound (free hash-table) (number-length integer) (used integer))
  (let ((new-args nil)
        (new-used used))
    (loop for a in (append (args f) (list (content f)))
          for pa := (multiple-value-list (rename-tptp-object-variables-internal a bound free number-length new-used))
          do (push (first pa) new-args)
          do (setf new-used (second pa)))
    (values
      (make-instance (class-of f)
                     :name (name f)
                     :args (reverse (cdr new-args))
                     :content (first new-args))
      new-used)))
(defmethod rename-tptp-object-variables-internal ((f tptp-object-variable) bound (free hash-table) (number-length integer) (used integer))
  (let* ((name (name f))
         (assigned-name (or (gethash name free)
                            (cdr (assoc name bound :test 'equal))))
         (new-name
           (unless assigned-name
             (let ((format-string (format nil "F~~~d,'0d" number-length)))
               (format nil format-string (1+ used))))))
    (if new-name
      (values (make-instance 'tptp-object-variable :name new-name) (1+ used))
      (values (make-instance 'tptp-object-variable :name assigned-name) used))))
(defmethod rename-tptp-object-variables-internal ((f tptp-fof-quantified) bound (free hash-table) (number-length integer) (used integer))
  (let* ((format-string (format nil "B~~~d,'0d" number-length))
         (bound-names (loop for a in (args f)
                            for k upfrom (1+ used)
                            for new-name := (format nil format-string k)
                            collect (cons (name a) new-name))))
    (multiple-value-bind
      (new-content new-used)
      (rename-tptp-object-variables-internal
        (content f)
        (append bound-names bound)
        free
        number-length
        (+ used (length (args f))))
    (values
      (make-instance 'tptp-fof-quantified
                     :name (name f)
                     :args (loop for n in bound-names collect
                                 (make-instance
                                   'tptp-object-variable :name (cdr n)))
                     :content new-content)
      new-used))))

(defmethod rename-tptp-object-variables-internal (f bound (free null) number-length used)
  (rename-tptp-object-variables-internal f bound (make-hash-table :test 'equal) number-length used))
(defmethod rename-tptp-object-variables-internal (f bound free (number-length null) used)
  (rename-tptp-object-variables-internal f bound free
                                (length (format nil "~a"
                                                (+ (tptp-object-binding-count f)
                                                   (length (tptp-object-free-variables f)))))
                                used))
(defmethod rename-tptp-object-variables-internal (f bound free number-length (used null))
  (rename-tptp-object-variables-internal f bound free number-length 0))

(defmethod rename-tptp-object-variables (f)
  (rename-tptp-object-variables-internal f nil nil nil nil))


(defun parse-fof-to-object (f)
  (tptp-parse-tree-to-object
    (esrap:parse 'tptp-syntax::fof_formula f)))

(defun normalise-fof-formula (f)
  (format
    nil "~a"
    (rename-tptp-object-variables
      (simplify-tptp-object
        (parse-fof-to-object f)))))
