
(asdf:defsystem :gen-fof-proof
  :name "gen-fof-proof"
  :depends-on 
  (
    :alexandria
    :iterate
    :esrap-peg
    :cl-ppcre
    :parse-number
    :cl-json
    :ironclad
    :bordeaux-threads
    :pcall
    :lparallel
    #+sbcl :sb-posix
   )
  :components
  (
   (:file "package")
   (:static-file "tptp-staged-bnf.peg")
   (:file "hash" :depends-on ("package"))
   (:file "tptp" :depends-on ("package" "tptp-staged-bnf.peg"))
   (:file "tptp-ast" :depends-on ("package" "tptp"))
   (:file "tptp-ast-normal" :depends-on ("package" "tptp" "tptp-ast"))
   (:file "fof-object" :depends-on ("package" "tptp" "tptp-ast"))
   (:file "fof-splits" :depends-on ("package" "fof-object"))
   (:file "statement-object" :depends-on ("package" "tptp" "tptp-ast" "fof-object"))
   (:file "fof-renames" :depends-on ("package" "fof-object" "statement-object"))
   (:file "tpi-filter" :depends-on ("package" "statement-object" "fof-object" "tptp"))
   (:file "tpi" :depends-on ("package" "tptp-ast" "tptp"))
   (:file "tpi-extra" :depends-on ("package" "tptp-ast" "tptp" "tpi" "hash"))
   (:file "lemma-generation" :depends-on ("package" "fof-object" "fof-splits"
                                          "statement-object" "tpi"))
   (:file "tpi-case-analysis" :depends-on ("package" "fof-object" "fof-splits"
                                          "statement-object" "tpi"
                                          "lemma-generation"))
   (:file "fof-eval" :depends-on ("package" "tptp" "tptp-ast"))
   (:file "vue" :depends-on ("package"))
   (:file "process-tptp-proof" :depends-on ("package" "tptp" "vue" "fof-object"))
   )
  )

