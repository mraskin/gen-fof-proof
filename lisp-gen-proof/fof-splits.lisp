(in-package :gen-fof-proof)

(defmethod fof-split-conjunction-claim (f) (list f))
(defmethod fof-split-disjunction-claim (f) (list f))
(defmethod fof-split-conjunction-claim ((f tptp-fof-quantified))
  (cond ((equal (name f) "!")
         (loop for subf in (fof-split-conjunction-claim (content f))
               collect (make-instance (class-of f)
                                      :name (name f)
                                      :args (args f)
                                      :content subf)))
        (t (call-next-method))))
(defmethod fof-split-disjunction-claim ((f tptp-fof-quantified))
  (cond ((equal (name f) "?")
         (loop for subf in (fof-split-disjunction-claim (content f))
               collect (make-instance (class-of f)
                                      :name (name f)
                                      :args (args f)
                                      :content subf)))
        (t (call-next-method))))
(defmethod fof-split-conjunction-claim ((f tptp-propositional-infix))
  (let* ((op (name f)))
    (cond
      ((equal op "&")
       (let* ((args (args f))
              (args-split (mapcar 'fof-split-conjunction-claim
                                  args))
              (subfs (reduce 'append args-split)))
         subfs))
      ((equal op "=>")
       (loop with args := (args f)
             with assume := (first args)
             with conclude := (second args)
             with subfs := (fof-split-conjunction-claim conclude)
             with success := (> (length subfs) 1)
             for subf in subfs
             unless success return (call-next-method)
             collect
             (make-instance (class-of f)
                            :name "=>"
                            :args (list assume subf))))
      ((equal op "<=")
       (loop with args := (args f)
             with assume := (second args)
             with conclude := (first args)
             with subfs := (fof-split-conjunction-claim conclude)
             with success := (> (length subfs) 1)
             for subf in subfs
             unless success return (call-next-method)
             collect
             (make-instance (class-of f)
                            :name "=>"
                            :args (list assume subf))))
      ((equal op "<=>")
       (let* ((args (args f))
              (left (first args))
              (right (second args))
              (subleft (fof-split-conjunction-claim left))
              (subright (fof-split-conjunction-claim right))
              (implications
                (append
                  (loop for sl in subleft collect
                        (list right sl))
                  (loop for sr in subright collect
                        (list left sr)))))
         (loop for i in implications collect
               (make-instance (class-of f)
                              :name "=>"
                              :args i))))
      (t (call-next-method)))))

(defun fof-and (f g)
  (make-instance
    'tptp-propositional-infix
    :name "&"
    :args
    (append
      (if
        (and (typep f 'tptp-propositional-infix)
             (equal (name f) "&"))
        (args f)
        (list f))
      (if
        (and (typep g 'tptp-propositional-infix)
             (equal (name g) "&"))
        (args g)
        (list g)))))

(defun fof-codistribute-propositional-assoc (f)
  (assert (typep f 'tptp-propositional-infix))
  (let* ((op (name f))
         (co-op (cond ((equal op "&") "|")
                      ((equal op "|") "&")
                      (t (error "Cannot distribute over ~a" op))))
         (components
           (loop for a in (args f)
                 if (and (typep a 'tptp-propositional-infix)
                         (equal (name a) co-op))
                 collect (fof-distribute-propositional-assoc a)
                 else collect (list a)))
         (products
           (apply
             'alexandria:map-product
             (lambda (&rest args)
               (make-instance 'tptp-propositional-infix
                              :name op
                              :args args))
             components)))
    products))
(defun fof-distribute-propositional-assoc (f)
  (assert (typep f 'tptp-propositional-infix))
  (let* ((op (name f))
         (co-op (cond ((equal op "&") "|")
                      ((equal op "|") "&")
                      (t (error "Cannot distribute over ~a" op))))
         (components
           (loop for a in (args f)
                 if (and (typep a 'tptp-propositional-infix)
                         (equal (name a) co-op))
                 collect (fof-codistribute-propositional-assoc a)
                 else collect (list a))))
    (mapcar
      'simplify-tptp-object
      (reduce 'append components))))

(defmethod fof-case-analysis ((f tptp-fof-quantified) (g tptp-fof-quantified))
  (assert (equal (mapcar 'name (args f))
                 (mapcar 'name (args g))))
  (assert (equal (name f) "!"))
  (assert (equal (name g) "!"))
  (loop for subf in (fof-case-analysis (content f) (content g))
        collect (make-instance (class-of f)
                               :name (name f)
                               :args (args f)
                               :content subf)))
(defmethod fof-case-analysis ((f tptp-propositional-infix) (g tptp-propositional-infix))
  (assert (equal (name f) "=>"))
  (cond ((equal (name g) "=>")
         (let* ((af (args f))
                (ag (args g))
                (f-assume (simplify-tptp-object (first af)))
                (g-assume (simplify-tptp-object (first ag)))
                (exact-match (equal
                               (format nil "~a" f-assume)
                               (format nil "~a" g-assume)))
                (f-assume-parts
                  (and (typep f-assume 'tptp-propositional-infix)
                       (loop for a in (args f-assume)
                             collect (format nil "~a" a))))
                (g-assume-parts
                  (and (typep g-assume 'tptp-propositional-infix)
                       (loop for a in (args g-assume)
                             collect (format nil "~a" a))))
                (conjunction-implication
                  (and f-assume-parts
                       g-assume-parts
                       (equal (name f-assume) "&")
                       (equal (name g-assume) "&")
                       (null (set-difference
                               g-assume-parts f-assume-parts
                               :test 'equal))))
                (disjunction-implication
                  (and f-assume-parts
                       g-assume-parts
                       (equal (name f-assume) "|")
                       (equal (name g-assume) "|")
                       (null (set-difference
                               f-assume-parts g-assume-parts
                               :test 'equal)))))
           (assert (or exact-match
                       conjunction-implication
                       disjunction-implication))
           (fof-case-analysis f (second (args g)))))
        ((equal (name g) "|")
         (loop for a in (fof-distribute-propositional-assoc g)
               collect
               (make-instance (class-of f)
                              :name "=>"
                              :args (list
                                      (fof-and (first (args f)) a)
                                      (second (args f))))))
        ((equal (name g) "&")
         (loop for a in (fof-codistribute-propositional-assoc g)
               collect
               (make-instance (class-of f)
                              :name "=>"
                              :args (list
                                      (fof-and (first (args f)) a)
                                      (second (args f))))))
        (t (error "Cannot do case analysis here"))))

(defmethod fof-precondition-compatibility ((f tptp-fof-quantified) (g tptp-fof-quantified))
  (assert (equal (mapcar 'name (args f))
                 (mapcar 'name (args g))))
  (assert (equal (name f) "!"))
  (assert (equal (name g) "!"))
  (assert (null (set-difference (args g) (args f) :test 'equal :key 'name)))
  (let* ((subf (fof-precondition-compatibility
                (content f) (content g))))
    (when subf
      (make-instance (class-of f)
                     :name (name f)
                     :args (args f)
                     :content subf))))
(defmethod fof-precondition-compatibility ((f tptp-propositional-infix) (g tptp-propositional-infix))
  (assert (equal (name f) "=>"))
  (cond ((equal (name g) "=>")
         (make-instance 'tptp-propositional-infix
                        :name "=>"
                        :args (list (first (args f))
                                    (first (args g)))))
        ((find (name g) '("|" "&") :test 'equal)
         nil)
        (t (error "Cannot do case analysis here"))))

(defmethod fof-simplified-cases-formula ((f tptp-fof-quantified))
  (assert (equal (name f) "!"))
  (make-instance (class-of f)
                 :name (name f)
                 :args (args f)
                 :content (fof-simplified-cases-formula (content f))))

(defmethod fof-simplified-cases-formula ((f tptp-propositional-infix))
  (cond
    ((equal (name f) "=>")
     (make-instance (class-of f)
                    :name (name f)
                    :args (list (first (args f))
                                (fof-simplified-cases-formula
                                  (second (args f))))))
    ((equal (name f) "|")
     (make-instance (class-of f)
                    :name (name f)
                    :args (fof-distribute-propositional-assoc f)))
    ((equal (name f) "&")
     (make-instance (class-of f)
                    :name "|"
                    :args (fof-codistribute-propositional-assoc f)))
    (t (error "Cannot do case analysis here"))))

(defmethod fof-extract-cases ((f tptp-fof-quantified))
  (assert (equal (name f) "!"))
  (fof-extract-cases (content f)))
(defmethod fof-extract-cases ((f tptp-propositional-infix))
  (cond
    ((equal (name f) "=>")
     (fof-extract-cases (second (args f))))
    ((equal (name f) "|")
     (fof-distribute-propositional-assoc f))
    ((equal (name f) "&")
     (fof-codistribute-propositional-assoc f))
    (t (error "Cannot do case analysis here"))))

(defmethod fof-extract-cases-core ((f tptp-fof-quantified))
  (if (equal (name f) "!")
    (fof-extract-cases-core (content f))
    (call-next-method)))
(defmethod fof-extract-cases-core ((f tptp-propositional-infix))
  (cond
    ((equal (name f) "=>")
     (second (args f)))
    (t (call-next-method))))
(defmethod fof-extract-cases-core ((f tptp-formula)) f)

(defmethod fof-inject-case ((f tptp-fof-quantified) g)
  (assert (equal (name f) "!"))
  (make-instance (class-of f)
                 :name (name f)
                 :args (args f)
                 :content (fof-inject-case (content f) g)))
(defmethod fof-inject-case ((f tptp-propositional-infix) g)
  (assert (equal (name f) "=>"))
  (make-instance (class-of f)
                 :name (name f)
                 :args (list
                         (make-instance 'tptp-propositional-infix
                                        :name "&"
                                        :args (list (first (args f)) g))
                         (second (args f)))))
