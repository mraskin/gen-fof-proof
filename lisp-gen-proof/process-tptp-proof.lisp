(in-package :gen-fof-proof)

(defun inference-deps (annotations)
  (when annotations
  (let* ((kind (first annotations)))
    (case kind
      ((tptp-syntax::annotations)
       (inference-deps (second (first-subtree
                                 annotations 'tptp-syntax::source))))
      ((tptp-syntax::general_term)
       (inference-deps (second annotations)))
      ((tptp-syntax::general_list)
       (inference-deps (first-subtree
                         annotations 'tptp-syntax::general_terms)))
      ((tptp-syntax::general_terms)
       (let* ((args (second annotations))
              (head (first args)))
         (if (symbolp head) (inference-deps args)
           (append (inference-deps head)
                   (inference-deps
                     (find
                       'tptp-syntax::general_terms
                       args :key (lambda (x) (and (listp x) (first x)))))))))
      ((tptp-syntax::general_function)
       (let* ((args (second annotations))
              (name (tptp-true-text (first args))))
         (when (equal name "inference")
           (let* ((parameters (first-subtree
                                args 'tptp-syntax::general_terms))
                  (rule (tptp-true-text
                          (first-subtree
                            parameters 'tptp-syntax::general_term)))
                  (info-and-parents (first-subtree
                                      (second parameters)
                                      'tptp-syntax::general_terms))
                  (parents (find
                             'tptp-syntax::general_terms
                             (second info-and-parents)
                             :key (lambda (x) (and (listp x) (first x))))))
             rule
             (inference-deps parents)))))
      ((tptp-syntax::general_data)
       (inference-deps (second annotations)))
      ((tptp-syntax::number tptp-syntax::atomic_word)
       (list (tptp-true-text annotations)))
      (t nil)))))

(defun process-tptp-proof (entries)
  (let* ((entries (tptp-entries-plist entries)))
    (loop for e in entries
          for ann := (getf e :annotations)
          for deps := (inference-deps ann)
          for rule := (tptp-true-text
                        (first-subtree
                          (first-subtree
                            (first-subtree
                              (first-subtree ann 'tptp-syntax::source)
                              'tptp-syntax::general_function)
                            'tptp-syntax::general_terms)
                          'tptp-syntax::general_term))
          collect (append e (list :inference-rule rule :deps deps)))))

(defun tptp-proof-to-dot (entries &key
                                  (label-function
                                    (lambda (e)
                                      (extract-parse-tree-text
                                        (getf e :formula) nil))))
  (format
    nil "digraph G {~%~{~a~%~}}~%"
    (loop
      with res := nil
      for e in (process-tptp-proof entries)
      for formula := (extract-parse-tree-text (getf e :formula) nil)
      for name := (tptp-true-text (getf e :name))
      do (push (format
                 nil
                 "\"~a\" [label=\"~a\", style=\"filled\", fillcolor=\"~a\"];"
                 name (funcall label-function e) "#ffffff"
                 ) res)
      do (loop for d in (getf e :deps) do
               (push (format
                       nil
                       "\"~a\" -> \"~a\" [color=\"~a\"];"
                       d name "#000000"
                       ) res))
      finally (return (reverse res)))))

(defun tptp-proof-node-coordinates (entries 
                                     &key
                                     (label-function
                                       (let ((counter 0))
                                         (lambda (e)
                                           e
                                           (format nil "~a" (incf counter)))))
                                     (layout-algorithm "dot"))
  (let* ((dot (tptp-proof-to-dot 
                entries
                :label-function
                label-function))
         (dot-command
           (if (find-package :nix-deps)
             (format
               nil "~a/bin/dot"
               (symbol-value (find-symbol (string :*graphviz*) :nix-deps)))
             "dot"))
         (dot-algo-switch (format nil "-K~a" layout-algorithm))
         (dot-reply
           (with-input-from-string (s dot)
             (format *trace-output* "Running dot on ~a bytes for ~a entries~%"
                     (length dot) (length entries))
             (uiop:run-program
               `(,dot-command "-Tdot" ,dot-algo-switch)
               :input s :output :string)))
         (dot-json
           (with-input-from-string (s dot-reply)
             (format *trace-output* "Running dot to get JSON on ~a bytes for ~a entries~%"
                     (length dot-reply) (length entries))
             (uiop:run-program
               `(,dot-command "-Tdot_json" ,dot-algo-switch)
               :input s :output :string)))
         (dot-data (cl-json:decode-json-from-string dot-json))
         (dot-nodes (loop for node in (cdr (assoc :objects dot-data))
                          for name := (cdr (assoc :name node))
                          for pos := (cdr (assoc :pos node))
                          for coords := (mapcar 'parse-number:parse-number
                                                (cl-ppcre:split "," pos))
                          for x := (first coords)
                          for y := (second coords)
                          collect (list :name name :x x :y y)))
         (dot-nodes-ht (make-hash-table :test 'equal)))
    (loop for n in dot-nodes do
          (setf (gethash (getf n :name) dot-nodes-ht) n))
    (format *trace-output* "Processed dot output~%")
    dot-nodes-ht))

(defun tptp-proof-to-vue (entries &key (scale 15) (xscale scale) (yscale scale))
  (with-output-to-string (s)
    (write-vue-lead-in s)
    (loop
      with cnt := 0
      with named-nodes := (make-hash-table :test 'equal)
      with node-coordinates := (tptp-proof-node-coordinates entries)
      for e in (process-tptp-proof entries)
      for formula := (extract-parse-tree-text (getf e :formula) nil)
      for name := (tptp-true-text (getf e :name))
      for num := (incf cnt)
      for rule := (getf e :inference-rule)
      for role := (tptp-true-text (getf e :role))
      for color := (cond
                     ((equal role "axiom") "#CCFFCC")
                     ((equal role "lemma") "#E7F7CC")
                     ((equal role "conjecture") "#FFFFBB")
                     ((equal role "negated_conjecture") "#BBFFFF")
                     ((equal role "plain") "#FFFFFF")
                     (t "#DDDDDD"))
      for coords := (gethash name node-coordinates)
      for x := (and coords (* xscale (getf coords :x)))
      for y := (and coords (* yscale (getf coords :y)))
      do (setf (gethash name named-nodes) num)
      do (write-vue-node
           s num formula :notes
           (format nil "~{~a~%~}" (list name role rule))
           :color color :x x :y y)
      do (loop for d in (getf e :deps) do
               (write-vue-edge s (incf cnt) "" (gethash d named-nodes) num
                               :notes rule))
      )
    (write-vue-lead-out s)))

(defun split-szs-proofs (text)
  (cond ((pathnamep text)
         (with-open-file (f text)
           (return-from split-szs-proofs (split-szs-proofs f))))
        ((streamp text)
         (return-from
           split-szs-proofs
           (split-szs-proofs
             (alexandria:read-stream-content-into-string text)))))
  (let* ((lines (cl-ppcre:split
                  (format nil "[~{~a~}]" (list #\Newline #\Return))
                  text)))
    (loop with res := nil
          with block := nil
          with in-block-p := nil
          for l in lines
          for start := (cl-ppcre:scan "^ *[#%]* *SZS output start( |$)" l)
          for end := (cl-ppcre:scan "^ *[#%]* *SZS output end( |$)" l)
          do (assert (not (and start in-block-p)))
          do (assert (or in-block-p (not end)))
          do (cond (start (setf in-block-p t))
                   (end (setf in-block-p nil)
                        (push (format nil "~{~a~%~}" (reverse block)) res)
                        (setf block nil))
                   (in-block-p (push l block))
                   (t ))
          finally (progn (assert (not in-block-p))
                         (return (reverse res))))))

(defun proof-summary (proof)
  (loop with conjecture := nil
        with assumptions := nil
        for e in (process-tptp-proof proof)
        for role := (tptp-true-text (getf e :role))
        for deps := (getf e :deps)
        for conjp := (equal role "conjecture")
        when conjp do (setf conjecture e)
        unless (or deps conjp) do (push e assumptions)
        finally (return (values assumptions conjecture))))

(defun summarize-split-proof (proofs)
  (let* ((parts (split-szs-proofs proofs))
         (lemma-numbers (make-hash-table :test 'equal))
         (axiom-numbers (make-hash-table :test 'equal))
         (lemma-counter 0)
         (axiom-counter 0))
    (format
      nil "~{~a~%~}"
      (loop
        for p in parts
        for axioms := nil
        for new-axioms := nil
        for lemmas := nil
        for conjecture := nil
        for ct := nil
        for cln := nil
        for counter upfrom 1
        do (format *trace-output*
                   "Summarizing proof part ~a / ~a ...~%"
                   counter (length parts))
        do (progn
             #+sbcl (sb-ext:gc :full t)
             )
        do
        (multiple-value-bind (assumptions conj) (proof-summary p)
          (loop for a in assumptions
                for f := (getf a :formula)
                for ft := (normalise-fof-formula
                            (extract-parse-tree-text f nil))
                for ln := (gethash ft lemma-numbers)
                for ano := (unless ln (gethash ft axiom-numbers))
                for ann := (unless (or ln ano)
                             (let ((k (incf axiom-counter)))
                               (setf (gethash ft axiom-numbers) k)
                               k))
                for an := (or ano ann)
                for num-a := (append
                               (list (if ln :lemma-number :axiom-number)
                                     (or ln an))
                               a)
                when ln do (push num-a lemmas)
                when ann do (push num-a new-axioms)
                when an do (push num-a axioms))
          (setf conjecture conj
                ct (normalise-fof-formula
                     (extract-parse-tree-text (getf conjecture :formula) nil))
                cln (or (gethash ct lemma-numbers)
                       (incf lemma-counter)))
          )
        do (setf (gethash ct lemma-numbers) cln)
        collect
        (format
          nil "~{~%~a~}"
          (loop for a in new-axioms
                for an := (getf a :axiom-number)
                for at := (normalise-fof-formula
                            (extract-parse-tree-text
                              (getf a :formula) nil))
                collect
                (format nil "fof(a_~8,'0d, axiom, ~a)." an at)))
        collect
        (format
          nil
          "fof(l_~8,'0d, plain, ~a, inference(fof_proof, [status(thm)],[~{~a~#[~:;,~]~}]))."
          cln ct
          (append
            (loop for a in axioms
                  for an := (getf a :axiom-number)
                  collect (format nil "a_~8,'0d" an))
            (loop for a in lemmas
                  for an := (getf a :lemma-number)
                  collect (format nil "l_~8,'0d" an))))))))

(defun proof-deadends (proof)
  (let* ((entries (process-tptp-proof proof))
         (seen (make-hash-table :test 'equal))
         (index (make-hash-table :test 'equal))
         (conclusion (first (last entries)))
         (used nil)
         (unused nil))
    (loop for e in entries
          for name := (tptp-true-text (tptp-no-space (getf e :name)))
          do (setf (gethash name index) e))
    (loop with stack := (list (tptp-true-text
                                (tptp-no-space (getf conclusion :name))))
          while stack
          for name := (pop stack)
          for entry := (gethash name index)
          for deps := (getf entry :deps)
          do (setf (gethash name seen) t)
          do (loop for d in deps
                   unless (gethash d seen)
                   do (push d stack)))
    (loop for e in entries
          for n := (tptp-true-text (tptp-no-space (getf e :name)))
          do (if (gethash n seen) (push n used) (push n unused)))
    (values
      (format nil "~{~a~%~}"
              (loop for n in (reverse unused)
                    for e := (gethash n index)
                    collect
                    (format
                      nil "fof(~a, unused, ~a)."
                      (tptp-true-text (tptp-no-space n))
                      (normalise-fof-formula
                        (extract-parse-tree-text
                          (getf e :formula) nil)))))
      (reverse unused) (reverse used))))

(defun split-proofs-simplification-checks (source mask)
  (loop with counter := 0
        with parts := (split-szs-proofs source)
        with pl := (length parts)
        for proof-part in parts
        for statements := (parse-tptp proof-part)
        for sl := (length statements)
        for k upfrom 1
        do (format *trace-output* "Processing ~a / ~a ~%" k pl)
        do (loop for s in statements
                 for kk upfrom 1
                 for formula :=
                 (loop
                   for marker in
                   '(
                     tptp-syntax::fof_formula
                     tptp-syntax::cnf_formula
                     )
                   for content := (first-subtree s marker)
                   when content return content)
                 for target :=
                 (when formula (format nil "~a.~8,'0d.p"
                                       mask (incf counter)))
                 for formula-text :=
                 (when formula (extract-parse-tree-text formula nil))
                 do (format *trace-output*
                            "sub-processing: ~a / ~a for ~a / ~a ~%"
                            kk sl k pl)
                 when formula-text do
                 (alexandria:write-string-into-file
                   (format nil "fof(c_~8,'0d,conjecture,(~a)<=>(~a)).~%"
                           counter formula-text
                           (normalise-fof-formula formula-text))
                   target :if-exists :supersede))))
