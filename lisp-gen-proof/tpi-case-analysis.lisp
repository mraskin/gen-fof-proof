(in-package :gen-fof-proof)

(defclass case-splitting-state ()
  ((processed-conditions :accessor processed-conditions
                         :initarg :processed-conditions
                         :initform nil
                         :type list)
   (unprocessed-conditions :accessor unprocessed-conditions
                           :initarg :unprocessed-conditions
                           :initform nil
                           :type list)
   (split-conditions :accessor split-conditions
                     :initarg :split-conditions
                     :initform nil
                     :type list)))

(defmethod print-object ((obj case-splitting-state) s)
  (format s "#<case-splitting-state: S~s U~s P~s>"
          (split-conditions obj)
          (unprocessed-conditions obj)
          (processed-conditions obj)))

(defmethod case-analysis-assumption ((s case-splitting-state))
  (assert (or (processed-conditions s)
              (unprocessed-conditions s)
              (split-conditions s)))
  (let* ((split-conditions (split-conditions s))
         (split-count (length split-conditions))
         (split-clause (case split-count
                         (0 nil)
                         (1 (first split-conditions))
                         (t (make-instance 'tptp-propositional-infix
                                           :name "|"
                                           :args split-conditions))))
         (straight-conditions
           (append (when split-clause (list split-clause))
                   (unprocessed-conditions s)
                   (processed-conditions s)))
         (straight-count (length straight-conditions))
         (straight-clause (case straight-count
                         (0 (error "This cannot be: condition set was nonempty"))
                         (1 (first straight-conditions))
                         (t (make-instance 'tptp-propositional-infix
                                           :name "&"
                                           :args straight-conditions)))))
    straight-clause))
(defmethod case-analysis-assumption ((f tptp-formula)) f)

(defun case-splitting-complete-p (s)
  (and (typep s 'case-splitting-state)
       (null (unprocessed-conditions s))
       (null (split-conditions s))))

(defun sort-by-string-length-asc (l)
  (sort (subseq l 0) '< :key
        (lambda (x) (length (format nil "~a" x)))))

(defmethod case-list-split-once ((f tptp-formula))
  (make-instance 'case-splitting-state
                 :processed-conditions
                 (list f)))
(defmethod case-list-split-once ((f tptp-propositional-infix))
  (cond
    ((equal (name f) "|")
     (make-instance 'case-splitting-state
                    :split-conditions
                    (sort-by-string-length-asc (args f))))
    ((equal (name f) "&")
     (make-instance 'case-splitting-state
                    :unprocessed-conditions
                    (sort-by-string-length-asc (args f))))
    (t (call-next-method))))

(defun case-splitting-join (s1 s2)
  (assert (or (null (split-conditions s1))
              (null (split-conditions s2))))
  (make-instance 'case-splitting-state
                 :split-conditions
                 (or (split-conditions s1)
                     (split-conditions s2))
                 ; The order assumes that the first unprocessed condition
                 ; for s1 was split into s2
                 :processed-conditions
                 (append (processed-conditions s2)
                         (processed-conditions s1))
                 :unprocessed-conditions
                 (sort-by-string-length-asc
                   (append 
                     (unprocessed-conditions s1)
                     (unprocessed-conditions s2)))))
(defun case-splitting-drop-first-unprocessed (s)
  (make-instance 'case-splitting-state
                 :split-conditions (split-conditions s)
                 :processed-conditions (processed-conditions s)
                 :unprocessed-conditions
                 (rest (unprocessed-conditions s))))

(defun case-splitting-step (s)
  (cond ((typep s 'tptp-formula)
         (list
           (make-instance 'case-splitting-state
                          :unprocessed-conditions (list s))))
        ((case-splitting-complete-p s) (list s))
        ((split-conditions s)
         (let* ((uc (unprocessed-conditions s))
                (pc (processed-conditions s))
                (sc (split-conditions s))
                (scl (length sc))
                (fsc (first sc))
                (rsc (rest sc))
                (ssc (first rsc))
                (fcss
                  (make-instance 'case-splitting-state
                                 :unprocessed-conditions
                                 (sort-by-string-length-asc (cons fsc uc))
                                 :processed-conditions pc
                                 :split-conditions nil)))
           (case scl
             (0 (error "This is impossible: split-conditions was nonempty"))
             (1 (list fcss))
             (2 (list fcss
                      (make-instance 'case-splitting-state
                                     :unprocessed-conditions
                                     (sort-by-string-length-asc (cons ssc uc))
                                     :processed-conditions pc
                                     :split-conditions nil)))
             (t (list fcss
                      (make-instance 'case-splitting-state
                                     :unprocessed-conditions uc
                                     :processed-conditions pc
                                     :split-conditions rsc))))))
        ((unprocessed-conditions s)
         (let* ((c (first (unprocessed-conditions s)))
                (step (case-list-split-once c))
                (result (case-splitting-join
                          (case-splitting-drop-first-unprocessed s)
                          step)))
           (list result)))))

(defun case-analysis-tree (name formula assumption group
                                &key
                                bottom-group
                                bottom-role
                                include-equiassumption-entries)
  (let* ((leaf (case-splitting-complete-p assumption))
         (assumption-formula (case-analysis-assumption assumption))
         (local-name (format nil
                             "cat_~a_case_analysis_~a_~a"
                             name 
                             (if leaf "leaf" "tree_node")
                             (sha512 assumption-formula)))
         (local-role
           (if leaf
             (or bottom-role
                 (and bottom-group
                      (format nil "checked_lemma_~a"
                              bottom-group))
                 "checked_lemma")
             (format nil "checked_lemma_~a" local-name)))
         (current-implication (fof-inject-case
                                formula assumption-formula))
         (implication-statement
           (make-instance
             'tptp-statement
             :kind "fof" :name local-name
             :role local-role
             :content current-implication))
         (implication-entry
           (list (append
                   (if leaf
                     (list "$case_analysis_tree"
                           "$case_analysis_tree_leaf")
                     (list "$case_analysis_tree"
                           "$case_analysis_tree_node"
                           "$quiet" "$trivial"))
                   (list group "tpi"))
                 (format nil "~a" implication-statement)
                 implication-statement))
         (dependencies
           (unless leaf
             (case-splitting-step assumption)))
         (single-dep-assumption
           (when (= (length dependencies) 1)
             (case-analysis-assumption
               (first dependencies))))
         (equiassumption-core
           (when single-dep-assumption
             (make-instance 'tptp-propositional-infix
                            :name "=>"
                            :args (list assumption-formula
                                        single-dep-assumption))))
         (equiassumption-formula
           (and equiassumption-core
                (make-instance
                  'tptp-fof-quantified
                  :name "!"
                  :args (loop for v in
                              (tptp-object-free-variables
                                equiassumption-core)
                              collect
                              (make-instance 'tptp-object-variable
                                             :name v))
                  :content equiassumption-core)))
         (equiassumption-name
           (when equiassumption-formula
             (format nil "cat_~a_case_analysis_equiassumption_~a"
                     name (sha512 equiassumption-formula))))
         (equiassumption-statement
           (when equiassumption-formula
             (make-instance 'tptp-statement
                            :name equiassumption-name
                            :role (format nil "checked_lemma_tautology_~a"
                                          equiassumption-name)
                            :kind "fof"
                            :content equiassumption-formula)))
         (equiassumption-entry
           (when equiassumption-statement
             (list 
               (list "tpi"
                     local-name
                     "$case_analysis_tree"
                     "$case_analysis_tree_equiassumption"
                     "$quiet" "$trivial" "$textual")
               (format nil "~a" equiassumption-statement)
               equiassumption-statement)))
         (passthrough
           (and single-dep-assumption
                (equal (format nil "~a" single-dep-assumption)
                       (format nil "~a" assumption-formula))))
         (subtrees
           (loop for d in dependencies
                 collect
                 (case-analysis-tree
                   name formula d
                   (if passthrough group local-name)
                   :bottom-group bottom-group
                   :bottom-role bottom-role
                   :include-equiassumption-entries
                   include-equiassumption-entries)))
         (subtrees-joined (reduce 'append subtrees)))
    (if passthrough
      subtrees-joined
      (append
        (list implication-entry)
        (when
          (and equiassumption-entry
               include-equiassumption-entries)
          (list equiassumption-entry))
        subtrees-joined))))

(def-tpi-handler "add_cases" (data context)
  (let* ((input data)
         (disjunction-name (name (first (args input))))
         (target-name (name (second (args input))))
         (disjunction
           (find 
             disjunction-name
             (gethash :formulae context)
             :test 'equal
             :key (lambda (x) (name (third x)))))
         (disjunction-formula (content (third disjunction)))
         (formulae-split
           (split-formulae
             (lambda (&key name role)
               (and (cl-ppcre:scan "^checked_lemma" role)
                    (equal name target-name)))
             context))
         (target (second formulae-split))
         (target-statement (second target))
         (target-groups (first target))
         (target-formula
           (progn
             (assert target-statement)
             (content target-statement)))
         (group (format nil "ca_~a_case_analysis_~a"
                        target-name (sha512 target-formula)))
         (fragment-entries
           (progn
             (case-analysis-tree
               target-name target-formula
               (fof-extract-cases-core disjunction-formula)
               group
               :include-equiassumption-entries
               (= 0 (length (getenv_value
                              "=TPI_CASE_ANALYSIS_SKIP_EQUIASSUMPTION"
                              context))))))
         (precondition-implication (fof-precondition-compatibility
                                     target-formula disjunction-formula))
         (precondition-statement
           (when precondition-implication
             (assert (role (third disjunction)))
             (make-instance 'tptp-statement
                            :kind "fof"
                            :name 
                            (format 
                              nil "cas_~a_case_analysis_sufficiency_~a"
                              target-name (sha512 precondition-implication))
                            :role (role (third disjunction))
                            :content precondition-implication)))
         (precondition-entry
           (when precondition-implication
             (list
               (list group "tpi")
               (format nil "~a" precondition-statement)
               precondition-statement)))
         (new-statement
           (make-instance 'tptp-statement
                          :name target-name
                          :kind "fof"
                          :role (format nil "checked_lemma_~a"
                                        group)
                          :content target-formula))
         (target-entry
           (list target-groups
                 (format nil "~a" new-statement)
                 new-statement)))
    (push group (first disjunction))
    (loop for e in fragment-entries
          for s := (third e)
          for n := (name s)
          do (setf (gethash n (gethash :inactive-formulae context)) t))
    (setf
      (gethash :formulae context)
      (append (first formulae-split)
              (list target-entry)
              fragment-entries
              (when precondition-entry (list precondition-entry))
              (third formulae-split)))))
