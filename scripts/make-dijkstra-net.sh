#! /bin/sh

NO_RLWRAP=1 ../lisp-gen-proof/gen-fof-proof.bin --non-interactive \
        --load make-petri-net.lisp \
        --eval "(dijkstra-net-to-file $1 \"${2:-dijkstra-${1}.pnet}\")"
