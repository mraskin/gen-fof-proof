from arithmetic import *

from operator import *

from enum import Enum, unique

from abc import ABC

class Statement(ABC):
    def __init__(self):
        self.label = None
        self.pc_advance = 1

    def __repr__(self):
        return "Statement[{}]({})".format(self.__class__.__name__, str(self))

    def flatten(self):
        return [self]

class Skip(Statement):
    def __str__(self):
        return "skip; [pc: +{}]".format(self.pc_advance)

class Goto(Statement):
    def __init__(self, goto_label):
        self.label = None
        self.pc_advance = 0
        self.goto_label = goto_label
        self.goto_index = -1

    def __str__(self):
        r = "goto {}".format(self.goto_label)
        if self.goto_index >= 0:
            r = "{} <{}>".format(r, self.goto_index)
        r = "{} [pc: +{}]".format(r, self.pc_advance)
        return r

class Assignment(Statement):
    def __init__(self, var, expr):
        self.label = None
        self.pc_advance = 1
        self.var = var
        self.expr = expr

    def __str__(self):
        return "{} := {} [pc: +{}]".format(str(self.var), str(self.expr),
                self.pc_advance)

class Condition(object):
    operators = {
            "==": eq,
            "!=": ne,
            "<=": le,
            "<" : lt,
            ">=": ge,
            ">" : gt
            }

    def __init__(self, lhs, comparison, rhs):
        self.lhs = lhs
        self.operation = self.operators[comparison]
        self.comparison_type = comparison
        self.rhs = rhs

    def __repr__(self):
        return "Condition({})".format(str(self))

    def __str__(self):
        return "if {} {} {}".format(
                str(self.lhs),
                self.comparison_type, str(self.rhs))

class Block(Statement):
    def __init__(self, statements):
        self.statements = statements

    def __repr__(self):
        return "Block({})".format(str(self))

    def __str__(self):
        r = "{\n"
        for stmt in self.statements:
            r += "\t" + str(stmt) + "\n"
        r += "}"
        return r

    def flatten(self):
        statements = []
        for stmt in self.statements:
            statements.extend(stmt.flatten())
        return statements

class Conditional(Statement):
    def __init__(self, condition, if_block, else_block):
        self.if_block   = if_block
        self.else_block = else_block
        self.condition  = condition

        self.if_block_len   = None
        self.else_block_len = None
        self.label          = None
        self.pc_advance     = 1

    def flatten(self):
        if_block = self.if_block.flatten()
        self.if_block_len = len(if_block)
        else_block = self.else_block.flatten() if self.else_block else []
        self.else_block_len = len(else_block)
        return [self] + if_block + else_block

    def __str__(self):
        if self.if_block_len:
            return "{} <+{}> <+{}> [pc: +{}]".format(
                    str(self.condition),
                    self.if_block_len,
                    self.else_block_len,
                    self.pc_advance)
        else:
            r = "{} {}".format(str(self.condition), str(self.if_block))
            if self.else_block:
                return "{} {}".format(r, str(self.else_block))
            else:
                return r
