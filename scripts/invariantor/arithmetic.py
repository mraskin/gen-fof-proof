import operator

class ArithmeticException(Exception):
    pass

class BinaryOperator(object):
    def __init__(self, left, right):
        self.left = left
        self.right = right

    def __str__(self):
        return "({left} {op} {right})".format(left = str(self.left),
                op = self.op, right = str(self.right))

    def __repr__(self):
        return "{class_name}({string})".format(
                class_name = type(self).__name__,
                string = str(self))


    @property
    def terminal_vars(self):
        return self.left.terminal_vars | self.right.terminal_vars

    @property
    def indexed_vars(self):
        return self.left.indexed_vars | self.right.indexed_vars

class Addition(BinaryOperator):
    op = '+'
    operation = operator.add

class Subtraction(BinaryOperator):
    op = '-'
    operation = operator.sub

class Constant(object):
    def __init__(self, value):
        self.value = int(value)

    def __str__(self):
        if self.value < 0:
            return "({})".format(str(self.value))
        else:
            return str(self.value)

    def __repr__(self):
        return "Constant({})".format(str(self))

    @property
    def is_terminal(self):
        return True

    @property
    def terminal_vars(self):
        return set()

    @property
    def indexed_vars(self):
        return set()

class Variable(object):
    def __init__(self, name, index = None):
        self.index = index
        self.name = str(name)

    def __hash__(self):
        return hash(self.name)

    @property
    def is_terminal(self):
        return not self.index

    @property
    def terminal_vars(self):
        if self.is_terminal:
            return {self}
        else:
            return self.index.terminal_vars

    @property
    def indexed_vars(self):
        if self.is_terminal:
            return set()
        else:
            return self.index.indexed_vars | {self}

    def __str__(self):
        if not self.index:
            return self.name
        else:
            return "{}[{}]".format(self.name, str(self.index))

    def __repr__(self):
        return "Variable({})".format(str(self))
