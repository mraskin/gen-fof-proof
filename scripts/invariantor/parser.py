from lark import Lark, Tree, Token, v_args
from lark.visitors import Transformer_InPlace

from arithmetic import *
from statements import *
from program    import *

grammar = r"""
%import common.WS
%ignore WS

COMMENT : /#.*/
%ignore COMMENT

%import common.CNAME -> VARNAME
%import common.SIGNED_INT -> SIGNED_INT
%import common.INT -> INT

program : globals locals block

globals : "globals" ":" [varlist] ";"

locals : "locals" ":" [varlist] ";"

varlist : VARNAME ( "," VARNAME )*

block : (statement ";")+

statement : action | label ":" action

?label : VARNAME

?action : assignment
        | goto
        | conditional
        | skip

skip : "skip"

assignment : variable ":=" expression

variable : VARNAME | VARNAME "[" variable "]"

?expression : variable
           | SIGNED_INT
           | addition
           | subtraction

addition : expression "+" expression

subtraction : expression "-" expression

goto : "goto" VARNAME

conditional : "if" condition "{" block "}" ["else" "{" block "}" ]

?condition : expression comparison expression

?comparison : neq
            | eq
            | lt
            | leq
            | gt
            | geq

neq : "!="
eq  : "=="
lt  : "<"
leq : "<="
gt  : ">"
geq : ">="

"""

parser = Lark(grammar, start = "program")

class Preprocessor(Transformer_InPlace):
    def __init__(self):
        self.global_variables = []
        self.local_variables = []

    def variable(self, children):
        if len(children) == 1:
            return Variable(children[0])
        else:
            return Variable(children[0], index = children[1])

    def addition(self, children):
        lhs = Constant(children[0].value) if type(children[0]) is Token else children[0]
        rhs = Constant(children[1].value) if type(children[1]) is Token else children[1]
        return Addition(lhs, rhs)

    def subtraction(self, children):
        lhs = Constant(children[0].value) if type(children[0]) is Token else children[0]
        rhs = Constant(children[1].value) if type(children[1]) is Token else children[1]
        return Subtraction(lhs, rhs)

    def assignment(self, children):
        rhs = Constant(children[1].value) if type(children[1]) is Token else children[1]
        return Assignment(children[0], rhs)

    def goto(self, children):
        return Goto(children[0].value)


    def eq(self, _):
        return "=="

    def neq(self, _):
        return "!="

    def leq(self, _):
        return "<="

    def lt(self, _):
        return "<"

    def geq(self, _):
        return ">="

    def gt(self, _):
        return ">"

    def condition(self, children):
        lhs  = children[0]
        comp = children[1]
        rhs  = children[2]

        lhs  = Constant(lhs.value) if type(lhs) is Token else lhs
        rhs  = Constant(rhs.value) if type(rhs) is Token else rhs
        return Condition(lhs, comp, rhs)

    def skip(self, children):
        return Skip();

    def varlist(self, children):
        return [c.value for c in children]

    def statement(self, children):
        if type(children[0]) is Token:
            label = children[0].value
            stmt  = children[1]
        else:
            label = None
            stmt  = children[0]
        stmt.label = label
        return stmt

    def conditional(self, children):
        return Conditional(children[0], children[1],
                children[2] if len(children) > 2 else None)

    def block(self, children):
        return Block(children)

    def globals(self, children):
        if children:
            return children[0]
        else:
            return []

    def locals(self, children):
        if children:
            return children[0]
        else:
            return []

def parse_ast(ast):
    # transformer
    preprocessor = Preprocessor()
    ast = preprocessor.transform(ast)
    program = Program(ast.children[0], ast.children[1], ast.children[2].flatten())
    print(program)
    return program

def parse_file(file_name):
    with open(file_name) as f:
        text = f.read()
        ast = parser.parse(text)
        # print(ast)
        return parse_ast(ast)

if __name__ == "__main__":
    ast = parse_file("example.inv")
