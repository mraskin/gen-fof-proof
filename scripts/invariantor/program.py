from statements import *

class ProgramError(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __repr__(self):
        return "ProgramError({})".format(self.msg)

class Process(object):
    def __init__(self, program, id, n, global_variables):
        self.program = program
        self.id      = id
        self.n       = n
        self.pc      = 0
        self.local_variables  = {name: 0 for name in self.program.local_variables}
        self.global_variables = global_variables

    def step(self):
        if not self.active: return
        current_stmt = self.program.statements[self.pc]
        current_stmt.evaluate(self)

    def get_var(self, var_name):
        if var_name == "id":
            return self.id
        elif var_name == "n":
            return self.n
        elif var_name in self.local_variables:
            return self.local_variables[var_name]
        elif var_name in self.global_variables:
            return self.global_variables[var_name]
        else:
            raise ProgramError(
                    "Value for unknown variable {} requested".format(var_name))

    def set_var(self, var_name, value):
        if var_name == "id" or var_name == "n":
            raise ProgramError("Cannot set constants id, n")
        elif var_name in self.local_variables:
            self.local_variables[var_name] = value
        elif var_name in self.global_variables:
            self.global_variables[var_name] = value
        else:
            raise ProgramError(
                    "Trying to set value {} for unknown variable {}".format(
                        value, var_name))

    @property
    def active(self):
        return 0 <= self.pc < len(self.program)

class Program(object):
    def __init__(self, global_variables, local_variables, statements):
        self.global_variables = global_variables
        self.local_variables = local_variables
        self.statements = statements
        labels = {}
        for index, stmt in enumerate(self.statements):
            if stmt.label:
                labels[stmt.label] = index
        for index, stmt in enumerate(self.statements):
            if type(stmt) is Goto:
                stmt.goto_index = labels[stmt.goto_label]
            elif type(stmt) is Conditional:
                try:
                    last_if_stmt = self.statements[index + stmt.if_block_len]
                    last_if_stmt.pc_advance = stmt.else_block_len + 1
                except IndexError:
                    msg = "Unexpected length <{}> of if_block for statement {}:{}"
                    msg = msg.format(stmt.if_block_len, index, stmt)
                    raise ProgramError(msg)
        self._check_variable_indexing()
        self._check_assignments()

    def __len__(self):
        return len(self.statements)

    def _check_variable_indexing(self):
        for index, stmt in enumerate(self.statements):
            if type(stmt) is Conditional:
                cond = stmt.condition
                indexed_vars = cond.rhs.indexed_vars | cond.lhs.indexed_vars
                terminal_vars = cond.rhs.terminal_vars | cond.lhs.terminal_vars
            elif type(stmt) is Assignment:
                indexed_vars = stmt.var.indexed_vars | stmt.expr.indexed_vars
                terminal_vars = stmt.var.terminal_vars | stmt.expr.terminal_vars
            else:
                indexed_vars = set()
                terminal_vars = set()
            indexed_vars_names = {v.name for v in indexed_vars}
            terminal_vars_names = {v.name for v in terminal_vars}
            unallowed_terminals = (terminal_vars_names
                    - (set(self.global_variables) | {"id", "N"}))
            unallowed_indexed   = (indexed_vars_names
                    - set(self.local_variables))
            if unallowed_terminals:
                msg = "Statement {}:{} uses {} as terminal variable(s)".format(
                        index, str(stmt), str(unallowed_terminals))
                raise ProgramError(msg)
            if unallowed_indexed:
                msg = "Statement {}:{} uses {} as indexed variable(s)".format(
                        index, str(stmt), str(unallowed_indexed))
                raise ProgramError(msg)

    def _check_assignments(self):
        for index, stmt in enumerate(self.statements):
            if type(stmt) is Assignment:
                if stmt.var.name in {"id", "n"}:
                    msg = ("Statement {}: {} tries to assign a value to "
                            + "the constant {}").format(
                                    index, str(stmt), stmt.var.name)
                    raise ProgramError(msg)

    def _check_global_local(self):
        global_variables = set(self.global_variables) + {"id", "n"}
        local_variables  = set(self.local_variables)
        if not global_variables.isdisjoint(local_variables):
            raise ProgramError("global and local variables are not disjoint!")

    def __repr__(self):
        return "Program({})".format(str(self))

    def __str__(self):
        r = ""
        max_index = len(self.statements) - 1
        for index, stmt in enumerate(self.statements):
            r += "{}: {}\n".format(str(index).zfill(len(str(max_index))), str(
                stmt))
        return r
