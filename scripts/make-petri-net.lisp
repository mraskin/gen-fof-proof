(defun pn-place-list (kind places)
  (format nil "~a {~%  ~{~a ~}~%}" kind places))

(defun pn-places (places)
  (pn-place-list "places" places))
(defun pn-initial (places)
  (pn-place-list "initial" places))

(defun pn-single-transition (transition)
  (format nil "{ ~{~a ~}} -> ~a -> { ~{~a ~}}"
          (second transition) (first transition) (third transition)))

(defun pn-transitions (transitions)
  (let* ((counter 0)
         (named-transitions
           (loop for trans in transitions
                 collect
                 (list (format nil "transition_~a_~a"
                               (first trans) (incf counter))
                       (second trans) (third trans))))
         (transition-name-list
           (pn-place-list "transitions"
                          (mapcar 'first named-transitions)))
         (arc-list
           (format nil "arcs {~%~{  ~a~%~}}"
                   (mapcar 'pn-single-transition named-transitions))))
    (format nil "~{~a~#[~:;~%~]~}"
            (list transition-name-list arc-list))))

(defun pn-body (name transitions initial)
  (let* ((place-mentions (loop for trans in transitions
                               unless (stringp (first trans))
                               do (error "Broken transition ~a" trans)
                               append (second trans)
                               append (third trans)))
         (places (sort place-mentions 'string<))
         (places (loop for prev := nil then current
                       for current in places
                       unless (equal current prev)
                       collect current)))
    (format nil "petri net ~s {~%~{~a~%~}}"
            name
            (list (pn-places places)
                  (pn-transitions transitions)
                  (pn-initial initial)))))

(defun pn-property (kind name content)
  (format nil "~a property ~s {~%  ~a~%}" kind name content))

(defun pn-liveness-property (name content)
  (pn-property "liveness" name content))

(defun pn-safety-property (name content)
  (pn-property "safety" name content))

(defun pn-notation (name transitions initial safety liveness)
  (format nil "~{~a~%~}"
          (append
            (list (pn-body name transitions initial))
            (loop for prop in safety collect (apply 'pn-safety-property prop))
            (loop for prop in liveness
                  collect (apply 'pn-liveness-property prop)))))

(defvar *n* 0)

(defun var-place (name agent value)
  (format nil "var_~a_of_~a_is_~a" name agent value))

(defun agent-state (agent name)
  (format nil "state_of_~a_is_~a" agent name))

(defun single-transition-entry (from to agent &key
                                     name-suffix
                                     replace observe)
  (list (format nil "~a~a~a"
                (agent-state agent from)
                (if name-suffix "_" "")
                (or name-suffix ""))
        `(,(agent-state agent from)
           ,@(mapcar 'first replace)
           ,@observe)
        `(,(agent-state agent to)
           ,@(mapcar 'second replace)
           ,@observe)
        agent))

(defun direct-transition (from to)
  (loop for agent from 1 to *n*
        collect (single-transition-entry from to agent)))

(defun add-replacement (transition from-place to-place)
  (list (first transition)
        (cons from-place (second transition))
        (cons to-place (third transition))
        (fourth transition)))

(defun add-observation-single (transition place)
  (add-replacement transition place place))

(defun add-observation (transitions place)
  (loop for transition in transitions
        collect (add-observation-single transition place)))

(defun decode-value (value agent)
  (cond ((numberp value) value)
        ((eq value :agent) agent)
        ((eq value :global) 0)
        ((eq value :false) 0)
        ((eq value :true) 1)
        ((eq value :n-proc) *n*)
        ((eq value :overflow-proc) (1+ *n*))
        (t (error "No idea what to do with value ~s" value))))

(defun add-assignment-single (transition var value range &key global)
  (let* ((agent (fourth transition))
         (target-agent (if global 0 agent)))
    (loop for old from (first range) to (second range)
          collect (add-replacement
                    transition
                    (var-place var target-agent old)
                    (var-place var target-agent
                               (decode-value value agent))))))

(defun add-assignment (transitions var value range &key global)
  (loop for transition in transitions
        append (add-assignment-single transition var value range
                                      :global global)))

(defun add-increment-single (transition var range)
  (loop for old from (first range) to (second range)
        collect (add-replacement
                  transition
                  (var-place var (fourth transition) old)
                  (var-place var (fourth transition) (1+ old)))))

(defun add-increment (transitions var range)
  (loop for transition in transitions
        append (add-increment-single transition var range)))

(defun add-var-condition-single (transition value var-path start &key negate)
  (if (null var-path)
    (when (equal (not negate)
                 (equal (decode-value start (fourth transition))
                        (decode-value value (fourth transition))))
      (list transition))
    (let ((start (decode-value start (fourth transition)))
          (var (first (first var-path)))
          (range (second (first var-path)))
          (rest-path (rest var-path)))
      (loop for var-value from (first range) to (second range)
            append
            (add-observation
              (add-var-condition-single
                transition value rest-path var-value :negate negate)
              (var-place var start var-value))))))

(defun add-var-condition (transitions value var-path start &key negate)
  (loop for transition in transitions
        append (add-var-condition-single
                 transition value var-path start
                 :negate negate)))

(defun conditional-transition (from then else value var-path start)
  (append
    (add-var-condition
      (direct-transition from then)
      value var-path start :negate nil)
    (add-var-condition
      (direct-transition from else)
      value var-path start :negate t)))

(defun aggregate-counts (places)
  (let* ((ht (make-hash-table :test 'equal)))
    (loop for p in places do (incf (gethash p ht 0)))
    ht))

(defun transition-allowed-p (transition aggregated-configuration)
  (let* ((ok t))
    (maphash (lambda (k v)
               (when (< (gethash k aggregated-configuration 0) v)
                 (setf ok nil)))
             (aggregate-counts (second transition)))
    ok))

(defun allowed-transitions (transitions configuration)
  (let* ((counts (aggregate-counts configuration)))
    (sort
      (remove-if
        (lambda (trans) (not (transition-allowed-p trans counts)))
        transitions)
      'string< :key 'first)))

(defun apply-transition (transition configuration)
  (let* ((in-counts (aggregate-counts (second transition))))
    (append (third transition)
            (loop for c in configuration
                  if (> (gethash c in-counts 0) 0)
                  do (decf (gethash c in-counts))
                  else collect c
                  finally (maphash (lambda (k v) k (assert (= v 0)))
                                   in-counts)))))

(defun apply-some-transition (transitions configuration &optional choice)
  (let* ((allowed (allowed-transitions transitions configuration))
         (choice (or choice (random (length allowed)))))
    (apply-transition (elt allowed choice) configuration)))

(defun run-transition-sequence (initial transitions choices &key formatter)
  (loop for choice in (cons nil choices)
        for configuration := initial then
        (apply-some-transition transitions configuration choice)
        do (format t "~a~%" (funcall (or formatter 'identity) configuration))
        finally (return configuration)))

(defun cut-prefix (prefix string)
  (and (>= (length string) (length prefix))
       (equal (subseq string 0 (length prefix)) prefix)
       (subseq string (length prefix))))

(defun collect-agent-data (configuration agent)
  (loop with states := nil
        with vars := nil
        for place in configuration
        for state := (cut-prefix (agent-state agent "") place)
        for var-match := (multiple-value-list
                           (cl-ppcre:scan
                             (var-place "([a-zA-Z]+)" agent "([0-9]+)")
                             place))
        for var := (and
                     (first var-match)
                     (destructuring-bind (start length starts ends)
                       var-match
                       (and(= start 0)
                         (= length (length place))
                         (loop for k from 0 to (1- (length starts)) 
                               collect
                               (subseq place (elt starts k) (elt ends k))))))
        when state do (push state states)
        when var do (push var vars)
        finally (return
                  (append
                    (list states)
                    (sort vars 'string< :key 'first)))))

(defun extract-agents (configuration range)
  (loop for agent from (first range) to (second range)
        collect (collect-agent-data configuration agent)))

(defun agent-based-formatter (n)
  (lambda (configuration)
    (format nil "~{~a~%~}"
            (loop for a in (extract-agents configuration (list 0 n))
                  for k upfrom 0
                  collect (format nil "~a:~{ ~a~}~{ ~{~a=~a~}~}"
                                  k (or (first a) (list "?"))
                                  (rest a))))))

(defun run-petri-net (net choices &key formatter)
  (run-transition-sequence (third net) (second net) choices
                           :formatter formatter))

(defun dijkstra-net-data (n)
  (let* ((*n* n))
    (list
      (format
        nil "Dijkstra's mutual exclusion algorithm for ~a agents" n)
      (append
        (direct-transition "initialState" "forbidSteal")
        (add-assignment
          (direct-transition "forbidSteal" "checkTurn")
          "stealable" :false (list 0 1))
        (conditional-transition
          "checkTurn" "declareEntry" "failureToEnter"
          :agent `(("turn" (0 ,*n*))) :global)
        (add-assignment
          (conditional-transition
            "failureToEnter" "cannotGrab" "grabTurn"
            :false `(("turn" (0 ,*n*)) ("stealable" (0 1))) :global)
          "outside" :true (list 0 1))
        (add-assignment
          (direct-transition "grabTurn" "cannotGrab")
          "turn" :agent (list 0 *n*) :global t)
        (direct-transition "cannotGrab" "checkTurn")
        (add-assignment
          (direct-transition "declareEntry" "startCheck")
          "outside" :false (list 0 1))
        (add-assignment
          (direct-transition "startCheck" "selfCheck")
          "counter" 1 (list 1 (1+ *n*)))
        (conditional-transition
          "selfCheck" "step" "collisionCheck"
          :agent `(("counter" (1 ,(1+ *n*)))) :agent)
        (conditional-transition
          "collisionCheck" "checkTurn" "step"
          :false
          `(("counter" (1 ,*n*))
            ("outside" (0 1)))
          :agent)
        (add-increment
          (direct-transition "step" "stepCheck")
          "counter" (list 1 *n*))
        (conditional-transition
          "stepCheck" "criticalSection" "selfCheck"
          :overflow-proc `(("counter" (1 ,(1+ *n*)))) :agent)
        (direct-transition "criticalSection" "yieldTurn")
        (add-assignment
          (direct-transition "yieldTurn" "declareExit")
          "turn" 0 (list 0 *n*) :global t)
        (add-assignment
          (direct-transition "declareExit" "permitSteal")
          "outside" 1 (list 0 1))
        (add-assignment
          (direct-transition "permitSteal" "initialState")
          "stealable" 1 (list 0 1))
        )
      (append
        (loop for agent from 1 to *n*
              collect (agent-state agent "initialState")
              collect (var-place "outside" agent 1)
              collect (var-place "stealable" agent 1)
              collect (var-place "counter" agent 1))
        (list
          (var-place "stealable" 0 1)
          (var-place "turn" 0 0)))
      `(("Agent 1 and agent 2 do not conflict"
         ,(format nil "~a + ~a >= 2"
                  (agent-state 1 "criticalSection")
                  (agent-state 2 "criticalSection"))))
      `(#+nil("Agent 1 can reach critical section"
              ,(format nil "~a > 0"
                       (agent-state 1 "criticalSection")))))))

(defun dijkstra-net (n)
  (apply 'pn-notation (dijkstra-net-data n)))

(defun dijkstra-net-to-file (n filename)
  (with-open-file (f filename :direction :output :if-exists :supersede)
    (format f "~a~%" (dijkstra-net n))))
