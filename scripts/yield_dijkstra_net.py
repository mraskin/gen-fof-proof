#!/bin/env python
import collections.abc
import sys

class Node(object):
    def __init__(self, name):
        self.name = str(name)
        self.outgoing = list()
        self.incoming = list()

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return self.name

    @property
    def stripped_name(self):
        return self.name.replace(" ", "").replace("[", "").replace(
                "]", "").replace(":=", "Becomes").replace(
                "!=", "Unequals").replace("<=", "Lessequal").replace(
                ">=", "Moreequal").replace("<", "Less").replace(
                ">", "More").replace("=", "Equals").replace("(", "").replace(
                ")", "").replace("++", "Increases").replace(",", "")

    def __repr__(self):
        return "{} ({})".format(self.__class__.__name__, self.name)

    def connect_to(self, others):
        if not isinstance(others, collections.abc.Iterable):
            others = [others]
        for other in others:
            self.outgoing.append(other)
            other.incoming.append(self)

class Place(Node):
    pass

class Transition(Node):
    @classmethod
    def connect(cls, pre_list, post_list, name):
        if len(pre_list) != len(post_list):
            print("transition ({}) is inbalanced!".format(name), file=sys.stderr)
        t = cls(name)
        for pre in pre_list:
            pre.connect_to(t)
        for post in post_list:
            t.connect_to(post)
        return t

def construct_dijkstra_net(number_of_processes):
    initialStates = [Place("state_Initial State {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    forbidSteals = [Place("state_Forbid Steal {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    checkTurns = [Place("state_Check Turn {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    failureToEnters = [Place("state_Failure to Enter {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    grabTurns = [Place("state_Grab Turn {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    cannotGrabs = [Place("state_Cannot Grab {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    declareEntrys = [Place("state_Declare Entry {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    startChecks = [Place("state_Start Check {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    selfChecks = [Place("state_Self Check {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    collisionChecks = [Place("state_Collision Check {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    steps = [Place("state_Step {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    criticalSections = [Place("state_Critical Section {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    yieldTurns = [Place("state_Yield Turn {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    declareExits = [Place("state_Declare Exit {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    permitSteals = [Place("state_Permit Steals {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    turnValues = [Place("var_Turn = {}".format(i)) for i in range(0,
        number_of_processes + 1)]
    jValues = [[Place("var_j[{}] = {}".format(i, v)) for v in range(0,
                number_of_processes + 2)] for i in range(0,
                    number_of_processes + 1)]
    outsides = [[Place("var_outside[{}] = {}".format(i, v)) for v in range(0, 2)]
                for i in range(0, number_of_processes + 1)]
    stealables = [[Place("var_stealable[{}] = {}".format(i, v)) for v in range(0, 2)]
                for i in range(0, number_of_processes + 1)]

    flat_jValues = [p for process_values in jValues for p in process_values]
    flat_outsides = [o for process_values in outsides for o in process_values]
    flat_stealables = [s for process_values in stealables for s in
            process_values]
    states = list(initialStates + forbidSteals + checkTurns + failureToEnters
            + grabTurns + cannotGrabs + declareEntrys + startChecks
            + selfChecks + collisionChecks + steps + criticalSections
            + yieldTurns + declareExits + permitSteals + flat_jValues
            + flat_outsides + flat_stealables + turnValues)

    transitions = list()

    for i in range(0, number_of_processes + 1):
        transitions.append(
            Transition.connect(
                    [initialStates[i], stealables[i][0]],
                    [forbidSteals[i], stealables[i][0]],
                    "Initial State to Forbid Steal {} (stealable = 0)".format(i))
            )
        transitions.append(
                Transition.connect(
                    [initialStates[i], stealables[i][1]],
                    [forbidSteals[i], stealables[i][0]],
                    "Initial State to Forbid Steal {} (stealable = 1)".format(i))
                )
        transitions.append(
                Transition.connect(
                    [forbidSteals[i]], [checkTurns[i]],
                    "Forbid Steal to Check Turns {}".format(i))
                )
        for v in range(0, number_of_processes + 1):
            if v == i: continue
            transitions.append(
                    Transition.connect(
                        [checkTurns[i], outsides[i][0], turnValues[v]],
                        [failureToEnters[i], outsides[i][1], turnValues[v]],
                        "Check Turns to Failure to Enter {} (outside = 0, turn = {})".format(i, v))
                    )
            transitions.append(
                Transition.connect(
                        [checkTurns[i], outsides[i][1], turnValues[v]],
                        [failureToEnters[i], outsides[i][1], turnValues[v]],
                        "Check Turns to Failure to Enter {} (outside = 1, turn = {})".format(i, v))
                )
            transitions.append(
                Transition.connect(
                        [failureToEnters[i], stealables[v][1], turnValues[v]],
                        [grabTurns[i], stealables[v][1], turnValues[i]],
                        "Failure to Enter to Grab Turn {i} (stealable[turn] = 1, turn := {i})".format(i = i))
                )
            transitions.append(
                Transition.connect(
                        [failureToEnters[i], stealables[v][0], turnValues[v]],
                        [cannotGrabs[i], stealables[v][0], turnValues[v]],
                        "Failure to Enter to Cannot Grab {i} (stealable[turn] = 0)".format(i = i))
            )
        transitions.append(
            Transition.connect(
                    [grabTurns[i]],
                    [cannotGrabs[i]],
                    "Grab Turns to Cannot Grab {}".format(i))
        )
        transitions.append(
            Transition.connect(
                    [cannotGrabs[i]],
                    [checkTurns[i]],
                    "Cannot Grab to Check Turn {}".format(i))
            )
        transitions.append(
            Transition.connect(
                    [checkTurns[i], turnValues[i], outsides[i][0]],
                    [declareEntrys[i], turnValues[i], outsides[i][0]],
                    "Check Turn to Declare Entry {i} (turn = {i}, outside[{i}] = 0)".format(i = i))
            )
        transitions.append(
            Transition.connect(
                    [checkTurns[i], turnValues[i], outsides[i][1]],
                    [declareEntrys[i], turnValues[i], outsides[i][0]],
                    "Check Turn to Declare Entry {i} (turn = {i}, outside[{i}] = 1)".format(i = i))
            )
        for v in range(0, number_of_processes + 1):
            transitions.append(
                Transition.connect(
                        [declareEntrys[i], jValues[i][v]],
                        [startChecks[i], jValues[i][1]],
                        "Declare Entry to Start Check (j[{}] = 1)".format(i))
                )
        transitions.append(
            Transition.connect(
                    [startChecks[i]],
                    [selfChecks[i]],
                    "Start Check to Self Check {}".format(i))
            )
        for v in range(0, number_of_processes + 1):
            if i == v: continue
            transitions.append(
                Transition.connect(
                        [selfChecks[i], jValues[i][v]],
                        [collisionChecks[i], jValues[i][v]],
                        "Self Check to Collision Check {i} (j[{i}] != {i})".format(i = i))
                )
            transitions.append(
                Transition.connect(
                        [collisionChecks[i], jValues[i][v], outsides[v][0]],
                        [checkTurns[i], jValues[i][v], outsides[v][0]],
                        "Collision Check to Check Turns {i} (outside[j[{i}]] = 0)".format(i = i))
                )
            transitions.append(
                Transition.connect(
                        [collisionChecks[i], jValues[i][v], outsides[v][1]],
                        [steps[i], jValues[i][v+1], outsides[v][1]],
                        "Collision Check to Step {i} (outside[j[{i}]] = 1, step[{i}]++)".format(i = i))
                )
        transitions.append(
            Transition.connect(
                    [selfChecks[i], jValues[i][i]],
                    [steps[i], jValues[i][i+1]],
                    "Self Check to Step {i}(j[{i}] = {i}, step[{i}]++)".format(i = i))
            )
        transitions.append(
            Transition.connect(
                    [steps[i], jValues[i][number_of_processes + 1]],
                    [criticalSections[i], jValues[i][number_of_processes + 1]],
                    "Step to Critical Section {i} (j[{i}] > N)".format(i = i))
            )
        for v in range(0, number_of_processes + 1):
            transitions.append(
                Transition.connect(
                        [steps[i], jValues[i][v]],
                        [selfChecks[i], jValues[i][v]],
                        "Step to Self Check {i} (j[{i}] <= N)".format(i = i))
                )
            transitions.append(
                Transition.connect(
                        [criticalSections[i], turnValues[v]],
                        [yieldTurns[i], turnValues[0]],
                        "Critical Section to Yield Turn {} (turn := 0)".format(i))
                )
        transitions.append(
            Transition.connect(
                    [yieldTurns[i], outsides[i][0]],
                    [declareExits[i], outsides[i][1]],
                    "Yield Turn to Declare Exit {i} (outside[{i}] = 0, outside[{i}] := 1)".format(i = i))
            )
        transitions.append(
            Transition.connect(
                    [yieldTurns[i], outsides[i][1]],
                    [declareExits[i], outsides[i][1]],
                    "Yield Turn to Declare Exit {i} (outside[{i}] = 1, outside[{i}] := 1)".format(i = i))
            )
        transitions.append(
            Transition.connect(
                    [declareExits[i], stealables[i][0]],
                    [permitSteals[i], stealables[i][1]],
                    "Declare Exit to Permit Steal {i} (stealable[{i}] = 0, stealable[[i]] := 1)".format(i = i))
            )
        transitions.append(
            Transition.connect(
                    [declareExits[i], stealables[i][1]],
                    [permitSteals[i], stealables[i][1]],
                    "Declare Exit to Permit Steal {i} (stealable[{i}] = 1, stealable[{i}] := 1)".format(i = i))
            )
        transitions.append(
            Transition.connect(
                    [permitSteals[i]], [initialStates[i]],
                    "Permit Steal to Initial State {}".format(i))
            )
    initials = list(initialStates[1:]
            + [outsides[i][1] for i in range(0, number_of_processes + 1)]
            + [jValues[i][0] for i in range(0, number_of_processes + 1)]
            + [stealables[i][1] for i in range(0, number_of_processes + 1)]
            + [turnValues[0]])

    return (states, transitions, initials, criticalSections)

def output_pnet(state_nodes, transitions, initials, critical_sections, number_of_processes):
    print("petri net \"Dijstra's version for n processes of Dekker's mutual exclusion algorithm for n={}\" {{".format(
        number_of_processes))
    print("\tplaces {")
    for state in state_nodes:
        print("\t\t{}".format(state.stripped_name))
    print("\t}")
    print("\ttransitions {")
    for transition in transitions:
        print("\t\t{}".format(transition.stripped_name))
    print("\t}")
    print("\tarcs {")
    for transition in transitions:
        print("\t\t{", end = " ")
        for i in transition.incoming:
            print(i.stripped_name, end = " ")
        print("} ->", end = " ")
        print(transition.stripped_name, end = " ")
        print("-> {", end = " ")
        for i in transition.outgoing:
            print(i.stripped_name, end = " ")
        print("}", end = "\n")
    print("\t}")
    print("\tinitial {")
    for init in initials:
        print("\t\t{}".format(init.stripped_name))
    print("\t}")
    print("}")
    print("safety property \"mutual exclusion\" {")
    print("\t", end = "")
    crits_list = list(critical_sections)
    head = crits_list[0]
    rest = crits_list[1:]
    print("\t{}".format(head.stripped_name), end = " ")
    for r in rest:
        print("+ {}".format(r.stripped_name), end = " ")
    print(">= 2")
    print("}")

if __name__ == "__main__":
    number_of_processes = int(sys.argv[1])
    output_pnet(*construct_dijkstra_net(number_of_processes), number_of_processes)
