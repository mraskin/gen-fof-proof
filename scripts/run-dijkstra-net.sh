#! /bin/sh

../lisp-gen-proof/gen-fof-proof.bin --non-interactive --load make-petri-net.lisp --eval "(run-petri-net (dijkstra-net-data $1) (loop for k from 1 to ${3:-20} $(for i in $2; do echo collect $i; done)) :formatter (agent-based-formatter $1))"
