#! /bin/sh

../lisp-gen-proof/gen-fof-proof.bin --non-interactive \
        --load make-petri-net.lisp \
        --eval '(princ (funcall (agent-based-formatter '"$1"')
                                (cl-ppcre:split "," "'"$2"'")))'
